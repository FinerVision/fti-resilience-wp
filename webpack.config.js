const path = require("path");
const webpack = require("webpack");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CopyPlugin = require("copy-webpack-plugin");
const {CleanWebpackPlugin} = require("clean-webpack-plugin");
require("dotenv").config();

const SRC_PATH = path.resolve(__dirname, "src", "assets");
const BUILD_PATH = path.resolve(__dirname, "public", "assets");

const config = {
  stats: "minimal",
  target: "web",
  entry: [path.join(SRC_PATH, "index.js"), path.join(SRC_PATH, "index.scss")],
  output: {
    path: BUILD_PATH,
    filename: "index.js",
    publicPath: "/",
  },
  plugins: [
    new webpack.DefinePlugin({
      // Pass values from .env file to browser
      "process.env.APP_ENV": JSON.stringify(process.env.APP_ENV),
    }),
    new MiniCssExtractPlugin({
      filename: "index.css",
      path: BUILD_PATH,
    }),
    new CopyPlugin([
      {
        from: path.join(SRC_PATH, "img"),
        to: path.join(BUILD_PATH, "img"),
      },
    ]),
    //new CleanWebpackPlugin(),
  ],
  module: {
    rules: [
      {
        test: /\.scss$/,
        exclude: /node_modules/,
        use: [
          MiniCssExtractPlugin.loader,
          "css-loader",
          "sass-loader",
        ],
      },
      {
        test: /\.js$/,
        use: "babel-loader",
        exclude: /node_modules/,
      },
      {
        test: /\.(png|jpeg|jpg|gif|webp|svg|mp4|webm|ttf|woff|woff2|ico)$/,
        use: [
          {
            loader: "file-loader",
            // options: {
            //   name: "[name].[ext]",
            //   outputPath: "fonts/",
            // },
          },
        ],
      },
    ],
  },
  devServer: {
    contentBase: BUILD_PATH,
    writeToDisk: true,
    port: process.env.CLIENT_DEV_PORT || 8081,
    host: "0.0.0.0",
    proxy: {
      '/': `http://0.0.0.0:${process.env.APP_PORT}`,
    },
  },
  devtool: "source-map",
};

module.exports = (env, argv) => {
  if (argv.mode === "production") {
    config.devtool = false;
  }
  return config;
};
