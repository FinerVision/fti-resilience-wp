#!/usr/bin/env bash

# Get new changes

git pull

docker system prune -f

docker-compose -f docker-compose.yml up --build -d

docker-compose -f docker-compose.yml exec server php artisan migrate

docker system prune -f
