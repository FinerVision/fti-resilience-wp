<main class="remediation-and-dispute-resolution">
    <div class="Background--Gradient-Blue-Black">
        <?php
        require __DIR__ . '/../components/leverCarousel.php'; ?>
    </div>

    <div class="container-large-md container mt-3 mt-lg-5">
    </div>
    <section class="Section--decoration-right">
        <div class="container-large-md container">
            <div class="row pt-0 pt-lg-5">
                <div class="col-sm-12 col-md-6">
                    <h2 class="Title--type-1 mb-3">
                        Businesses must brace themselves for a storm of investigations and litigation in the wake of COVID-19.
                    </h2>
                    <div class="Paragraph--type-3">
                        <p>
                            COVID-19 has created an environment where employees and other company stakeholders are more likely to engage in various forms of misconduct.  Fraudulently boosting financial results due to pressure to meet market expectations and internal targets, misappropriation of corporate assets, and gaps in companies’ internal controls are just a few examples of the heightened issues due to COVID-19.  In fact, whistleblower activity is up globally by 35%, and history supports there will be increased investigations as during the 2008 financial crisis complex US enforcement actions increased by 60%.
                        </p>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6">


                    <div class="mw-300px mx-auto text-center">
                        <div class="Progress-circle mx-auto">
                            <img src="./assets/img/icons/remediation-and-dispute-resolution.svg"/>
                        </div>
                        <p class="mt-3 Paragraph--type-2">
                            of business leaders surveyed were
                            not involved in the legal
                            processes at their organisation
                            <span>(compared to just 28% for operations

                            and 29% for strategy)</span>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="Section--decoration-left mb-5">
        <div class="container-large-md container">
            <div class="row mt-5">
                <div class="col-sm-12 col-md-6">
                    <div class="w-60 mx-auto">
                        <div class="Number">
                            28%
                            <div class="Number__bar">
                                <div class="Number__bar-full" style="width: 28%"></div>
                            </div>
                        </div>
                        <p class="mt-3 Paragraph--type-2">
                            of business leaders expect to
                            be victims of fraud in 2020
                        </p>
                    </div>
                    <div class="w-60 mx-auto">
                        <div class="Number">
                            9%
                            <div class="Number__bar">
                                <div class="Number__bar-full" style="width: 9%"></div>
                            </div>
                        </div>
                        <p class="mt-3 Paragraph--type-2">
                            of business leaders surveyed said they
                            would likely be involved in an investigation,
                            compared to 18% for political disruption
                            and 19% for trade restrictions
                        </p>
                    </div>

                </div>
                <div class="col-sm-12 col-md-6">
                    <div class="Paragraph--type-3">
                        <p>
                            COVID-19 has also opened a Pandora’s box of disputes – the inevitable consequence of insolvency issues, restructuring and supply chain disruptions. More than 440 COVID-related class actions have been filed in the US since March. Force majeure clauses are being invoked worldwide: in February alone, such clauses were invoked for over 3,000 contracts (valued at $38bn) in China. The surge in legal action is certain to continue around the world. 
                        </p>
                        <p>
                            The reasons are easy to understand. Companies are seeking to reduce exposure and mitigate damages through amendments to terms and pricing across various contract types, from securities and M&A to employment, fraud and insolvency. Financial pressures are forcing businesses to attempt to escape contractual obligations with some parties and seek reassurance (or damages) from others. Deadlines, supply conditions, fines, penalties and termination possibilities will all be contested. Even pre-existing disputes will be disrupted in this increasingly hostile environment: previously settled obligations may become impossible to fulfil, and remediation efforts rendered insufficient.
                        </p>
                        <p>
                            Businesses will not only face intense scrutiny from governments and regulators, but also the dual risk of direct enforcement action, and legal action from third parties (e.g. class-action lawsuits) as pre- and post-COVID misconduct is exposed.
                        </p>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <section class="Section--background-gray pt-3 pb-5">
        <div class="container-large-md container">
            <div class="row my-3"></div>
            <div class="row">
                <div class="col-sm-12 mb-5">
                    <div class="Title--type-2 text-uppercase">
                        Preparing now will create competitive advantage in the new normal
                    </div>
                </div>
                <div class="col-sm-12 col-md-6">
                    <div class="Paragraph--type-3">
                        <p>
                            Understandably, few businesses anticipated these issues. In January, FTI’s Resilience Barometer<sup>TM</sup> found fewer than one in five companies expecting to be litigated against in 2020. Only 9% of business leaders anticipated investigation from regulatory bodies. Political disruption and trade restrictions preoccupied twice as many respondents as regulatory investigations.
                        </p>
                        <p>
                            The world has now changed. Fortunately, with prompt action, lack of preparation can in many cases be remedied.
                        </p>
                    </div>

                </div>
                <div class="col-sm-12 col-md-6">
                    <div class="w-60 mx-auto">
                        <div class="Number">
                            20%
                            <div class="Number__bar">
                                <div class="Number__bar-full" style="width: 20%">

                                </div>
                            </div>
                            <div class="Paragraph--type-2 mt-3">
                                of business leaders surveyed said they had been litigated against in the year, and 19% said
                                they expected to be litigated against in the next 12 months
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <div class="Section__triangle Section__triangle--type-8">
        <div class="container-large-md container py-5">
            <div class="row mt-5">
                <div class="col-sm-12 text-center">
                    <div class="mw-300px mx-auto">
                        <div class="Title--type-2 text-white">
                            COMPETE
                        </div>
                        <div class="Paragraph--type-1 text-white">
                            Business fundamentals must ultimately be re-evaluated to ensure they suit emerging business conditions: Systems for negotiating and performing contracts should be modified to build resilience against new threats, and to stay ahead of the evolving regulatory landscape.

                        </div>
                    </div>
                </div>
            </div>
            <div class="row py-5 my-5"></div>
            <div class="row mb-5 mt-150px">
                <div class="col-sm-12 col-md-4 text-center">
                    <div class="mw-360px mx-auto">
                        <div class="Title--type-2 text-white text-right">
                            ADAPT
                        </div>
                        <div class="Paragraph--type-1 text-right">
                            Business leaders must also adjust oversight of the business to prevent misconduct, overhaul investigation and dispute action plans, and renegotiate contracts that are no longer appropriate. Fraud and other threat detection systems should be optimised.
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-4"></div>
                <div class="col-sm-12 col-md-4 text-center">
                    <div class="mw-360px mx-auto">
                        <div class="Title--type-2 text-white text-left">
                            PROTECT
                        </div>
                        <div class="Paragraph--type-1 text-left">
                            Businesses must quickly identify supply chain vulnerabilities, review financial and regulatory compliance, ensure that internal controls are operating effectively in the new environment, assess obligations to vendors and review contracts. In-house counsel can play a key role in finding avenues of renegotiation to aid in cost reduction across the business, and to ensure business continuity in a contentious environment.

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <section class="Home__Perspectives">
        <div class="container-large-md container">
            <div class="Home__Perspectives__Title">
                <h2 class="Home__Perspectives__Title-Text"><?php echo $sections['perspectives']['title']; ?></h2>
            </div>
            <div class="Home__Perspectives__Slider-Wrapper">

                <div class="Home__Perspectives__Slider-Wrapper__Slider">
                    <div class="swipper-container">
                        <div class="Home__Perspectives-Slider-Button button-previous">
                            <img src="/content/img/left-arrow-navigation-v1.svg"/>
                        </div>
                        <div class="swiper-wrapper">

                            <div class="swiper-slide">
                                <div class="Home__Perspectives__Card">
                                    <div class="Home__Perspectives__Card__Header">
                                        <div class="Home__Perspectives__Card__Header-Image-Border border-line--type-2"></div>
                                        <div class="Home__Perspectives__Card__Header-Image-Placeholder" style="background-size:cover; background-image: url(/content/img/Crop-4-01.png)"></div>

                                    </div>
                                    <div class="Home__Perspectives__Card__Description">
                                        <p class="color-remediation">COVID-19: Commercial Risk Mitigation & Damages Claims in Africa</p>
                                        <a class="color-remediation Home__Perspectives__Card__Description-Link"
                                           href="/remediation-and-dispute-resolution/COVID-19:CommercialRiskMitigation&DamagesClaimsinAfrica">
                                            READ MORE
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="swiper-slide">
                                <div class="Home__Perspectives__Card">
                                    <div class="Home__Perspectives__Card__Header">
                                        <div class="Home__Perspectives__Card__Header-Image-Border border-line--type-2"></div>
                                        <div class="Home__Perspectives__Card__Header-Image-Placeholder" style="background-size:cover; background-image: url(/content/img/Crop-5-01.png)"></div>

                                    </div>
                                    <div class="Home__Perspectives__Card__Description">
                                        <p class="color-remediation">Decade of Disputes: The Trillion Dollar Investor View</p>
                                        <a class="color-remediation Home__Perspectives__Card__Description-Link"
                                           href="/remediation-and-dispute-resolution/DecadeofDisputes:TheTrillionDollarInvestorView">
                                            READ MORE
                                        </a>
                                    </div>
                                </div>
                            </div>


                            <div class="swiper-slide">
                                <div class="Home__Perspectives__Card">
                                    <div class="Home__Perspectives__Card__Header">
                                        <div class="Home__Perspectives__Card__Header-Image-Border border-line--type-2"></div>
                                        <div class="Home__Perspectives__Card__Header-Image-Placeholder" style="background-size:cover; background-image: url(/content/img/Crop-2-01.png)"></div>

                                    </div>
                                    <div class="Home__Perspectives__Card__Description">
                                        <p class="color-remediation">
                                            Tackling financial crime: now is the time to break down silos
                                        </p>
                                        <a class="color-remediation Home__Perspectives__Card__Description-Link"
                                           href="/remediation-and-dispute-resolution/Tackling-financial-crime:-now-is-the-time-to-break-down-silos">
                                            READ MORE
                                        </a>
                                    </div>
                                </div>
                            </div>



                        </div>
                        <div class="Home__Perspectives-Slider-Button button-next">
                            <img src="/content/img/right-arrow-navigation-v1.svg"/>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <?php include 'exploreanotherlever.php'; ?>
</main>
