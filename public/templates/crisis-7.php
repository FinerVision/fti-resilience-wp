<main class="Home">
    <?php include 'perspectiveheader.php'; ?>

    <div class="Section--background-perspectives">
        <div class="container-large-md container">
            <div class="row">
                <div class="col-sm-12 col-md-8 Section--bg-white py-5">
                    <div class="Title--type-2 color-dark-blue mb-4">XX JULY 2020</div>
                    <hr class="HR HR--type-1">
                    <h1 class="Title--type-7 mt-3">
                        Climate Image:<br/> A New Growth Trajectory?
                    </h1>
                    <b>
                        It can take decades to build the desired reputation and the press of a button to destroy one.
                        From Financial Services to Fast Moving Consumer Goods (FMCG), firms are beginning to see the
                        perception of their climate image as a critical component of their business proposition and key
                        to attracting employee talent, consumers and investors.
                    </b>
                    <p>
                        In 2015, the Task Force on Climate-Related Financial Disclosures (TCFD) set up by the Financial
                        Stability Board (FSB) developed voluntary, consistent climate-related financial disclosures for
                        use by companies, banks and investors in providing information to stakeholders.
                    </p>
                    <p>
                        It was thought that increasing the amount of reliable information on firms’ exposures to climate
                        change would highlight potential dangers and opportunities, consequently strengthening the
                        global economic system and facilitating the transition to a lower-carbon economy.
                    </p>
                    <p>
                        <b>
                            Why should firms manage their climate image proactively?

                        </b>
                    </p>
                    <p>
                        Shifts in consumer preferences are precipitating climate image as an important factor that
                        influences how consumers and investors are making choices about firms. The change in sentiment
                        towards climate issues is driving firms to consider how to proactively manage their public
                        climate image and avoid potential damage to their reputation that comes with environmentally
                        controversial activities or partnerships.
                    </p>
                    <p>
                        Social media and digital news channels create a platform for consumers, employees, investors and
                        others to scrutinise and discuss how a firm is perceived to be supporting climate-related
                        action, how it is reacting to policy or technological developments to support the transition to
                        a lower-carbon economy, or who the firm chooses as its partners and suppliers.
                    </p>
                </div>
                <div class="col-sm-12 col-md-4 Section--sidebar">
                    <div class="Title--type-2 color-dark-blue mb-4">&nbsp;</div>
                    <div class="SocialBox">
                        SHARE THIS CONTENT
                        <ul class="SocialShare SocialShare--align-left mt-3">
                            <li class="SocialShare__Item">
                                <a class="SocialShare__Item__Link" href="#" target="_blank">
                                    <img class="SocialShare__Item__Link-Icon" src="../assets/img/facebook-icon-v2.svg">
                                </a>
                            </li>
                            <li class="SocialShare__Item">
                                <a class="SocialShare__Item__Link" href="#" target="_blank">
                                    <img class="SocialShare__Item__Link-Icon" src="../assets/img/twitter-icon-v2.svg">
                                </a>
                            </li>
                            <li class="SocialShare__Item">
                                <a class="SocialShare__Item__Link" href="#" target="_blank">
                                    <img class="SocialShare__Item__Link-Icon" src="../assets/img/linkedin-icon-v2.svg">
                                </a>
                            </li>
                            <li class="SocialShare__Item">
                                <a class="SocialShare__Item__Link" href="#" target="_blank">
                                    <img class="SocialShare__Item__Link-Icon" src="../assets/img/share-icon-v1.svg">
                                </a>
                            </li>
                        </ul>
                    </div>

                    <div class="Document mt-5">
                        <img src="../assets/img/documents/how-european-retailers-manage-covid-19-disruption.png"
                             class="img-fluid"/>
                    </div>

                    <a href="../assets/img/documents/climate-image-new-growth-trajectory.pdf" target="_blank"
                       class="Button Button--type-1 mt-3">
                        DOWNLOAD >
                    </a>
                </div>
            </div>
        </div>
    </div>

    <?php include 'exploreothertopics.php'; ?>
</main>
