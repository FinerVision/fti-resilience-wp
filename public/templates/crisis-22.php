<main class="Home">
    <?php include 'perspectiveheader.php'; ?>

    <div class="Section--background-perspectives">
        <div class="container-large-md container">
            <div class="row">
                <div class="col-sm-12 col-md-8 Section--bg-white py-5">
                    <div class="Title--type-2 color-dark-blue mb-4">XX JULY 2020</div>
                    <hr class="HR HR--type-1">
                    <h1 class="Title--type-7 mt-3">
                        The Case for Culture
                    </h1>
                    <b>
                        Yes, culture matters right now, and it matters for your business success in a post-COVID-19
                        world.

                    </b>
                    <p>
                        Here is why you cannot afford to ignore culture: in a world in which we are being forced to
                        quickly redefine the future normal, you will need to stand not just on your strengths but on
                        what makes you different; you will need to actively utilize and further develop your cultural
                        foundation; and you may even need to rewire the DNA of your organization’s behaviors to meet
                        future demands and create a more resilient business.
                    </p>
                    <p>
                        The seismic shift of the coronavirus crisis has accelerated several trends related to the future
                        of work and future business models. The risk of doing nothing or assuming these developments
                        won’t influence your culture is just too great. So, while it is likely that many companies won’t
                        prioritize culture in the coming weeks, those that do will be much better prepared to adapt and
                        thrive in a post-COVID-19 world.
                    </p>
                </div>
                <div class="col-sm-12 col-md-4 Section--sidebar">
                    <div class="Title--type-2 color-dark-blue mb-4">&nbsp;</div>
                    <div class="SocialBox">
                        SHARE THIS CONTENT
                        <ul class="SocialShare SocialShare--align-left mt-3">
                            <li class="SocialShare__Item">
                                <a class="SocialShare__Item__Link" href="#" target="_blank">
                                    <img class="SocialShare__Item__Link-Icon" src="../assets/img/facebook-icon-v2.svg">
                                </a>
                            </li>
                            <li class="SocialShare__Item">
                                <a class="SocialShare__Item__Link" href="#" target="_blank">
                                    <img class="SocialShare__Item__Link-Icon" src="../assets/img/twitter-icon-v2.svg">
                                </a>
                            </li>
                            <li class="SocialShare__Item">
                                <a class="SocialShare__Item__Link" href="#" target="_blank">
                                    <img class="SocialShare__Item__Link-Icon" src="../assets/img/linkedin-icon-v2.svg">
                                </a>
                            </li>
                            <li class="SocialShare__Item">
                                <a class="SocialShare__Item__Link" href="#" target="_blank">
                                    <img class="SocialShare__Item__Link-Icon" src="../assets/img/share-icon-v1.svg">
                                </a>
                            </li>
                        </ul>
                    </div>

                    <div class="Document mt-5">
                        <img src="../assets/img/documents/FTI-Cybersecurity-Building-Cybersecurity-Resilience.png"
                             class="img-fluid"/>
                    </div>

                    <a href="../assets/img/documents/FTI-Cybersecurity-Building-Cybersecurity-Resilience.pdf"
                       target="_blank" class="Button Button--type-1 mt-3">
                        DOWNLOAD >
                    </a>
                </div>
            </div>
        </div>
    </div>

    <?php include 'exploreothertopics.php'; ?>
</main>
