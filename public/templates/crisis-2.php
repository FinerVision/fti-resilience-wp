<main class="Home">
    <?php include 'perspectiveheader.php'; ?>


    <div class="Section--background-perspectives">
        <div class="container-large-md container">
        <div class="row">
            <div class="col-sm-12 col-md-8 Section--bg-white py-5">
                <hr class="HR HR--type-1">
                <h1 class="Title--type-8 mt-3">
                    The Anatomy Of A Crisis: Volume 2 – Why Do Markets Overreact To Profit Warnings?
                </h1>

                <b>
                    FTI Consulting’s ‘Anatomy of a Crisis’ series investigates the emerging trends in corporate crises, learning from the past to make predictions about the future. Our first research report, published in 2017, analysed 100 crises over the past 20 years to assess what patterns emerged.

                </b>
                <p>
                    We combine our expertise in Corporate and Financial Reputation with our skills in Data Analytics to review past events and draw conclusions about how situations are likely to unfold in the future. In doing so, our research helps Boards and communicators to plan ahead when facing crisis scenarios of their own.
                </p>
                <p>
                    In this edition, we take a close look at the interaction between company announcements and market moves, addressing one central question of relevance to boards, management teams, PR professionals and IR teams alike: Why do financial markets overreact to bad news?
                </p>
                <p>
                    The crises we reviewed span the last 20 years, and include oil spills, cyber hacks, plane crashes, cases of fraud, product recalls and many more. We were interested to see what patterns emerge from these events – patterns which might be instructive for Boards and communicators when facing their own crisis scenario.

                </p>
            </div>
            <div class="col-sm-12 col-md-4 Section--sidebar">
                <div class="Title--type-2 color-dark-blue mb-4">&nbsp;</div>
                <div class="SocialBox">
                    SHARE THIS CONTENT
                    <ul class="SocialShare SocialShare--align-left mt-3">
                        <li class="SocialShare__Item">
                            <a class="SocialShare__Item__Link" href="#" target="_blank">
                                <img class="SocialShare__Item__Link-Icon" src="../assets/img/facebook-icon-v2.svg">
                            </a>
                        </li>
                        <li class="SocialShare__Item">
                            <a class="SocialShare__Item__Link" href="#" target="_blank">
                                <img class="SocialShare__Item__Link-Icon" src="../assets/img/twitter-icon-v2.svg">
                            </a>
                        </li>
                        <li class="SocialShare__Item">
                            <a class="SocialShare__Item__Link" href="#" target="_blank">
                                <img class="SocialShare__Item__Link-Icon" src="../assets/img/linkedin-icon-v2.svg">
                            </a>
                        </li>
                        <li class="SocialShare__Item">
                            <a class="SocialShare__Item__Link" href="#" target="_blank">
                                <img class="SocialShare__Item__Link-Icon" src="../assets/img/share-icon-v1.svg">
                            </a>
                        </li>
                    </ul>
                </div>

                <div class="Document mt-5">
                    <img src="../assets/img/documents/FTI Consulting Anatomy of a Crisis 2 - Why do markets overreact to profit warnings.png" class="img-fluid"/>
                </div>

                <a href="../assets/img/documents/FTI Consulting Anatomy of a Crisis 2 - Why do markets overreact to profit warnings.pdf" target="_blank" class="Button Button--type-1 mt-3">
                    DOWNLOAD >
                </a>
            </div>
        </div>
        </div>
    </div>

    <?php include 'exploreothertopics.php'; ?>
</main>
