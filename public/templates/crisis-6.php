<main class="Home">
    <?php include 'perspectiveheader.php'; ?>

    <div class="Section--background-perspectives">
        <div class="container-large-md container">
            <div class="row">
                <div class="col-sm-12 col-md-8 Section--bg-white py-5">
                    <div class="Title--type-2 color-dark-blue mb-4">XX JULY 2020</div>
                    <hr class="HR HR--type-1">
                    <h1 class="Title--type-7 mt-3">
                        How European Retailers Can Manage COVID-19 Disruption and Make It to the Other Side
                    </h1>
                    <b>
                        Only a few months ago shopping with friends or family was a normal part of daily life, yet now
                        it is considered a potentially perilous excursion for many of us. COVID-19 stunned our economy
                        and shuttered vast portions of the retail sector, with physical retail footfall reportedly
                        dropping by c.80% at its peak. With restrictions eased, retailers across Europe need to learn to
                        manage through the continued downturn, be prepared for a potential second wave and adapt to an
                        uncertain future.
                    </b>
                    <p>
                        Retailers are familiar with business continuity plans — they are annual exercises for many
                        retailers, given extreme weather events, distribution centre closures and other local disasters.
                        However, few, if any, retailers planned for a black swan event that would close the entire store
                        fleet for weeks.
                    </p>
                    <p>
                        In this article, our Business Transformation team looks at how European retailers will adapt to
                        the disruption caused by COVID-19 and what key measures they should be considering in order to
                        make it to the other side.
                    </p>
                </div>
                <div class="col-sm-12 col-md-4 Section--sidebar">
                    <div class="Title--type-2 color-dark-blue mb-4">&nbsp;</div>
                    <div class="SocialBox">
                        SHARE THIS CONTENT
                        <ul class="SocialShare SocialShare--align-left mt-3">
                            <li class="SocialShare__Item">
                                <a class="SocialShare__Item__Link" href="#" target="_blank">
                                    <img class="SocialShare__Item__Link-Icon" src="../assets/img/facebook-icon-v2.svg">
                                </a>
                            </li>
                            <li class="SocialShare__Item">
                                <a class="SocialShare__Item__Link" href="#" target="_blank">
                                    <img class="SocialShare__Item__Link-Icon" src="../assets/img/twitter-icon-v2.svg">
                                </a>
                            </li>
                            <li class="SocialShare__Item">
                                <a class="SocialShare__Item__Link" href="#" target="_blank">
                                    <img class="SocialShare__Item__Link-Icon" src="../assets/img/linkedin-icon-v2.svg">
                                </a>
                            </li>
                            <li class="SocialShare__Item">
                                <a class="SocialShare__Item__Link" href="#" target="_blank">
                                    <img class="SocialShare__Item__Link-Icon" src="../assets/img/share-icon-v1.svg">
                                </a>
                            </li>
                        </ul>
                    </div>

                    <div class="Document mt-5">
                        <img src="../assets/img/documents/how-european-retailers-manage-covid-19-disruption.png"
                             class="img-fluid"/>
                    </div>

                    <a href="../assets/img/documents/how-european-retailers-manage-covid-19-disruption.pdf"
                       target="_blank" class="Button Button--type-1 mt-3">
                        DOWNLOAD >
                    </a>
                </div>
            </div>
        </div>
    </div>

    <?php include 'exploreothertopics.php'; ?>
</main>
