<main class="Home">
    <?php include 'perspectiveheader.php'; ?>

    <div class="Section--background-perspectives">
        <div class="container-large-md container">
            <div class="row">
                <div class="col-sm-12 col-md-8 Section--bg-white py-5">
                    <div class="Title--type-2 color-dark-blue mb-4">XX JULY 2020</div>
                    <hr class="HR HR--type-1">
                    <h1 class="Title--type-7 mt-3">
                        Tackling financial crime: now is the time to break down silos
                    </h1>
                    <p>
                        The sharing of financial crime intelligence within financial services is often limited and
                        segregated. Technology can bridge the gap between jurisdictions, business lines and departments,
                        whilst saving time on investigations, driving down costs and identifying financial crime
                        typologies. So why do organisations persist in using an outdated, fragmented approach to
                        financial crime risk management?
                    </p>
                    <p>
                        Intelligence Sharing Is the Cornerstone of an Effective Anti-Financial Crime Framework
                    </p>
                    <p>
                        Amid the Coronavirus pandemic, now more than ever, firms need to find a more effective way to
                        share intelligence on financial crime to ensure they maintain effective controls and mitigate
                        risks. The Financial Conduct Authority has stated that criminals are taking advantage of the
                        current situation and targeting firms’ systems through fraud and exploitation schemes.

                    </p>
                </div>
                <div class="col-sm-12 col-md-4 Section--sidebar">
                    <div class="Title--type-2 color-dark-blue mb-4">&nbsp;</div>
                    <div class="SocialBox">
                        SHARE THIS CONTENT
                        <ul class="SocialShare SocialShare--align-left mt-3">
                            <li class="SocialShare__Item">
                                <a class="SocialShare__Item__Link" href="#" target="_blank">
                                    <img class="SocialShare__Item__Link-Icon" src="../assets/img/facebook-icon-v2.svg">
                                </a>
                            </li>
                            <li class="SocialShare__Item">
                                <a class="SocialShare__Item__Link" href="#" target="_blank">
                                    <img class="SocialShare__Item__Link-Icon" src="../assets/img/twitter-icon-v2.svg">
                                </a>
                            </li>
                            <li class="SocialShare__Item">
                                <a class="SocialShare__Item__Link" href="#" target="_blank">
                                    <img class="SocialShare__Item__Link-Icon" src="../assets/img/linkedin-icon-v2.svg">
                                </a>
                            </li>
                            <li class="SocialShare__Item">
                                <a class="SocialShare__Item__Link" href="#" target="_blank">
                                    <img class="SocialShare__Item__Link-Icon" src="../assets/img/share-icon-v1.svg">
                                </a>
                            </li>
                        </ul>
                    </div>

                    <div class="Document mt-5">
                        <img src="../assets/img/documents/tackling-financial-crime-now-time-break-down-silos.png"
                             class="img-fluid"/>
                    </div>

                    <a href="../assets/img/documents/tackling-financial-crime-now-time-break-down-silos.pdf"
                       target="_blank" class="Button Button--type-1 mt-3">
                        DOWNLOAD >
                    </a>
                </div>
            </div>
        </div>
    </div>

    <?php include 'exploreothertopics.php'; ?>
</main>
