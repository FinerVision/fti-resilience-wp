<main class="remediation-and-dispute-resolution">
    <div class="Background--Gradient-Blue-Black">
        <?php
        require __DIR__ . '/../components/leverCarousel.php'; ?>
    </div>

    <div class="container-large-md container mt-3 mt-lg-5">
    </div>
    <section class="Section--decoration-right">
        <div class="container-large-md container">
            <div class="row pt-0 pt-lg-5">
                <div class="col-sm-12 col-md-6">
                    <h2 class="Title--type-1 color-blue800 mb-3">
                        The most successful organisations in coming years will be those that recognise cyber resilience as a vital source of competitive advantage – but for now, cybersecurity strategy could be key to survival in a world disrupted by COVID-19.
                    </h2>
                </div>
                <div class="col-sm-12 col-md-6">
                    <div class="mw-300px mx-auto">
                        <p class="mt-2 Paragraph--type-2">
                            More than
                        </p>
                        <p class="Title--type-7 color-blue800 mb-0 font-weight-bold">1 in 5</p>
                        <p class="mt-2 Paragraph--type-2">
                            business leaders surveyed had
                            lost customer information
                            through cybersecurity incidents
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="Section--decoration-left mb-5">
        <div class="container-large-md container">
            <div class="row mt-5">
                <div class="col-sm-12 col-md-6 pl-md-5">
                    <div class="mw-300px mx-auto">

                        <p class="mt-2 Paragraph--type-2">
                            Companies receive
                        </p>
                        <p class="Title--type-7 color-blue800 mb-0 font-weight-bold">5x</p>
                        <p class="Paragraph--type-2 mt-0">
                            the media attention in the month
                            following a cyber breach, and
                        </p>
                        <p class="Title--type-7 color-blue800 mb-0 font-weight-bold">8x</p>
                        <p class="Paragraph--type-2 mt-0">
                            the social media coverage
                        </p>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6">
                    <div class="Paragraph--type-3">
                        <p>
                            Cyber attacks were already frequent before the outbreak of COVID-19. According to FTI Consulting’s Resilience Barometer™ 2020 (published in January), 20% of organisations had been victims of a ransom or data hostage situation.

                        </p>
                        <p>
                            Ongoing digitisation of business means cyber attacks can inflict damage right across commercial ecosystems. Of companies experiencing cyber attacks, 27% lose revenue and similar proportions lose assets or customer information. Reputational harm is another danger. FTI research indicates that on average, a company receives five times more media coverage and eight times more social media coverage in the month after a cyber breach than in normal conditions.

                        </p>
                        <p>
                            Despite these risks, a worrying 90% of Barometer respondents reported gaps in their organisations’ cybersecurity defences. Now, more than ever, it’s vital to address these gaps and mitigate risks in a dynamic, increasingly vulnerable world.

                        </p>
                    </div>

                </div>
            </div>
        </div>

    </section>
    <section class="Section--background-gray pt-3 pb-5">
        <div class="container-large-md container">
            <div class="row my-3"></div>
            <div class="row">
                <div class="col-sm-12 mb-5">
                    <div class="Title--type-2 text-uppercase">
                        ESTABLISHING A CYBER RISK MITIGATION STRATEGY
                    </div>
                </div>
                <div class="col-sm-12 col-md-6">
                    <div class="Paragraph--type-3">
                        <p>
                            Resilience across the business can best be achieved through a comprehensive cyber risk mitigation strategy. This entails analysing the organisation’s unique cyber risk profile, creating and maintaining organisation-wide cybersecurity awareness, identifying critical assets, and developing and testing business continuity and incident response plans.
                        </p>
                        <p>
                            The strategy must be driven by the CEO and Board. For that to happen, cybersecurity leaders and advisers should explain the issues in a way that makes sense to senior management, focusing on commercial imperatives rather than technicalities. 
                        </p>
                    </div>

                </div>
                <div class="col-sm-12 col-md-6">
                    <div class="Paragraph--type-3 mb-3">
                        <p>
                            Through holistic management of cyber risk, businesses can mitigate threats and reduce downtime. If an incident does occur, resilient organisations can respond effectively, safeguard their reputation, and restore trust in their digital ecosystems. 

                        </p>
                    </div>
                </div>

            </div>
            <div class="row mt-5 py-5">
                <div class="col">
                    <div class="Number color-blue800">
                        90%
                        <div class="Number__bar bar--white">
                            <div class="Number__bar-full color-blue800-bg" style="width: 90%">
                            </div>
                        </div>
                        <div class="Paragraph--type-2 mt-3">
                            of business leaders surveyed
                            believed they had gaps in their
                            cybersecurity frameworks
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="Number color-blue800">
                        20%
                        <div class="Number__bar bar--white">
                            <div class="Number__bar-full color-blue800-bg" style="width: 20%">

                            </div>
                        </div>
                        <div class="Paragraph--type-2 mt-3">
                            of business leaders surveyed
                            had been victims of ransom
                            or data-hostage situations
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="Number color-blue800">
                        35%
                        <div class="Number__bar bar--white">
                            <div class="Number__bar-full color-blue800-bg" style="width: 35%">

                            </div>
                        </div>
                        <div class="Paragraph--type-2 mt-3">
                            of business leaders surveyed believed
                            the gaps in their cybersecurity
                            frameworks came from employee
                            awareness and training
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="Section__triangle Section__triangle--type-7">
        <div class="container-large-md container py-5">
            <div class="row mt-5">
                <div class="col-sm-12 text-center">
                    <div class="mw-360px mx-auto">
                        <div class="Title--type-2 text-white">
                            COMPETE
                        </div>
                        <div class="Paragraph--type-1 text-white">
                            Given the inevitability of cybersecurity incidents, the entire workforce should be recruited to build cyber resilience. Increasing the maturity of the cybersecurity approach in this way will create competitive advantage by positioning the business to seize new opportunities and grow securely.

                        </div>
                    </div>
                </div>
            </div>
            <div class="row py-5 my-5"></div>
            <div class="row mb-5 mt-150px">
                <div class="col-sm-12 col-md-4 text-center">
                    <div class="mw-360px mx-auto">
                        <div class="Title--type-2 text-white text-right">
                            ADAPT
                        </div>
                        <div class="Paragraph--type-1 text-right">
                            Cybersecurity policies, processes and procedures must adapt to new working practices. Consider how your threat profile has changed, whether the incident response plan is compatible with remote working, and how far third-party suppliers comply with cybersecurity policies.
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-4"></div>
                <div class="col-sm-12 col-md-4 text-center">
                    <div class="mw-360px mx-auto">
                        <div class="Title--type-2 text-white text-left">
                            PROTECT
                        </div>
                        <div class="Paragraph--type-1 text-left">
                            Today’s threat landscape is evolving rapidly: for example, remote working may dictate an increased reliance on employee diligence, together with some acceptance of temporary IT solutions that are installed without assessing vulnerabilities. A cybersecurity risk assessment is key to protecting your business, employees and technical infrastructure.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <section class="Home__Perspectives">
        <div class="container-large-md container">
            <div class="Home__Perspectives__Title">
                <h2 class="Home__Perspectives__Title-Text"><?php echo $sections['perspectives']['title']; ?></h2>
            </div>
            <div class="Home__Perspectives__Slider-Wrapper">

                <div class="Home__Perspectives__Slider-Wrapper__Slider">
                    <div class="swipper-container">
                        <div class="Home__Perspectives-Slider-Button button-previous">
                            <img src="/content/img/left-arrow-navigation-v1.svg"/>
                        </div>
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <div class="Home__Perspectives__Card">
                                    <div class="Home__Perspectives__Card__Header">
                                        <div class="Home__Perspectives__Card__Header-Image-Border border-line--type-7"></div>
                                        <div class="Home__Perspectives__Card__Header-Image-Placeholder" style="background-size:cover; background-image: url(/content/img/Crop-4-01.png)"></div>

                                    </div>
                                    <div class="Home__Perspectives__Card__Description">
                                        <p class="color-digital">Covid 19 New Cyber Threats</p>
                                        <a class="color-digital sHome__Perspectives__Card__Description-Link"
                                           href="/digital-trust-and-ecosystems/The-Anatomy-of-a-Crisis-3-Cyber-Breaches">
                                            READ MORE
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="swiper-slide">
                                <div class="Home__Perspectives__Card">
                                    <div class="Home__Perspectives__Card__Header">
                                        <div class="Home__Perspectives__Card__Header-Image-Border border-line--type-7"></div>
                                        <div class="Home__Perspectives__Card__Header-Image-Placeholder" style="background-size:cover; background-image: url(/content/img/Crop-5-01.png)"></div>

                                    </div>
                                    <div class="Home__Perspectives__Card__Description">
                                        <p class="color-digital">The Anatomy of a Crisis #3 – Cyber Breaches
                                        </p>
                                        <a class="color-digital sHome__Perspectives__Card__Description-Link"
                                           href="/digital-trust-and-ecosystems/COVID-19-New-Cyber-Threats">
                                            READ MORE
                                        </a>
                                    </div>
                                </div>
                            </div>


                            <div class="swiper-slide">
                                <div class="Home__Perspectives__Card">
                                    <div class="Home__Perspectives__Card__Header">
                                        <div class="Home__Perspectives__Card__Header-Image-Border border-line--type-7"></div>
                                        <div class="Home__Perspectives__Card__Header-Image-Placeholder" style="background-size:cover; background-image: url(/content/img/Crop-2-01.png)"></div>

                                    </div>
                                    <div class="Home__Perspectives__Card__Description">
                                        <p class="color-digital">
                                            Not If But When: Building Cybersecurity Resilience to Secure Competitive Advantage
                                        </p>
                                        <a class="color-digital sHome__Perspectives__Card__Description-Link"
                                           href="/digital-trust-and-ecosystems/Building-Cybersecurity-Resilience">
                                            READ MORE
                                        </a>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="Home__Perspectives-Slider-Button button-next">
                            <img src="/content/img/right-arrow-navigation-v1.svg"/>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <?php include 'exploreanotherlever.php'; ?>
</main>
