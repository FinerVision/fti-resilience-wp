<main class="Home">
    <img class="Home--Detail--Horizontal" src="/content/img/shard-detail-horizontal-v1.svg"/>
    <img class="Home--Detail--Vertical" src="/content/img/shard-detail-vertical-v1.svg"/>
    <div class="Background--Gradient-Blue-Black">
        <div class="container-large-md container">
            <div class="Home__Header">
                <div class="row">
                    <div class="col-sm-6">
                        <h1 class="Home__Header-Title Animate Animate--fade-up">
                            <?php echo $header['title'] ?>
                        </h1>
                    </div>
                    <div class="col-sm-6">
                        <div class="Home__Header__Detail mt-3">
                            <img class="Home__Header__Detail-Image Animate Animate--slide-to-left" src="<?php echo $header['detailImage'] ?>"/>
                        </div>
                    </div>
                </div>
                <div class="Home__Header__Description">
                    <p class="Home__Header__Description-Text Animate Animate--fade-up"><?php echo $header['description']; ?></p>
                </div>
            </div>
            <section class="Home__Info__Graphic">
                <div class="Home__Info__Graphic__Content mb-1 mb-md-5">
                    <div class="Home__Info__Graphic__Content__Title Animate--delay-elements">
                        <img class="Home__Info__Graphic__Content__Title-Icon Animate Animate--fade-up"
                             src="<?php echo $sections['infoGraphic']['iconSrc']; ?>"/>
                        <h2 class="Home__Info__Graphic__Content__Title-Text Animate Animate--fade-up"><?php echo $sections['infoGraphic']['title']; ?></h2>
                    </div>
                </div>
                <div class="Home__Info__Graphic-Wrapper" style="text-align: center">
<!--                    <iframe src="graph.html" height="100%" width="auto" frameBorder="0" title="Iframe Example"></iframe>-->
<!--                    <iframe src="graph.html" name="banner" width="660px" height="590" marginwidth="0">Browser not compatible. </iframe>-->
<br/>
<br/>
<br/>
                    <div id="triangle-graphic"></div>
<!--                    <img class="Home__Info__Graphic-Image" src="/content/img/the-resilience-agenda-infographic-v1.svg"/>-->
                </div>
            </section>
            <section class="Home__Resilience-Barometer mt-5">
                <div class="Home__Resilience-Barometer__Content">
                    <div class="Home__Resilience-Barometer__Content__Title Animate--delay-elements">
                        <img class="Home__Resilience-Barometer__Content__Title-Icon Animate Animate--fade-up mh-100"
                             src="<?php echo $sections['infoGraphic']['iconSrc']; ?>"/>
                        <h2 class="Animate Animate--fade-up"><?php echo $sections['resilienceBarometer']['title'] ?></h2>
                    </div>
                </div>
                <div class="row Barometer--Section-Content">
                    <div class="col-sm-7">
                        <div class="Home__Resilience-Barometer__Description">
                            <?php echo $sections['resilienceBarometer']['description'] ?>
<!--                            <div class="Home__Resilience-Barometer__Description-Transparent">-->
<!---->
<!--                            </div>-->
<!--                            <div class="Home__Resilience-Barometer__Description__Icon">-->
<!--                                <img class="Home__Resilience-Barometer__Description__Icon-Image"-->
<!--                                     src="/content/img/load-more-down-arrow-v1.svg"/>-->
<!--                            </div>-->
                        </div>
                    </div>
                    <div class="offset-1"></div>
                    <div class="col-sm-4 pt-2">
                        <h3 class="Home__Resilience-Barometer__Counts-Title mb-0 text-center text-md-left">
                            <?php
                            echo $sections['resilienceBarometer']['countsTitle']
                            ?>
                        </h3>
                        <div class="Home__Resilience-Barometer__Counts">

                            <div class="Home__Resilience-Barometer__Counts-Count pt-0">
                                <?php
                                echo $sections['resilienceBarometer']['largeCompaniesCountDescription']
                                ?>
                                <span class="Home__Resilience-Barometer__Counts-Count-Number Counter Animate Animate--count-numbers"
                                      data-from="0" data-precision="10" data-to="<?php echo $sections['resilienceBarometer']['largeCompaniesCount']; ?>">
                                    0
                                </span>
                                <span class="Home__Resilience-Barometer__Counts-Count-Title">
                                <?php
                                echo $sections['resilienceBarometer']['largeCompaniesCountTitle']
                                ?>
                            </span>
                            </div>
                            <div class="Home__Resilience-Barometer__Counts-Count">
                                <?php
                                echo $sections['resilienceBarometer']['peopleCountDescription']
                                ?>
                                <span class="Home__Resilience-Barometer__Counts-Count-Number">
                                    <span class="Counter Animate Animate--count-numbers"
                                          data-from="0" data-precision="1" data-to="<?php echo $sections['resilienceBarometer']['peopleCount']; ?>">
                                        0
                                    </span>
                                million
                            </span>
                                <span class="Home__Resilience-Barometer__Counts-Count-Title">
                                <?php
                                echo $sections['resilienceBarometer']['peopleCountTitle']
                                ?>
                            </span>
                            </div>
                            <div class="Home__Resilience-Barometer__Counts-Count">
                                <?php
                                echo $sections['resilienceBarometer']['globalTurnoverCountDescription']
                                ?>
                                <span class="Home__Resilience-Barometer__Counts-Count-Number">
                                $<span class="Counter Animate Animate--count-numbers"
                                       data-from="0" data-precision="1" data-to="<?php echo $sections['resilienceBarometer']['globalTurnoverCount']; ?>">0</span> trillion
                            </span>
                                <span class="Home__Resilience-Barometer__Counts-Count-Title">
                                <?php
                                echo $sections['resilienceBarometer']['globalTurnoverCountTitle']
                                ?>
                            </span>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <section class="Home__Perspectives">
        <div class="container-large-md container">
            <div class="Home__Perspectives__Title">
                <h2 class="Home__Perspectives__Title-Text"><?php echo $sections['perspectives']['title']; ?></h2>
            </div>
            <div class="Home__Perspectives__Slider-Wrapper">

                <div class="Home__Perspectives__Slider-Wrapper__Slider">
                    <div class="swipper-container">
                        <div class="Home__Perspectives-Slider-Button button-previous">
                            <img src="/content/img/left-arrow-navigation-v1.svg"/>
                        </div>
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <div class="Home__Perspectives__Card">
                                    <div class="Home__Perspectives__Card__Header">
                                        <div class="Home__Perspectives__Card__Header-Image-Border"></div>
                                        <div class="Home__Perspectives__Card__Header-Image-Placeholder" style="background-size:cover; background-image: url(/content/img/Crop-4-01.png)"></div>

                                    </div>
                                    <div class="Home__Perspectives__Card__Description">
                                        <p>Anatomy of a Crisis I – Communicating through a crisis</p>
                                        <a class="Home__Perspectives__Card__Description-Link"
                                           href="/escalation-planning-and-response/anatomy-of-a-crisis-I">
                                            READ MORE
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="swiper-slide">
                                <div class="Home__Perspectives__Card">
                                    <div class="Home__Perspectives__Card__Header">
                                        <div class="Home__Perspectives__Card__Header-Image-Border"></div>
                                        <div class="Home__Perspectives__Card__Header-Image-Placeholder" style="background-size:cover; background-image: url(/content/img/Crop-5-01.png)"></div>

                                    </div>
                                    <div class="Home__Perspectives__Card__Description">
                                        <p>The Anatomy Of A Crisis: Volume 2 – Why Do Markets Overreact To Profit Warnings?</p>
                                        <a class="Home__Perspectives__Card__Description-Link"
                                           href="/escalation-planning-and-response/the-anatomy-of-a-crisis-volume-2">
                                            READ MORE
                                        </a>
                                    </div>
                                </div>
                            </div>


                            <div class="swiper-slide">
                                <div class="Home__Perspectives__Card">
                                    <div class="Home__Perspectives__Card__Header">
                                        <div class="Home__Perspectives__Card__Header-Image-Border"></div>
                                        <div class="Home__Perspectives__Card__Header-Image-Placeholder" style="background-size:cover; background-image: url(/content/img/Crop-2-01.png)"></div>

                                    </div>
                                    <div class="Home__Perspectives__Card__Description">
                                        <p>
                                            COVID-19: Is business ready for a second spike?
                                        </p>
                                        <a class="Home__Perspectives__Card__Description-Link"
                                           href="/escalation-planning-and-response/covid-19">
                                            READ MORE
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="swiper-slide">
                                <div class="Home__Perspectives__Card">
                                    <div class="Home__Perspectives__Card__Header">
                                        <div class="Home__Perspectives__Card__Header-Image-Border"></div>
                                        <div class="Home__Perspectives__Card__Header-Image-Placeholder" style="background-size:cover; background-image: url(/content/img/Crop-4-01.png)"></div>

                                    </div>
                                    <div class="Home__Perspectives__Card__Description">
                                        <p>COVID-19: Commercial Risk Mitigation & Damages Claims in Africa</p>
                                        <a class="Home__Perspectives__Card__Description-Link"
                                           href="/remediation-and-dispute-resolution/COVID-19:CommercialRiskMitigation&DamagesClaimsinAfrica">
                                            READ MORE
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="swiper-slide">
                                <div class="Home__Perspectives__Card">
                                    <div class="Home__Perspectives__Card__Header">
                                        <div class="Home__Perspectives__Card__Header-Image-Border"></div>
                                        <div class="Home__Perspectives__Card__Header-Image-Placeholder" style="background-size:cover; background-image: url(/content/img/Crop-5-01.png)"></div>

                                    </div>
                                    <div class="Home__Perspectives__Card__Description">
                                        <p>Decade of Disputes: The Trillion Dollar Investor View</p>
                                        <a class="Home__Perspectives__Card__Description-Link"
                                           href="/remediation-and-dispute-resolution/DecadeofDisputes:TheTrillionDollarInvestorView">
                                            READ MORE
                                        </a>
                                    </div>
                                </div>
                            </div>


                            <div class="swiper-slide">
                                <div class="Home__Perspectives__Card">
                                    <div class="Home__Perspectives__Card__Header">
                                        <div class="Home__Perspectives__Card__Header-Image-Border"></div>
                                        <div class="Home__Perspectives__Card__Header-Image-Placeholder" style="background-size:cover; background-image: url(/content/img/Crop-2-01.png)"></div>

                                    </div>
                                    <div class="Home__Perspectives__Card__Description">
                                        <p>
                                            Tackling financial crime: now is the time to break down silos
                                        </p>
                                        <a class="Home__Perspectives__Card__Description-Link"
                                           href="/remediation-and-dispute-resolution/Tackling-financial-crime:-now-is-the-time-to-break-down-silos">
                                            READ MORE
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="swiper-slide">
                                <div class="Home__Perspectives__Card">
                                    <div class="Home__Perspectives__Card__Header">
                                        <div class="Home__Perspectives__Card__Header-Image-Border"></div>
                                        <div class="Home__Perspectives__Card__Header-Image-Placeholder" style="background-size:cover; background-image: url(/content/img/Crop-4-01.png)"></div>

                                    </div>
                                    <div class="Home__Perspectives__Card__Description">
                                        <p>The CEO Brand and Its Impact On Business</p>
                                        <a class="Home__Perspectives__Card__Description-Link"
                                           href="/government-and-stakeholder-relations/The-CEO-Brand-and-Its-Impact-On-Business">
                                            READ MORE
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="swiper-slide">
                                <div class="Home__Perspectives__Card">
                                    <div class="Home__Perspectives__Card__Header">
                                        <div class="Home__Perspectives__Card__Header-Image-Border"></div>
                                        <div class="Home__Perspectives__Card__Header-Image-Placeholder" style="background-size:cover; background-image: url(/content/img/Crop-5-01.png)"></div>

                                    </div>
                                    <div class="Home__Perspectives__Card__Description">
                                        <p>Activism Vulnerability Report
                                        </p>
                                        <a class="Home__Perspectives__Card__Description-Link"
                                           href="/government-and-stakeholder-relations/Activism-Vulnerability-Report">
                                            READ MORE
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="swiper-slide">
                                <div class="Home__Perspectives__Card">
                                    <div class="Home__Perspectives__Card__Header">
                                        <div class="Home__Perspectives__Card__Header-Image-Border"></div>
                                        <div class="Home__Perspectives__Card__Header-Image-Placeholder" style="background-size:cover; background-image: url(/content/img/Crop-2-01.png)"></div>

                                    </div>
                                    <div class="Home__Perspectives__Card__Description">
                                        <p>
                                            The New T&Cs of Investor Relations An Open Letter to the FCO
                                        </p>
                                        <a class="Home__Perspectives__Card__Description-Link"
                                           href="/government-and-stakeholder-relations/The-New-T&Cs-of-Investor-Relations">
                                            READ MORE
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="Home__Perspectives__Card">
                                    <div class="Home__Perspectives__Card__Header">
                                        <div class="Home__Perspectives__Card__Header-Image-Border"></div>
                                        <div class="Home__Perspectives__Card__Header-Image-Placeholder" style="background-size:cover; background-image: url(/content/img/Crop-4-01.png)"></div>

                                    </div>
                                    <div class="Home__Perspectives__Card__Description">
                                        <p>
                                            Climate Image:<br/> A New Growth Trajectory?
                                        </p>
                                        <a class="Home__Perspectives__Card__Description-Link"
                                           href="/economic-impact-sustainability/climate-image-a-new-growth-trajectory">
                                            READ MORE
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="swiper-slide">
                                <div class="Home__Perspectives__Card">
                                    <div class="Home__Perspectives__Card__Header">
                                        <div class="Home__Perspectives__Card__Header-Image-Border"></div>
                                        <div class="Home__Perspectives__Card__Header-Image-Placeholder" style="background-size:cover; background-image: url(/content/img/Crop-5-01.png)"></div>

                                    </div>
                                    <div class="Home__Perspectives__Card__Description">
                                        <p>                    A Test of Resilience: COVID-19 and the Business of Europe’s Green Deal

                                        </p>
                                        <a class="Home__Perspectives__Card__Description-Link"
                                           href="/economic-impact-sustainability/A-Test-of-Resilience-COVID-19">
                                            READ MORE
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="swiper-slide">
                                <div class="Home__Perspectives__Card">
                                    <div class="Home__Perspectives__Card__Header">
                                        <div class="Home__Perspectives__Card__Header-Image-Border"></div>
                                        <div class="Home__Perspectives__Card__Header-Image-Placeholder" style="background-size:cover; background-image: url(/content/img/Crop-2-01.png)"></div>

                                    </div>
                                    <div class="Home__Perspectives__Card__Description">
                                        <p>
                                            Time to Rethink the ‘S’ in ESG
                                        </p>
                                        <a class="Home__Perspectives__Card__Description-Link"
                                           href="/economic-impact-sustainability/Time-to-Rethink-the-S-in-ESG">
                                            READ MORE
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="swiper-slide">
                                <div class="Home__Perspectives__Card">
                                    <div class="Home__Perspectives__Card__Header">
                                        <div class="Home__Perspectives__Card__Header-Image-Border"></div>
                                        <div class="Home__Perspectives__Card__Header-Image-Placeholder" style="background-size:cover; background-image: url(/content/img/Crop-4-01.png)"></div>

                                    </div>
                                    <div class="Home__Perspectives__Card__Description">
                                        <p>
                                            Rethinking RIF Workforce Optimization in Healthcare
                                        </p>
                                        <a class="Home__Perspectives__Card__Description-Link"
                                           href="/business-model-workforce-transformation/Rethinking-RIF-Workforce-Optimization-in-Healthcare">
                                            READ MORE
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="swiper-slide">
                                <div class="Home__Perspectives__Card">
                                    <div class="Home__Perspectives__Card__Header">
                                        <div class="Home__Perspectives__Card__Header-Image-Border"></div>
                                        <div class="Home__Perspectives__Card__Header-Image-Placeholder" style="background-size:cover; background-image: url(/content/img/Crop-5-01.png)"></div>

                                    </div>
                                    <div class="Home__Perspectives__Card__Description">
                                        <p>
                                            Toward a Brave New World: reboarding your workforce in the new normal.
                                        </p>
                                        <a class="Home__Perspectives__Card__Description-Link"
                                           href="/business-model-workforce-transformation/Toward-a-Brave-New-World:-reboarding-your-workforce-in-the-new-normal">
                                            READ MORE
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="swiper-slide">
                                <div class="Home__Perspectives__Card">
                                    <div class="Home__Perspectives__Card__Header">
                                        <div class="Home__Perspectives__Card__Header-Image-Border"></div>
                                        <div class="Home__Perspectives__Card__Header-Image-Placeholder" style="background-size:cover; background-image: url(/content/img/Crop-2-01.png)"></div>

                                    </div>
                                    <div class="Home__Perspectives__Card__Description">
                                        <p>
                                            The Case for Culture

                                        </p>
                                        <a class="Home__Perspectives__Card__Description-Link"
                                           href="/business-model-workforce-transformation/The-Case-for-Culture">
                                            READ MORE
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="swiper-slide">
                                <div class="Home__Perspectives__Card">
                                    <div class="Home__Perspectives__Card__Header">
                                        <div class="Home__Perspectives__Card__Header-Image-Border"></div>
                                        <div class="Home__Perspectives__Card__Header-Image-Placeholder" style="background-size:cover; background-image: url(/content/img/Crop-4-01.png)"></div>

                                    </div>
                                    <div class="Home__Perspectives__Card__Description">
                                        <p>
                                            The Anatomy of a Crisis #3 – Cyber Breaches
                                        </p>
                                        <a class="Home__Perspectives__Card__Description-Link"
                                           href="/digital-trust-and-ecosystems/The-Anatomy-of-a-Crisis-3-Cyber-Breaches">
                                            READ MORE
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="swiper-slide">
                                <div class="Home__Perspectives__Card">
                                    <div class="Home__Perspectives__Card__Header">
                                        <div class="Home__Perspectives__Card__Header-Image-Border"></div>
                                        <div class="Home__Perspectives__Card__Header-Image-Placeholder" style="background-size:cover; background-image: url(/content/img/Crop-5-01.png)"></div>

                                    </div>
                                    <div class="Home__Perspectives__Card__Description">
                                        <p>
                                            Not If But When: Building Cybersecurity Resilience to Secure Competitive Advantage
                                        </p>
                                        <a class="Home__Perspectives__Card__Description-Link"
                                           href="/digital-trust-and-ecosystems/Building-Cybersecurity-Resilience">
                                            READ MORE
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="swiper-slide">
                                <div class="Home__Perspectives__Card">
                                    <div class="Home__Perspectives__Card__Header">
                                        <div class="Home__Perspectives__Card__Header-Image-Border"></div>
                                        <div class="Home__Perspectives__Card__Header-Image-Placeholder" style="background-size:cover; background-image: url(/content/img/Crop-2-01.png)"></div>

                                    </div>
                                    <div class="Home__Perspectives__Card__Description">
                                        <p>
                                            The Case for Culture

                                        </p>
                                        <a class="Home__Perspectives__Card__Description-Link"
                                           href="/business-model-workforce-transformation/The-Case-for-Culture">
                                            READ MORE
                                        </a>
                                    </div>
                                </div>
                            </div>


                            <div class="swiper-slide">
                                <div class="Home__Perspectives__Card">
                                    <div class="Home__Perspectives__Card__Header">
                                        <div class="Home__Perspectives__Card__Header-Image-Border"></div>
                                        <div class="Home__Perspectives__Card__Header-Image-Placeholder" style="background-size:cover; background-image: url(/content/img/Crop-4-01.png)"></div>

                                    </div>
                                    <div class="Home__Perspectives__Card__Description">
                                        <p>Anatomy of a Crisis I – Communicating through a crisis</p>
                                        <a class="Home__Perspectives__Card__Description-Link"
                                           href="/digital-trust-and-ecosystems/The-Anatomy-of-a-Crisis-3-Cyber-Breaches">
                                            READ MORE
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="swiper-slide">
                                <div class="Home__Perspectives__Card">
                                    <div class="Home__Perspectives__Card__Header">
                                        <div class="Home__Perspectives__Card__Header-Image-Border"></div>
                                        <div class="Home__Perspectives__Card__Header-Image-Placeholder" style="background-size:cover; background-image: url(/content/img/Crop-5-01.png)"></div>

                                    </div>
                                    <div class="Home__Perspectives__Card__Description">
                                        <p>The Anatomy of a Crisis #3 – Cyber Breaches
                                        </p>
                                        <a class="Home__Perspectives__Card__Description-Link"
                                           href="/digital-trust-and-ecosystems/COVID-19-New-Cyber-Threats">
                                            READ MORE
                                        </a>
                                    </div>
                                </div>
                            </div>


                            <div class="swiper-slide">
                                <div class="Home__Perspectives__Card">
                                    <div class="Home__Perspectives__Card__Header">
                                        <div class="Home__Perspectives__Card__Header-Image-Border"></div>
                                        <div class="Home__Perspectives__Card__Header-Image-Placeholder" style="background-size:cover; background-image: url(/content/img/Crop-2-01.png)"></div>

                                    </div>
                                    <div class="Home__Perspectives__Card__Description">
                                        <p>
                                            Not If But When: Building Cybersecurity Resilience to Secure Competitive Advantage
                                        </p>
                                        <a class="Home__Perspectives__Card__Description-Link"
                                           href="/digital-trust-and-ecosystems/Building-Cybersecurity-Resilience">
                                            READ MORE
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="Home__Perspectives-Slider-Button button-next">
                            <img src="/content/img/right-arrow-navigation-v1.svg"/>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <section class="Home__Join-Conversation" style="background-image: url(content/img/latest-news-shard-detail-v1.svg); background-position: center 30px; background-repeat: no-repeat; background-size: cover;">
        <div class="container-large-md container">
            <h1 class="Home__Join-Conversation-Title"><?php echo $sections['joinConversation']['title']; ?></h1>
            <a class="Home__Join-Conversation-Link" href="<?php echo $sections['joinConversation']['linkUrl']; ?>"
               class="Home__Join-Conversation-Title"><?php echo $sections['joinConversation']['linkText']; ?></a>
        </div>
    </section>
</main>
