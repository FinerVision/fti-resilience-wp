<main class="Home">
    <?php include 'perspectiveheader.php'; ?>

    <div class="Section--background-perspectives">
        <div class="container-large-md container">
            <div class="row">
                <div class="col-sm-12 col-md-8 Section--bg-white py-5">
                    <div class="Title--type-2 color-dark-blue mb-4">XX JULY 2020</div>
                    <hr class="HR HR--type-1">
                    <h1 class="Title--type-7 mt-3">
                        Activism Vulnerability Report
                    </h1>
                    <b>
                        With the rise of shareholder activism in recent years and its evolution into an asset class of
                        its own, companies should prepare themselves for targeted shareholder activity by assessing
                        their vulnerability to such actions.
                    </b>
                    <p>
                        To accomplish this, FTI Consulting built the Activism Screener (the “Screener”), a proprietary
                        model that evaluates U.S. and Canadian public companies’ vulnerability to shareholder activist
                        pressures, based on a variety of quarterly reported metrics. The Screener was built and refined
                        in 2017 and 2018 in collaboration with experts across FTI Consulting’s practices, drawing
                        expertise from corporate finance, corporate governance, and forensic and technical accounting
                        professionals.
                    </p>
                    <p>
                        While some industries are more susceptible than others, every company needs to be cognizant of
                        its key vulnerabilities and how it stands compared to industry peers. To help predict activist
                        targets, FTI Consulting has built a proprietary Activism Screener that uses a variety of
                        quarterly reported metrics to determine the vulnerability of U.S. and Canadian public companies
                        to shareholder activist pressures. This issue of FTI Consulting’s quarterly Activism
                        Vulnerability Report will summarize the Screener’s results and evaluate 25 well-known industries
                        based upon an average of the aggregate vulnerability scores of the components of each industry.
                        FTI Consulting plans to publish the Report quarterly as financial data and other common publicly
                        disclosed information become available.
                    </p>
                    <p>
                        This report provides a starting point for companies and their advisors seeking to locate their
                        most prevalent vulnerabilities from an external perspective. Contact us if you would like to
                        discuss how to create a long-term, actionable plan to address these vulnerabilities and to
                        increase the confidence of all investors, including those not yet present.
                    </p>
                </div>
                <div class="col-sm-12 col-md-4 Section--sidebar">
                    <div class="Title--type-2 color-dark-blue mb-4">&nbsp;</div>
                    <div class="SocialBox">
                        SHARE THIS CONTENT
                        <ul class="SocialShare SocialShare--align-left mt-3">
                            <li class="SocialShare__Item">
                                <a class="SocialShare__Item__Link" href="#" target="_blank">
                                    <img class="SocialShare__Item__Link-Icon" src="../assets/img/facebook-icon-v2.svg">
                                </a>
                            </li>
                            <li class="SocialShare__Item">
                                <a class="SocialShare__Item__Link" href="#" target="_blank">
                                    <img class="SocialShare__Item__Link-Icon" src="../assets/img/twitter-icon-v2.svg">
                                </a>
                            </li>
                            <li class="SocialShare__Item">
                                <a class="SocialShare__Item__Link" href="#" target="_blank">
                                    <img class="SocialShare__Item__Link-Icon" src="../assets/img/linkedin-icon-v2.svg">
                                </a>
                            </li>
                            <li class="SocialShare__Item">
                                <a class="SocialShare__Item__Link" href="#" target="_blank">
                                    <img class="SocialShare__Item__Link-Icon" src="../assets/img/share-icon-v1.svg">
                                </a>
                            </li>
                        </ul>
                    </div>

                    <div class="Document mt-5">
                        <img src="../assets/img/documents/activism-vulnerability-report.png" class="img-fluid"/>
                    </div>

                    <a href="../assets/img/documents/activism-vulnerability-report.pdf" target="_blank"
                       class="Button Button--type-1 mt-3">
                        DOWNLOAD >
                    </a>
                </div>
            </div>
        </div>
    </div>

    <?php include 'exploreothertopics.php'; ?>
</main>
