<main class="Home">
    <?php include 'perspectiveheader.php'; ?>

    <div class="Section--background-perspectives">
        <div class="container-large-md container">
            <div class="row">
                <div class="col-sm-12 col-md-8 Section--bg-white py-5">
                    <div class="Title--type-2 color-dark-blue mb-4">XX JULY 2020</div>
                    <hr class="HR HR--type-1">
                    <h1 class="Title--type-7 mt-3">
                        3 Approaches to IP Valuation for Pandemic Recovery
                    </h1>
                    <p>
                        The COVID-19 pandemic sent shockwaves around the globe, many of which still reverberate. Facing
                        a public health crisis, many countries, including the U.S., enacted strict measures to slow the
                        viral spread. While closures, shelter in place orders, and social distancing efforts may have
                        lessened the infection count, an economic cost was extracted.
                    </p>
                    <p>
                        As unemployment levels soared, other unintended consequences manifested including the crippling
                        of many businesses, both large and small. Unfortunately, in some cases, these entities will
                        shutter permanently.
                    </p>
                    <p>
                        As companies attempt to climb out of the COVID-19 inflicted rubble, difficult choices will be
                        weighed. For some, these alternatives may include bankruptcy or restructuring. For others, a
                        merger or acquisition may be on the horizon, whether welcomed or not.

                    </p>
                    <p>
                        Other options may encompass a sale or licensing of assets for a needed cash infusion. Regardless
                        of which path is taken, a rigorous valuation of a company’s intellectual property will likely be
                        required.

                    </p>
                    <p>
                        For many U.S. businesses, one of the most valuable assets on the balance sheet is intellectual
                        property. In fact, in many cases, various forms of intellectual property and intellectual
                        capital provide the competitive advantage necessary for success.

                    </p>
                    <p>
                        As such, assigning a reasonable value to these differentiating assets is paramount in a
                        restructuring, sale, or licensing situation, in order to effectively capture the intellectual
                        property’s monetary worth. Understanding the potential impact of the COVID-19 pandemic may prove
                        beneficial, if not crucial, in determining an intellectual property asset’s value.

                    </p>
                </div>
                <div class="col-sm-12 col-md-4 Section--sidebar">
                    <div class="Title--type-2 color-dark-blue mb-4">&nbsp;</div>
                    <div class="SocialBox">
                        SHARE THIS CONTENT
                        <ul class="SocialShare SocialShare--align-left mt-3">
                            <li class="SocialShare__Item">
                                <a class="SocialShare__Item__Link" href="#" target="_blank">
                                    <img class="SocialShare__Item__Link-Icon" src="../assets/img/facebook-icon-v2.svg">
                                </a>
                            </li>
                            <li class="SocialShare__Item">
                                <a class="SocialShare__Item__Link" href="#" target="_blank">
                                    <img class="SocialShare__Item__Link-Icon" src="../assets/img/twitter-icon-v2.svg">
                                </a>
                            </li>
                            <li class="SocialShare__Item">
                                <a class="SocialShare__Item__Link" href="#" target="_blank">
                                    <img class="SocialShare__Item__Link-Icon" src="../assets/img/linkedin-icon-v2.svg">
                                </a>
                            </li>
                            <li class="SocialShare__Item">
                                <a class="SocialShare__Item__Link" href="#" target="_blank">
                                    <img class="SocialShare__Item__Link-Icon" src="../assets/img/share-icon-v1.svg">
                                </a>
                            </li>
                        </ul>
                    </div>

                    <div class="Document mt-5">
                        <img src="../assets/img/documents/covid-19-3-approaches-ip-valuation-pandemic-recovery.png"
                             class="img-fluid"/>
                    </div>

                    <a href="../assets/img/documents/covid-19-3-approaches-ip-valuation-pandemic-recovery.pdf"
                       target="_blank" class="Button Button--type-1 mt-3">
                        DOWNLOAD >
                    </a>
                </div>
            </div>
        </div>
    </div>

    <?php include 'exploreothertopics.php'; ?>
</main>
