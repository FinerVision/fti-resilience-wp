<main class="Home">
    <?php include 'perspectiveheader.php'; ?>

    <div class="Section--background-perspectives">
        <div class="container-large-md container">
            <div class="row">
                <div class="col-sm-12 col-md-8 Section--bg-white py-5">
                    <div class="Title--type-2 color-dark-blue mb-4">XX JULY 2020</div>
                    <hr class="HR HR--type-1">
                    <h1 class="Title--type-7 mt-3">
                        A Test of Resilience: <br/>COVID-19 and the Business of Europe’s Green Deal

                    </h1>
                    <b>
                        The impact of the global COVID-19 pandemic is clearly enormous in health, social and economic
                        terms. It has also had an immediate environmental impact which may play out over the longer
                        term, with a number of political repercussions.
                    </b>
                    <p>
                        How resilient is the European Union’s Green Deal proving to be? What are the implications for
                        business, policy-makers and other stakeholders to this strategic initiative which seeks to make
                        Europe climate neutral by 2050 strategy? The impact could have global consequences.
                    </p>
                    <b>Coronavirus as a sustainability challenge</b>
                    <p>
                        If the notion of ‘business as usual’ as we entered the 2020s had any residual meaning, it must
                        surely have lost it in the wake of COVID-19. The impact of the global pandemic is clearly
                        enormous in health, social and economic terms.
                    </p>
                    <p>
                        Even if different countries and regions are experiencing its grim human effects in different
                        ways, depending on the timing and extent of public health responses and their effectiveness, the
                        degree to which social and economic activity has shut down is significant almost everywhere.

                    </p>
                    <p>
                        With the prospect that restrictions are maintained in some form for many months to come, global
                        financial markets are anticipating a depression as severe as in the 1930s, and perhaps even more
                        so in some countries – even taking into account the historic commitments from governments and
                        financial institutions to construct emergency economic survival and recovery packages.
                    </p>

                </div>
                <div class="col-sm-12 col-md-4 Section--sidebar">
                    <div class="Title--type-2 color-dark-blue mb-4">&nbsp;</div>
                    <div class="SocialBox">
                        SHARE THIS CONTENT
                        <ul class="SocialShare SocialShare--align-left mt-3">
                            <li class="SocialShare__Item">
                                <a class="SocialShare__Item__Link" href="#" target="_blank">
                                    <img class="SocialShare__Item__Link-Icon" src="../assets/img/facebook-icon-v2.svg">
                                </a>
                            </li>
                            <li class="SocialShare__Item">
                                <a class="SocialShare__Item__Link" href="#" target="_blank">
                                    <img class="SocialShare__Item__Link-Icon" src="../assets/img/twitter-icon-v2.svg">
                                </a>
                            </li>
                            <li class="SocialShare__Item">
                                <a class="SocialShare__Item__Link" href="#" target="_blank">
                                    <img class="SocialShare__Item__Link-Icon" src="../assets/img/linkedin-icon-v2.svg">
                                </a>
                            </li>
                            <li class="SocialShare__Item">
                                <a class="SocialShare__Item__Link" href="#" target="_blank">
                                    <img class="SocialShare__Item__Link-Icon" src="../assets/img/share-icon-v1.svg">
                                </a>
                            </li>
                        </ul>
                    </div>

                    <div class="Document mt-5">
                        <img
                            src="../assets/img/documents/200422 FTI consulting - Coronavirus and the EU's Green Deal (FINAL).png"
                            class="img-fluid"/>
                    </div>

                    <a href="../assets/img/documents/200422 FTI consulting - Coronavirus and the EU's Green Deal (FINAL).pdf"
                       target="_blank" class="Button Button--type-1 mt-3">
                        DOWNLOAD >
                    </a>
                </div>
            </div>
        </div>
    </div>

    <?php include 'exploreothertopics.php'; ?>
</main>
