<main class="Home">
    <?php include 'perspectiveheader.php'; ?>

    <div class="Section--background-perspectives">
        <div class="container-large-md container">
            <div class="row">
                <div class="col-sm-12 col-md-8 Section--bg-white py-5">
                    <div class="Title--type-2 color-dark-blue mb-4">XX JULY 2020</div>
                    <hr class="HR HR--type-1">
                    <h1 class="Title--type-7 mt-3">
                        COVID-19: New Cyber Threats
                    </h1>
                    <b>
                        While we all adjust to the new world of social distancing and working remotely as a result of
                        the coronavirus disease (COVID-19), it is important to not let our guard down when it comes to
                        cyber risks. FTI Cybersecurity is here to help navigate this new normal and assist with our
                        collective security.
                    </b>
                    <p>
                        Several new scams have emerged, including mass phishing and malware attacks involving emails
                        designed to appear as legitimate messages from medical or health organizations. These emails
                        contain malware-laden attachments designed to either harvest credentials or infect systems. The
                        World Health Organization has released warnings about this threat.
                    </p>
                    <p><b>Advanced Cyber Attacks</b></p>
                    <p>
                        Concerningly, cyber criminals have upped their efforts beyond using a “spray and pray” approach
                        typically seen with phishing emails. A recent threat involves distributing legitimate
                        information regarding worldwide COVID-19 infection rates that secretly spreads malware to
                        unknowing computers.
                    </p>
                    <p>
                        “In one scheme, an interactive dashboard of coronavirus infections and deaths produced by Johns
                        Hopkins University is being used in malicious Web sites (and possibly spam emails) to spread
                        password-stealing malware.”
                    </p>
                    <p>
                        Furthering the issue, Russian cyber criminals are selling a “coronavirus infection kit” that
                        uses the Hopkins interactive map as part of a Java-based malware deployment scheme. The use of
                        real-time data and an interactive map makes the scam believable, especially when individuals are
                        less guarded due to the uneasiness of dealing with a pandemic.
                    </p>
                </div>
                <div class="col-sm-12 col-md-4 Section--sidebar">
                    <div class="Title--type-2 color-dark-blue mb-4">&nbsp;</div>
                    <div class="SocialBox">
                        SHARE THIS CONTENT
                        <ul class="SocialShare SocialShare--align-left mt-3">
                            <li class="SocialShare__Item">
                                <a class="SocialShare__Item__Link" href="#" target="_blank">
                                    <img class="SocialShare__Item__Link-Icon" src="../assets/img/facebook-icon-v2.svg">
                                </a>
                            </li>
                            <li class="SocialShare__Item">
                                <a class="SocialShare__Item__Link" href="#" target="_blank">
                                    <img class="SocialShare__Item__Link-Icon" src="../assets/img/twitter-icon-v2.svg">
                                </a>
                            </li>
                            <li class="SocialShare__Item">
                                <a class="SocialShare__Item__Link" href="#" target="_blank">
                                    <img class="SocialShare__Item__Link-Icon" src="../assets/img/linkedin-icon-v2.svg">
                                </a>
                            </li>
                            <li class="SocialShare__Item">
                                <a class="SocialShare__Item__Link" href="#" target="_blank">
                                    <img class="SocialShare__Item__Link-Icon" src="../assets/img/share-icon-v1.svg">
                                </a>
                            </li>
                        </ul>
                    </div>

                    <div class="Document mt-5">
                        <img src="../assets/img/documents/covid-19-new-cyber-threats.png" class="img-fluid"/>
                    </div>

                    <a href="../assets/img/documents/covid-19-new-cyber-threats.pdf" target="_blank"
                       class="Button Button--type-1 mt-3">
                        DOWNLOAD >
                    </a>
                </div>
            </div>
        </div>
    </div>

    <?php include 'exploreothertopics.php'; ?>
</main>
