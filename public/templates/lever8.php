<main class="real-time-data-analytics">
    <div class="Background--Gradient-Blue-Black">
        <?php
        require __DIR__ . '/../components/leverCarousel.php'; ?>
    </div>

    <div class="container-large-md container mt-3 mt-lg-5">
    </div>
    <section class="Section--decoration-right">
        <div class="container-large-md container">
            <div class="row pt-0 pt-lg-5">
                <div class="col-sm-12 col-md-6">
                    <h2 class="Title--type-1 mb-3">
                        The most successful organisations in coming years will be those that
                        recognise cyber resilience as a vital source of competitive advantage – but for now,
                        cybersecurity strategy could be key to survival in a world disrupted by COVID-19.
                    </h2>
                </div>
                <div class="col-sm-12 col-md-6">
                    <div class="mw-300px mx-auto">
                        <p class="mb-1 Paragraph--type-2 mt-5">
                            More than
                        </p>
                        <div class="Number">
                            1 in 5
                        </div>
                        <p class="mt-1 Paragraph--type-2">
                            business leaders surveyed hadlost customer informationthrough cybersecurity incidents
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="Section--decoration-left mb-5">
        <div class="container-large-md container">
            <div class="row mt-5">
                <div class="col-sm-12 col-md-6 pl-md-5 x-y-center">
                    <div class="mw-300px mx-auto">
                        <p class="mt-0 mb-0 Paragraph--type-2">
                            Companies receive
                        </p>
                        <div class="Number">
                            5x
                        </div>
                        <p class="mt-0 mb-2 Paragraph--type-2">
                            he media attention in the month following a cyber breach, and
                        </p>
                        <div class="Number">
                            8x
                        </div>
                        <p class="mt-0 Paragraph--type-2">
                            the social media coverage
                        </p>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6">
                    <p class="Paragraph--type-3">
                        Cyber attacks were already frequent before the outbreak of COVID-19. According to FTI
                        Consulting’s Resilience Barometer™ 2020 (published in January), 20% of organisations had been
                        victims of a ransom or data hostage situation.

                    </p>
                    <p class="Paragraph--type-3">
                        The ongoing digitisation of business means cyber attacks can inflict
                        damage across commercial ecosystems. Of companies experiencing
                        cyber attacks, 27% lose revenue and similar proportions lose assets or
                        customer information. Reputational harm is another danger. FTI research
                        indicates that on average, a company receives five times more media
                        coverage and eight times more social media coverage in the month aer
                        a cyber breach than in normal conditions.
                    </p>
                    <p class="Paragraph--type-3">
                        Despite these risks, a worrying 90% of Barometer respondents reported gaps in their
                        organisations’ cybersecurity defences. The signs are that the situation has worsened during the
                        pandemic. An ICS survey found 47% of cybersecurity staff diverted to enabling remote working,
                        with an apparently associated increase in cybersecurity incidents.
                    </p>
                </div>
            </div>
        </div>

    </section>
    <section class="Section--background-gray pt-3 pb-5">
        <div class="container-large-md container">
            <div class="row my-3"></div>
            <div class="row">
                <div class="col-sm-12 mb-5">
                    <div class="Title--type-2 text-uppercase">
                        Building sustainability into the resilience strategy
                    </div>
                </div>
                <div class="col-sm-12 col-md-6">
                    <p class="Paragraph--type-3">
                        With ESG performance intrinsic to stakeholder confidence and financial
                        viability, business’s ability to weather future crises will depend heavily on
                        satisfying stakeholder expectations in this area. Sustainability must
                        therefore be viewed as an important element of resilience.
                    </p>
                    <p class="Paragraph--type-3">
                        Companies need to look well beyond mere regulatory compliance: they
                        need to anticipate and adjust to shis in government policy and in
                        shareholder and public attitudes, while remaining profitable. The
                        necessary insights into emerging attitudes can only be gained through
                        ongoing stakeholder engagement and communication.
                    </p>
                </div>
                <div class="col-sm-12 col-md-6">
                    <p class="Paragraph--type-3">
                        This communication must be two way, of course. Corporate reporting needs to address
                        sustainability issues fully: the ESG Resilience Compass 2020 found that 88% of institutional
                        investors believed there should be more reporting on the actual impact of businesses’
                        activities.
                    </p>
                    <p class="Paragraph--type-3">
                        Whatever the business’s size, industry or location, sustainability isan important tool in
                        building resilience and protecting value fortoday and tomorrow.
                    </p>
                </div>

            </div>
            <div class="row mt-5 py-5">
                <div class="col">
                    <div class="Number">
                        90%
                        <div class="Number__bar bar--white">
                            <div class="Number__bar-full" style="width: 90%">
                            </div>
                        </div>
                        <div class="Paragraph--type-2 mt-3">
                            of executives had experienced mental health problems as a result of dealing with crises
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="Number">
                        20%
                        <div class="Number__bar bar--white">
                            <div class="Number__bar-full" style="width: 20%">

                            </div>
                        </div>
                        <div class="Paragraph--type-2 mt-3">
                            of investors think companies can manage crises effectively
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="Number">
                        35%
                        <div class="Number__bar bar--white">
                            <div class="Number__bar-full" style="width: 35%">

                            </div>
                        </div>
                        <div class="Paragraph--type-2 mt-3">
                            of respondents had invested in crisis management in 2019
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="Section__triangle Section__triangle--type-7">
        <div class="container-large-md container py-5">
            <div class="row mt-5">
                <div class="col-sm-12 text-center">
                    <div class="Title--type-2 text-white">
                        COMPETE
                    </div>
                    <div class="Paragraph--type-1 text-white">
                        Given the inevitability of cybersecurity<br/>
                        incidents, the entire workforce should be<br/>
                        recruited to build cyber resilience. Increasing<br/>
                        the maturity of the cybersecurity approach in<br/>
                        this way will create competitive advantage by<br/>
                        positioning the business to seize new<br/>
                        opportunities and grow securely.
                    </div>
                </div>
            </div>
            <div class="row py-5 my-5"></div>
            <div class="row mb-5 mt-150px">
                <div class="col-sm-12 col-md-4 text-center">
                    <div class="mw-360px mx-auto">
                        <div class="Title--type-2 text-white text-right">
                            ADAPT
                        </div>
                        <div class="Paragraph--type-1 text-right">
                            Cybersecurity policies, processes and procedures<br/>
                            must adapt to new working practices. Consider<br/>
                            which threats have increased, whether the<br/>
                            incident response plan is compatible with<br/>
                            remote working, and how far third-party<br/>
                            suppliers comply with cybersecurity policies.
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-4"></div>
                <div class="col-sm-12 col-md-4 text-center">
                    <div class="mw-360px mx-auto">
                        <div class="Title--type-2 text-white text-left">
                            PROTECT
                        </div>
                        <div class="Paragraph--type-1 text-left">
                            Today’s threat landscape is evolving rapidly: for<br/>
                            example, remote working may dictate a heavier<br/>
                            reliance on employee diligence, together with<br/>
                            some acceptance of shadow IT. An emergency<br/>
                            cybersecurity risk assessment is key to<br/>
                            protecting your business, employees and<br/>
                            technical infrastructure.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <section class="Home__Perspectives">
        <div class="container-large-md container">
            <div class="Home__Perspectives__Title">
                <h2 class="Home__Perspectives__Title-Text"><?php echo $sections['perspectives']['title']; ?></h2>
            </div>
            <div class="Home__Perspectives__Slider-Wrapper">

                <div class="Home__Perspectives__Slider-Wrapper__Slider">
                    <div class="swipper-container">
                        <div class="Home__Perspectives-Slider-Button button-previous">
                            <img src="/content/img/left-arrow-navigation-v1.svg"/>
                        </div>
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <div class="Home__Perspectives__Card">
                                    <div class="Home__Perspectives__Card__Header">
                                        <div class="Home__Perspectives__Card__Header-Image-Border border-line--type-8"></div>
                                        <div class="Home__Perspectives__Card__Header-Image-Placeholder" style="background-size:cover; background-image: url(/content/img/Crop-4-01.png)"></div>

                                    </div>
                                    <div class="Home__Perspectives__Card__Description">
                                        <p class="color-real-time">How Will Data Fare in the Global Fight Against the Pandemic?</p>
                                        <a class="color-real-time Home__Perspectives__Card__Description-Link"
                                           href="/real-time-data-analytics/How-Will-Data-Fare-in-the-Global-Fight-Against-the-Pandemic">
                                            READ MORE
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="swiper-slide">
                                <div class="Home__Perspectives__Card">
                                    <div class="Home__Perspectives__Card__Header">
                                        <div class="Home__Perspectives__Card__Header-Image-Border border-line--type-8"></div>
                                        <div class="Home__Perspectives__Card__Header-Image-Placeholder" style="background-size:cover; background-image: url(/content/img/Crop-5-01.png)"></div>

                                    </div>
                                    <div class="Home__Perspectives__Card__Description">
                                        <p class="color-real-time">E-discovery in 2020: COVID-19’s Impact on Our Data Footprint
                                        </p>
                                        <a class="color-real-time Home__Perspectives__Card__Description-Link"
                                           href="/real-time-data-analytics/COVID-19-Impact-on-Our-Data-Footprint">
                                            READ MORE
                                        </a>
                                    </div>
                                </div>
                            </div>


                            <div class="swiper-slide">
                                <div class="Home__Perspectives__Card">
                                    <div class="Home__Perspectives__Card__Header">
                                        <div class="Home__Perspectives__Card__Header-Image-Border border-line--type-8"></div>
                                        <div class="Home__Perspectives__Card__Header-Image-Placeholder" style="background-size:cover; background-image: url(/content/img/Crop-2-01.png)"></div>

                                    </div>
                                    <div class="Home__Perspectives__Card__Description">
                                        <p class="color-real-time">
                                            A new era of data analytics for investigations and litigation
                                        </p>
                                        <a class="color-real-time Home__Perspectives__Card__Description-Link"
                                           href="/real-time-data-analytics/A-new-era-of-data-analytics-for-investigations-and-litigation">
                                            READ MORE
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="Home__Perspectives-Slider-Button button-next">
                            <img src="/content/img/right-arrow-navigation-v1.svg"/>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <?php include 'exploreanotherlever.php'; ?>
</main>
