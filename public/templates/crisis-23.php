<main class="Home">
    <?php include 'perspectiveheader.php'; ?>

    <div class="Section--background-perspectives">
        <div class="container-large-md container">
            <div class="row">
                <div class="col-sm-12 col-md-8 Section--bg-white py-5">
                    <div class="Title--type-2 color-dark-blue mb-4">XX JULY 2020</div>
                    <hr class="HR HR--type-1">
                    <h1 class="Title--type-7 mt-3">
                        How Will Data Fare in the Global Fight Against the Pandemic?
                    </h1>
                    <b>
                        Data-harnessing companies on both sides of the Atlantic face immense demands and
                        expectations. Public authorities and wider society expect data-harnessing companies to act in
                        fighting against COVID-19, especially through tracing app initiatives.
                    </b>
                    <p>
                        Emerging business partnerships are proactively combining expertise and harnessing data to
                        provide
                        solutions that mitigate or manage the public health crisis, and steer towards socio-economic
                        recovery. Public concerns and regulatory developments on privacy may evolve to account for
                        data’s
                        immediate purpose of safeguarding public health. </p>
                    <p>
                        Governments are now turning to private companies to seek input and collaboration in leveraging
                        the
                        growing potential and inclusion of data. As all market sectors mobilise resources against the
                        gravest
                        global crisis of our time, data-harnessing companies are equally obligated to be proactive.
                    </p>
                    <p>
                        However, as the pandemic starts to level off from its peak, governments may become dependent
                        and wary in equal measure of the power of data as a commodity and what this means for their
                        approach to data-harnessing companies. One outcome could be greater monitoring and scrutiny,
                        and it will be essential for business to maintain and build on existing dialogue with
                        policymakers and
                        regulators.
                    </p>
                    <p>
                        Recent years have seen data harnessing companies thrown into the spotlight for the perceived
                        risk
                        of compromise that their use of data represents for citizen privacy. Today though, that concern
                        sits
                        in stark opposition to the potential offer of safety and security that data-harnessing companies
                        can
                        make to citizens via tracing apps that track and limit the transmission of COVID-19.
                    </p>
                    <p>
                        Countries face the choice of following or diverging from existing data rules to tackle the
                        pandemic
                        and seek to incorporate new health-related functions for gathering and managing data within
                        their
                        national frameworks. Both options have elicited controversy.
                    </p>
                </div>
                <div class="col-sm-12 col-md-4 Section--sidebar">
                    <div class="Title--type-2 color-dark-blue mb-4">&nbsp;</div>
                    <div class="SocialBox">
                        SHARE THIS CONTENT
                        <ul class="SocialShare SocialShare--align-left mt-3">
                            <li class="SocialShare__Item">
                                <a class="SocialShare__Item__Link" href="#" target="_blank">
                                    <img class="SocialShare__Item__Link-Icon" src="../assets/img/facebook-icon-v2.svg">
                                </a>
                            </li>
                            <li class="SocialShare__Item">
                                <a class="SocialShare__Item__Link" href="#" target="_blank">
                                    <img class="SocialShare__Item__Link-Icon" src="../assets/img/twitter-icon-v2.svg">
                                </a>
                            </li>
                            <li class="SocialShare__Item">
                                <a class="SocialShare__Item__Link" href="#" target="_blank">
                                    <img class="SocialShare__Item__Link-Icon" src="../assets/img/linkedin-icon-v2.svg">
                                </a>
                            </li>
                            <li class="SocialShare__Item">
                                <a class="SocialShare__Item__Link" href="#" target="_blank">
                                    <img class="SocialShare__Item__Link-Icon" src="../assets/img/share-icon-v1.svg">
                                </a>
                            </li>
                        </ul>
                    </div>

                    <div class="Document mt-5">
                        <img src="../assets/img/documents/covid-19-how-data-fare-global-fight-against-pandemic.png"
                             class="img-fluid"/>
                    </div>

                    <a href="../assets/img/documents/covid-19-how-data-fare-global-fight-against-pandemic.pdf"
                       target="_blank" class="Button Button--type-1 mt-3">
                        DOWNLOAD >
                    </a>
                </div>
            </div>
        </div>
    </div>

    <?php include 'exploreothertopics.php'; ?>
</main>
