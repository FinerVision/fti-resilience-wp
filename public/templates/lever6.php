<main class="operational-and-financial-resilience">
    <div class="Background--Gradient-Blue-Black">
        <?php
        require __DIR__ . '/../components/leverCarousel.php'; ?>
    </div>

    <div class="container-large-md container mt-3 mt-lg-5">
    </div>
    <section class="Section--decoration-right">
        <div class="container-large-md container">
            <div class="row pt-0 pt-lg-5">
                <div class="col-sm-12 col-md-6">
                    <h2 class="Title--type-1 mb-3">Surviving a post-pandemic<br/>
                        downturn is not enough.<br/>
                        Businesses should be building<br/>
                        robust new models to ensure<br/>
                        long-term resilience.
                    </h2>

                </div>
                <div class="col-sm-12 col-md-6">
                    <div class="mw-300px mx-auto">
                        <p class="mt-3 Paragraph--type-2 mb-0">
                            Institutional investors attribute an average of
                        </p>
                        <div class="Number">
                            36%
                        </div>
                        <p class="mb-0 Paragraph--type-2 mt-negative-5px">
                            increased value in companies as a result of having a very positive ESG rating
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="Section--decoration-left mb-5">
        <div class="container-large-md container">
            <div class="row mt-5">
                <div class="col-sm-12 col-md-6 pl-md-5">
                    <div class="mw-300px mx-auto">
                        <p class="mt-3 mb-0 Paragraph--type-2">
                            Pre-COVID, more than
                        </p>
                        <div class="Number">
                            1 in 5
                        </div>
                        <p class="mb-0 Paragraph--type-2">
                            business leaders surveyed thought the crisis events with the biggest impact on turnover were major product defects and cyber attacks
                        </p>
                        <p class="mt-3 mb-0 Paragraph--type-2">
                            Pre-COVID, only
                        </p>
                        <div class="Number">
                            7%
                            <div class="Number__bar">
                                <div class="Number__bar-full" style="width: 7%">

                                </div>
                            </div>
                        </div>
                        <p class="mt-3 Paragraph--type-2">
                            of business leaders expected to deal with issues arising from bad debt to be an issue in 2020
                        </p>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6">
                    <p class="Paragraph--type-3">
                        The scale of the impending financial crisis following COVID-19 is not yet
                        known, but there are signs of a long winter to come. Estimates of
                        unsustainable corporate debt in the UK alone exceed £100bn.The Bank of
                        England predicts an aggregate cash flow deficit of around £140bn from
                        Q2 2020 to Q1 2021 – a type of problem anticipated by only 7% of
                        respondents to FTI’s January 2020 Resilience Barometer<sup>TM</sup> .

                    </p>
                    <p class="Paragraph--type-3">
                        Operational risks present before the pandemic have been amplified. The
                        Barometer revealed cyber-attacks and product defects as the biggest
                        threats to turnover (each named by 11% of respondents). Now COVID-19
                        has added complexities of its own to the picture. These include
                        disturbances to the supply chain and general operational disruption
                        stemming from social distancing and hygiene measures – not least the
                        move to mass working from home.
                    </p>
                </div>
            </div>
        </div>

    </section>
    <section class="Section--background-gray pt-3 pb-5">
        <div class="container-large-md container">
            <div class="row my-3"></div>
            <div class="row">
                <div class="col-sm-12 mb-5">
                    <div class="Title--type-2">
                        FUTURE-PROOFING THE BUSINESS
                    </div>
                </div>
                <div class="col-sm-12 col-md-6">
                    <p class="Paragraph--type-3">
                        Most businesses’ focus is currently on protecting short-term viability, but
                        equal attention should go towards building long-term strength by
                        adapting the operating model for this new business environment.
                    </p>
                    <p class="Paragraph--type-3">
                        Balance sheet resilience can be increased via operational and financial
                        restructuring – the latter entailing a combination of refinancing,
                        de-leveraging through disposal of non-core assets and tighter cash
                        flow management.
                    </p>
                </div>
                <div class="col-sm-12 col-md-6">
                    <p class="Paragraph--type-3">
                        AI and analytics are vital aids to risk mitigation; they were already the
                        most popular tools for this purpose before the pandemic, according to
                        the Barometer, which found they were used by 50% of respondents. Now,
                        cost pressures make technological solutions indispensable.
                    </p>
                    <p class="Paragraph--type-3">
                        To compete in a transformed ecosystem, businesses must challenge old
                        assumptions around financial and operational best practice. Those that
                        do will emerge leaner, smarter and with renewed purpose.
                    </p>
                </div>

            </div>
            <div class="row mt-5 py-5">
                <div class="col">
                    <div class="Number">
                        50%
                        <div class="Number__bar color-white-bg">
                            <div class="Number__bar-full" style="width: 50%"></div>
                        </div>
                        <div class="Paragraph--type-2 mt-3">
                            of business leaders surveyed used AI
                            and analytics to monitor for scenarios
                            which might impact turnover
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="Number">
                        60%
                        <div class="Number__bar color-white-bg">
                            <div class="Number__bar-full" style="width: 60%"></div>
                        </div>
                        <div class="Paragraph--type-2 mt-3">
                            of business leaders surveyed believed
                            their country would become a worse
                            place for business in the next year
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="Number">
                        70%
                        <div class="Number__bar color-white-bg">
                            <div class="Number__bar-full" style="width: 70%"></div>
                        </div>
                        <div class="Paragraph--type-2 mt-3">
                            is the average share price drop when
                            cases of financial mismanagement
                            develop into crises
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="Section__triangle Section__triangle--type-6">
        <div class="container-large-md container py-5">
            <div class="row mt-5">
                <div class="col-sm-12 text-center">
                    <div class="Title--type-2 text-white">
                        COMPETE
                    </div>
                    <div class="Paragraph--type-1 text-white">
                        To outperform their competitors in the longer<br/>
                        term, companies need a strong balance sheet and<br/>
                        a flexible operating model supporting resilience,<br/>
                        automation and efficiency – not just low costs.<br/>
                        Also essential is an actionable implementation plan<br/>

                        for delivering new ways of working.
                    </div>
                </div>
            </div>
            <div class="row py-5 my-5"></div>
            <div class="row mb-5 mt-150px">
                <div class="col-sm-12 col-md-4 text-center">
                    <div class="mw-360px mx-auto">
                        <div class="Title--type-2 text-white text-right">
                            ADAPT
                        </div>
                        <div class="Paragraph--type-1 text-right">
                            After rapidly adopting new ways of working,<br/>
                            businesses must plan for the longer term,<br/>
                            ensuring their strategies and programme<br/>
                            structures are flexible enough to enable<br/>
                            management to respond to fast-changing<br/>
                            and uncertain economic circumstances.
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-4"></div>
                <div class="col-sm-12 col-md-4 text-center">
                    <div class="mw-360px mx-auto">
                        <div class="Title--type-2 text-white text-left">
                            PROTECT
                        </div>
                        <div class="Paragraph--type-1 text-left">
                            Capital structures and operating models must be<br/>
                            robust enough to deal with short-term shocks.<br/>
                            Businesses must monitor liquidity with focused<br/>
                            cash management strategies, and prepare for<br/>
                            disruption scenarios with comprehensive<br/>
                            business continuity plans. Embedding integrated<br/>
                            risk management will protect employees, assets<br/>
                            and data, as well as cash.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <section class="Home__Perspectives">
        <div class="container-large-md container">
            <div class="Home__Perspectives__Title">
                <h2 class="Home__Perspectives__Title-Text"><?php echo $sections['perspectives']['title']; ?></h2>
            </div>
            <div class="Home__Perspectives__Slider-Wrapper">

                <div class="Home__Perspectives__Slider-Wrapper__Slider">
                    <div class="swipper-container">
                        <div class="Home__Perspectives-Slider-Button button-previous">
                            <img src="/content/img/left-arrow-navigation-v1.svg"/>
                        </div>
                        <div class="swiper-wrapper">


                            <div class="swiper-slide">
                                <div class="Home__Perspectives__Card">
                                    <div class="Home__Perspectives__Card__Header">
                                        <div class="Home__Perspectives__Card__Header-Image-Border border-line--type-6"></div>
                                        <div class="Home__Perspectives__Card__Header-Image-Placeholder" style="background-size:cover; background-image: url(/content/img/Crop-4-01.png)"></div>

                                    </div>
                                    <div class="Home__Perspectives__Card__Description">
                                        <p class="color-operational">
                                            3 Approaches to IP Valuation for Pandemic Recovery
                                        </p>
                                        <a class="color-operational Home__Perspectives__Card__Description-Link"
                                           href="/operational-financial-resilience/Approaches-to-IP-Valuation-for-Pandemic-Recovery">
                                            READ MORE
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="swiper-slide">
                                <div class="Home__Perspectives__Card">
                                    <div class="Home__Perspectives__Card__Header">
                                        <div class="Home__Perspectives__Card__Header-Image-Border border-line--type-6"></div>
                                        <div class="Home__Perspectives__Card__Header-Image-Placeholder" style="background-size:cover; background-image: url(/content/img/Crop-5-01.png)"></div>

                                    </div>
                                    <div class="Home__Perspectives__Card__Description">
                                        <p class="color-operational">
                                            COVID-19 and Commercial Real Estate Considerations
                                        </p>
                                        <a class="color-operational Home__Perspectives__Card__Description-Link"
                                           href="/operational-financial-resilience/COVID-19-and-Commercial-Real-Estate-Considerations">
                                            READ MORE
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="swiper-slide">
                                <div class="Home__Perspectives__Card">
                                    <div class="Home__Perspectives__Card__Header">
                                        <div class="Home__Perspectives__Card__Header-Image-Border border-line--type-6"></div>
                                        <div class="Home__Perspectives__Card__Header-Image-Placeholder" style="background-size:cover; background-image: url(/content/img/Crop-2-01.png)"></div>

                                    </div>
                                    <div class="Home__Perspectives__Card__Description">
                                        <p class="color-operational">
                                            How European Retailers Can Manage COVID-19 Disruption and Make It to the Other Side
                                        </p>
                                        <a class="color-operational Home__Perspectives__Card__Description-Link"
                                           href="/operational-financial-resilience/How-European-Retailers-Can-Manage-COVID-19">
                                            READ MORE
                                        </a>
                                    </div>
                                </div>
                            </div>




                        </div>
                        <div class="Home__Perspectives-Slider-Button button-next">
                            <img src="/content/img/right-arrow-navigation-v1.svg"/>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <?php include 'exploreanotherlever.php'; ?>
</main>
