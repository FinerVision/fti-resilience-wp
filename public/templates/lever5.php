<main class="business-model-and-workforce-transformation">
    <div class="Background--Gradient-Blue-Black">
        <?php
        require __DIR__ . '/../components/leverCarousel.php'; ?>
    </div>

    <div class="container-large-md container mt-3 mt-lg-5">
    </div>
    <section class="Section--decoration-right">
        <div class="container-large-md container">
            <div class="row pt-0 pt-lg-5">
                <div class="col-sm-12 col-md-6">
                    <h2 class="Title--type-1 mb-3">
                        A flexible organisation, culture and workforce are vital to resilience in an unpredictable
                        post-COVID world. Companies need to be able to maintain resilience in the face of radical
                        changes in working environment. To recruit and retain key talent, companies will have to offer
                        the right working conditions and be more cognisant of personal circumstances.
                    </h2>
                </div>
                <div class="col-sm-12 col-md-6">
                    <div class="mw-300px mx-auto">
                        <div class="Number">
                            94%
                            <div class="Number__bar">
                                <div class="Number__bar-full" style="width: 94%">

                                </div>
                            </div>
                        </div>
                        <p class="mt-3 Paragraph--type-2">
                            of business leaders surveyed believed
                            they were under pressure to improve
                            their corporate cultures
                        </p>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="Section--decoration-left mb-5">
        <div class="container-large-md container">
            <div class="row mt-5">
                <div class="col-sm-12 col-md-6 pl-md-5 x-y-center">
                    <p class="Paragraph--type-2">
                        The share price reduction as<br/>
                        a result of a crisis relating to a<br/>
                        company’s strategy is almost
                    </p>
                    <div class="Number">
                        5x greater
                    </div>
                    <p class="Paragraph--type-2">than the loss in profit</p>
                </div>
                <div class="col-sm-12 col-md-6">
                    <p class="Paragraph--type-3">
                        COVID-19 has dramatically changed assumptions and norms about how
                        organisations manage their workforces, along with the way they service
                        customers. Unfortunately, many organisations were ill-prepared for
                        upheaval on this scale. In FTI’s Resilience Barometer<sup>TM</sup> published in
                        January 2020, we reported that only 24% of respondents were driving
                        cultural change in their organisation to mitigate risk from crises – even
                        though 94% of respondents felt under pressure to improve their
                        corporate cultures.

                    </p>
                    <p class="Paragraph--type-3">
                        Following the pandemic, the one certainty is that change can happen fast and unpredictably. To
                        become more resilient organisations must be more proactive and introspective, having the courage
                        to overhaul business models, technology and culture to navigate a new and volatile landscape.
                        Unless a company remains relevant to its customers and its employees, both groups will look
                        elsewhere.

                    </p>
                </div>
            </div>
        </div>

    </section>
    <section class="Section--background-gray pt-3 pb-5">
        <div class="container-large-md container">
            <div class="row my-3"></div>
            <div class="row">
                <div class="col-sm-12 mb-5">
                    <div class="Title--type-2">

                    </div>
                </div>
                <div class="col-sm-12 col-md-6">
                    <div class="Paragraph--type-3">
                        <p>
                            Post-pandemic organisations need to be leaner, with products and services delivered through
                            a more flexible and agile business model. This implies more flexible facilities, a workforce
                            with diverse skills and capabilities, which are underpinned by a highly flexible operating
                            and resourcing model.
                        </p>
                        <p>
                            Companies will need to invest in technology and infrastructure to adapt to a new physical
                            working environment as social distancing and virtual working become common features.
                            Accommodating these and other new ways of working will be essential to providing flexibility
                            and for the retention of key staff and for maintaining competitiveness.
                        </p>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6">
                    <div class="Paragraph--type-3">
                        <p>
                            Shifting stakeholder attitudes will increase companies’ focus on ESG, sustainability and
                            social justice agendas, and specifically on people-related issues such as health and safety.
                            FTI research suggests this was already becoming a priority before the pandemic, ranking
                            alongside climate change among the top ESG issues for investors according to Global Investor
                            Insights 2020.
                        </p>
                        <p>
                            Alongside the chaos and disruption, COVID-19 presents companies with opportunities to
                            reimagine their business models for the better, transforming the way they work with internal
                            & external stakeholders and radically reducing costs that would have otherwise been fixed.
                        </p>
                    </div>
                </div>

            </div>
            <div class="row mt-5 py-5">
                <div class="col">
                    <div class="Number">
                        60%
                        <div class="Number__bar bar--white">
                            <div class="Number__bar-full" style="width: 60%">

                            </div>
                        </div>
                        <div class="Paragraph--type-2 mt-3">
                            of business leaders surveyed said
                            that employee safety was one of
                            their top ESG issues
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="Number">
                        24%
                        <div class="Number__bar bar--white">
                            <div class="Number__bar-full" style="width: 24%">

                            </div>
                        </div>
                        <div class="Paragraph--type-2 mt-3">
                            of business leaders are driving
                            cultural change in their organisation
                            to mitigate risk from crises
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="Number">
                        68%
                        <div class="Number__bar bar--white">
                            <div class="Number__bar-full" style="width: 68%">

                            </div>
                        </div>
                        <div class="Paragraph--type-2 mt-3">
                            of business leaders surveyed believed
                            they would lose competitiveness
                            within 5 years if they did not transform
                            their business to cope with technology
                            disruption
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="Section__triangle Section__triangle--type-5">
        <div class="container-large-md container py-5">
            <div class="row mt-5">
                <div class="col-sm-12 text-center">
                    <div class="mw-360px mx-auto">
                        <div class="Title--type-2 text-white">
                            COMPETE
                        </div>
                        <div class="Paragraph--type-1 text-white">
                            Others are seeing the opportunity to reimagine work. Organisations must create a compelling
                            business and model employee experience to attract and retain key employees as competitive
                            intensity increases. Adaptable leadership and cross-functional working will maximise the
                            workforce’s responsiveness to change.
                        </div>
                    </div>
                </div>
            </div>
            <div class="row py-5 my-5"></div>
            <div class="row mb-5 mt-150px">
                <div class="col-sm-12 col-md-4 text-center">
                    <div class="mw-360px mx-auto">
                        <div class="Title--type-2 text-white text-right">
                            ADAPT
                        </div>
                        <div class="Paragraph--type-1 text-right">
                            The new threats and opportunities also require new ways of working. Organisations must
                            resize their workforce and reshape their talent to maximise flexibility in the face of
                            ongoing market disruption, as well as leverage technology to gain competitive advantage or
                            indeed compete in new markets.

                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-4"></div>
                <div class="col-sm-12 col-md-4 text-center">
                    <div class="mw-360px mx-auto">
                        <div class="Title--type-2 text-white text-left">
                            PROTECT
                        </div>
                        <div class="Paragraph--type-1 text-left">
                            Culture change is now mandatory, not a “nice to have”. To protect themselves from threats
                            and seize opportunities, organisations must develop a resilient culture, characterised by
                            values and behaviours that tackle change head on and turn it into an advantage.

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <section class="Home__Perspectives">
        <div class="container-large-md container">
            <div class="Home__Perspectives__Title">
                <h2 class="Home__Perspectives__Title-Text"><?php echo $sections['perspectives']['title']; ?></h2>
            </div>
            <div class="Home__Perspectives__Slider-Wrapper">

                <div class="Home__Perspectives__Slider-Wrapper__Slider">
                    <div class="swipper-container">
                        <div class="Home__Perspectives-Slider-Button button-previous">
                            <img src="/content/img/left-arrow-navigation-v1.svg"/>
                        </div>
                        <div class="swiper-wrapper">


                            <div class="swiper-slide">
                                <div class="Home__Perspectives__Card">
                                    <div class="Home__Perspectives__Card__Header">
                                        <div
                                            class="Home__Perspectives__Card__Header-Image-Border border-line--type-5"></div>
                                        <div class="Home__Perspectives__Card__Header-Image-Placeholder"
                                             style="background-size:cover; background-image: url(/content/img/Crop-4-01.png)"></div>

                                    </div>
                                    <div class="Home__Perspectives__Card__Description">
                                        <p class="color-business-model">
                                            Rethinking RIF Workforce Optimization in Healthcare
                                        </p>
                                        <a class="color-business-model Home__Perspectives__Card__Description-Link"
                                           href="/business-model-workforce-transformation/Rethinking-RIF-Workforce-Optimization-in-Healthcare">
                                            READ MORE
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="swiper-slide">
                                <div class="Home__Perspectives__Card">
                                    <div class="Home__Perspectives__Card__Header">
                                        <div
                                            class="Home__Perspectives__Card__Header-Image-Border border-line--type-5"></div>
                                        <div class="Home__Perspectives__Card__Header-Image-Placeholder"
                                             style="background-size:cover; background-image: url(/content/img/Crop-5-01.png)"></div>

                                    </div>
                                    <div class="Home__Perspectives__Card__Description">
                                        <p class="color-business-model">
                                            Toward a Brave New World: reboarding your workforce in the new normal.
                                        </p>
                                        <a class="color-business-model Home__Perspectives__Card__Description-Link"
                                           href="/business-model-workforce-transformation/Toward-a-Brave-New-World:-reboarding-your-workforce-in-the-new-normal">
                                            READ MORE
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="swiper-slide">
                                <div class="Home__Perspectives__Card">
                                    <div class="Home__Perspectives__Card__Header">
                                        <div
                                            class="Home__Perspectives__Card__Header-Image-Border border-line--type-5"></div>
                                        <div class="Home__Perspectives__Card__Header-Image-Placeholder"
                                             style="background-size:cover; background-image: url(/content/img/Crop-2-01.png)"></div>

                                    </div>
                                    <div class="Home__Perspectives__Card__Description">
                                        <p class="color-business-model">
                                            The Case for Culture

                                        </p>
                                        <a class="color-business-model Home__Perspectives__Card__Description-Link"
                                           href="/business-model-workforce-transformation/The-Case-for-Culture">
                                            READ MORE
                                        </a>
                                    </div>
                                </div>
                            </div>


                        </div>
                        <div class="Home__Perspectives-Slider-Button button-next">
                            <img src="/content/img/right-arrow-navigation-v1.svg"/>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <?php include 'exploreanotherlever.php'; ?>
</main>
