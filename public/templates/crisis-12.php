<main class="Home">
    <?php include 'perspectiveheader.php'; ?>

    <div class="Section--background-perspectives">
        <div class="container-large-md container">
            <div class="row">
                <div class="col-sm-12 col-md-8 Section--bg-white py-5">
                    <div class="Title--type-2 color-dark-blue mb-4">XX JULY 2020</div>
                    <hr class="HR HR--type-1">
                    <h1 class="Title--type-7 mt-3">
                        Not If But When: <br/>Building Cybersecurity Resilience to Secure Competitive Advantage
                    </h1>
                    <b>
                        The current global pandemic isn’t going away anytime soon, and the economy is suffering its
                        biggest downturn in centuries. Looking ahead to the coming months and years, the most successful
                        organisations will be those which can leverage their resilience to create competitive advantage.
                        Resilient organisations will be less at risk of a cyber breach, be better able to respond to
                        incidents, and catch up quickly if the worst were to happen.
                    </b>
                    <p>
                        Head of Cybersecurity for the EMEA region Joshua Burch has written a white paper entitled ‘Not
                        If But When: Building Cybersecurity Resilience to Secure Competitive Advantage’, where he
                        sketches out the building blocks for constructing cybersecurity resilience, focusing on
                        strategic risk management, cultural change, leadership, and effective communication. This isn’t
                        a technical blueprint, but rather Joshua explores what it takes in today’s world to build an
                        organisation with cybersecurity resilience at its core.
                    </p>
                </div>
                <div class="col-sm-12 col-md-4 Section--sidebar">
                    <div class="Title--type-2 color-dark-blue mb-4">&nbsp;</div>
                    <div class="SocialBox">
                        SHARE THIS CONTENT
                        <ul class="SocialShare SocialShare--align-left mt-3">
                            <li class="SocialShare__Item">
                                <a class="SocialShare__Item__Link" href="#" target="_blank">
                                    <img class="SocialShare__Item__Link-Icon" src="../assets/img/facebook-icon-v2.svg">
                                </a>
                            </li>
                            <li class="SocialShare__Item">
                                <a class="SocialShare__Item__Link" href="#" target="_blank">
                                    <img class="SocialShare__Item__Link-Icon" src="../assets/img/twitter-icon-v2.svg">
                                </a>
                            </li>
                            <li class="SocialShare__Item">
                                <a class="SocialShare__Item__Link" href="#" target="_blank">
                                    <img class="SocialShare__Item__Link-Icon" src="../assets/img/linkedin-icon-v2.svg">
                                </a>
                            </li>
                            <li class="SocialShare__Item">
                                <a class="SocialShare__Item__Link" href="#" target="_blank">
                                    <img class="SocialShare__Item__Link-Icon" src="../assets/img/share-icon-v1.svg">
                                </a>
                            </li>
                        </ul>
                    </div>

                    <div class="Document mt-5">
                        <img src="../assets/img/documents/FTI-Cybersecurity-Building-Cybersecurity-Resilience.png"
                             class="img-fluid"/>
                    </div>

                    <a href="../assets/img/documents/FTI-Cybersecurity-Building-Cybersecurity-Resilience.pdf"
                       target="_blank" class="Button Button--type-1 mt-3">
                        DOWNLOAD >
                    </a>
                </div>
            </div>
        </div>
    </div>

    <?php include 'exploreothertopics.php'; ?>
</main>
