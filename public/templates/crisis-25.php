<main class="Home">
    <?php include 'perspectiveheader.php'; ?>

    <div class="Section--background-perspectives">
        <div class="container-large-md container">
            <div class="row">
                <div class="col-sm-12 col-md-8 Section--bg-white py-5">
                    <div class="Title--type-2 color-dark-blue mb-4">XX JULY 2020</div>
                    <hr class="HR HR--type-1">
                    <h1 class="Title--type-7 mt-3">
                        A new era of data analytics for investigations and litigation
                    </h1>
                    <b>
                        One of the key challenges for lawyers today, and a key differentiator for many clients, is the
                        ability
                        to engage with advanced analytics methodologies (and supporting technology) in order to make
                        the most of available data.
                    </b>
                    <p>
                        There is clear evidence that courts and regulators are becoming more accepting of analytics as
                        an
                        expert function, and are open to using it to inform decisions. Indeed, the evolution of advanced
                        analytics in court will follow that seen with e-discovery and technology assisted review both
                        here
                        and abroad. The challenge remains open: how to be an effective client advocate by bringing the
                        right
                        scepticism to moderate enthusiastic adoption of technology.
                    </p>
                    <p>
                        For most law firms, it is unrealistic to expect to have a full range of up-to-date data science
                        skills in-
                        house, especially given the rate of technology evolution. If partnering with an external
                        organisation,
                        it’s advisable to look to firms that field an integrated team of data scientists, traditional
                        analysts and
                        deep subject experts; such multidisciplinary teams can work with lawyers seamlessly to apply all
                        that
                        knowledge to investigations. Otherwise, too much can get lost in translation – analytics and
                        science
                        should be enabling expertise, not befuddling it with clever output.
                    </p>
                </div>
                <div class="col-sm-12 col-md-4 Section--sidebar">
                    <div class="Title--type-2 color-dark-blue mb-4">&nbsp;</div>
                    <div class="SocialBox">
                        SHARE THIS CONTENT
                        <ul class="SocialShare SocialShare--align-left mt-3">
                            <li class="SocialShare__Item">
                                <a class="SocialShare__Item__Link" href="#" target="_blank">
                                    <img class="SocialShare__Item__Link-Icon" src="../assets/img/facebook-icon-v2.svg">
                                </a>
                            </li>
                            <li class="SocialShare__Item">
                                <a class="SocialShare__Item__Link" href="#" target="_blank">
                                    <img class="SocialShare__Item__Link-Icon" src="../assets/img/twitter-icon-v2.svg">
                                </a>
                            </li>
                            <li class="SocialShare__Item">
                                <a class="SocialShare__Item__Link" href="#" target="_blank">
                                    <img class="SocialShare__Item__Link-Icon" src="../assets/img/linkedin-icon-v2.svg">
                                </a>
                            </li>
                            <li class="SocialShare__Item">
                                <a class="SocialShare__Item__Link" href="#" target="_blank">
                                    <img class="SocialShare__Item__Link-Icon" src="../assets/img/share-icon-v1.svg">
                                </a>
                            </li>
                        </ul>
                    </div>

                    <div class="Document mt-5">
                        <img src="../assets/img/documents/FTI-Cybersecurity-Building-Cybersecurity-Resilience.png"
                             class="img-fluid"/>
                    </div>

                    <a href="../assets/img/documents/FTI-Cybersecurity-Building-Cybersecurity-Resilience.pdf"
                       target="_blank" class="Button Button--type-1 mt-3">
                        DOWNLOAD >
                    </a>
                </div>
            </div>
        </div>
    </div>

    <?php include 'exploreothertopics.php'; ?>
</main>
