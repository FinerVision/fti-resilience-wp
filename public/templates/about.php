<main class="Home">
    <div class="Background--Gradient-Blue-Black h-200px-md">
        <div class="container-large-md container h-100">
            <div class="row h-100">
                <div class="col-sm-12 d-flex align-items-center">
                    <h1 class="text-white">About FTI Consulting</h1>
                </div>
            </div>
        </div>
    </div>

    <section class="Section--decoration-right my-5">
        <div class="container-large-md container">
            <div class="row">
                <div class="col-sm-12">
                    <h2 class="Title--type-1 mb-3 color-dark-blue">FTI Consulting is an independent global business advisory<br/>
                        firm dedicated to helping organisations manage change,<br/>
                        mitigate risk and resolve disputes.</h2>
                </div>
            </div>
        </div>
    </section>
    <section class="Section--decoration-map mb-2 py-5">
        <div class="container-large-md container">
            <div class="row mt-2">
                <div class="col-sm-12 col-md-6">
                    <br/>
                    <p class="Paragraph--type-3">Operating globally across 27 countries on six continents, we oer a
                        comprehensive suite of services designed to assist clients right across
                        the business cycle – from proactive risk management to the ability to
                        respond rapidly to unexpected events and dynamic environments.</p>
                    <p>
                        For further information on how FTI Consulting can help your
                        organisation to improve their resilience to critical events,
                        please <a href="#" style="color: red">contact one of our experts</a> .
                    </p>
                </div>
                <div class="col-sm-12 col-md-6">
                    <img src="../assets/img/map.png" class="mw-100"/>
                </div>
            </div>
        </div>

    </section>

</main>
