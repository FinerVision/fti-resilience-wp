<main class="Home">
    <?php include 'perspectiveheader.php'; ?>

    <div class="Section--background-perspectives">
        <div class="container-large-md container">
            <div class="row">
                <div class="col-sm-12 col-md-8 Section--bg-white py-5">
                    <div class="Title--type-2 color-dark-blue mb-4">XX JULY 2020</div>
                    <hr class="HR HR--type-1">
                    <h1 class="Title--type-7 mt-3">
                        The CEO Brand and Its Impact On Business
                    </h1>
                    <b>
                        New research by FTI Consulting sheds light on the role of the CEO on a company’s valuation.
                        While this is one factor among many, the research indicates that companies whose CEOs
                        demonstrate strong leadership, and were publicly vocal, better withstood the negative impacts of
                        the initial stages of the COVID-19 pandemic. This stronger investor confidence translates into
                        as much as US$260 billion in additional shareholder value.
                    </b>
                    <p>
                        Furthermore, CEOs who embrace the growing importance of ESG and ‘stakeholder capitalism’
                        performed better still in this period. ‘Stakeholder CEOs’ – those who spoke about issues not
                        directly related to their business’s bottom line – outperformed their industry peers by an
                        average of 3.75 percent. Beyond the COVID-19 crisis, the research reveals the longer-term impact
                        of a vocal CEO with outspoken leaders overseeing the vast majority (81 percent) of the fastest
                        growing companies in the world.
                    </p>
                    <p>
                        To come to these conclusions, FTI Consulting analysed the 100 fastest growing companies by share
                        price, listed in the United States, United Kingdom and Europe for their leadership’s
                        communication styles from February 2015 to February 2020. To be able to compare against data
                        measured against the backdrop of one of the most protracted bull markets in history, FTI
                        Consulting also looked at a turbulent bear market.
                    </p>
                    <p>
                        The impact of this leadership style on share prices was measured during the global COVID-19
                        pandemic, from one week before the WHO declared a pandemic (4 March 2020) to 11 May 2020.
                    </p>
                </div>
                <div class="col-sm-12 col-md-4 Section--sidebar">
                    <div class="Title--type-2 color-dark-blue mb-4">&nbsp;</div>
                    <div class="SocialBox">
                        SHARE THIS CONTENT
                        <ul class="SocialShare SocialShare--align-left mt-3">
                            <li class="SocialShare__Item">
                                <a class="SocialShare__Item__Link" href="#" target="_blank">
                                    <img class="SocialShare__Item__Link-Icon" src="../assets/img/facebook-icon-v2.svg">
                                </a>
                            </li>
                            <li class="SocialShare__Item">
                                <a class="SocialShare__Item__Link" href="#" target="_blank">
                                    <img class="SocialShare__Item__Link-Icon" src="../assets/img/twitter-icon-v2.svg">
                                </a>
                            </li>
                            <li class="SocialShare__Item">
                                <a class="SocialShare__Item__Link" href="#" target="_blank">
                                    <img class="SocialShare__Item__Link-Icon" src="../assets/img/linkedin-icon-v2.svg">
                                </a>
                            </li>
                            <li class="SocialShare__Item">
                                <a class="SocialShare__Item__Link" href="#" target="_blank">
                                    <img class="SocialShare__Item__Link-Icon" src="../assets/img/share-icon-v1.svg">
                                </a>
                            </li>
                        </ul>
                    </div>

                    <div class="Document mt-5">
                        <img src="../assets/img/documents/covid-19-ceo-brand-impact-business.png" class="img-fluid"/>
                    </div>

                    <a href="../assets/img/documents/covid-19-ceo-brand-impact-business.pdf" target="_blank"
                       class="Button Button--type-1 mt-3">
                        DOWNLOAD >
                    </a>
                </div>
            </div>
        </div>
    </div>

    <?php include 'exploreothertopics.php'; ?>
</main>
