<main class="government-and-stakeholders-relations">
    <div class="Background--Gradient-Blue-Black">
        <?php
        require __DIR__ . '/../components/leverCarousel.php'; ?>
    </div>
    <div class="container-large-md container mt-3 mt-lg-5">
    </div>
    <section class="Section--decoration-right">
        <div class="container-large-md container">
            <div class="row pt-0 pt-lg-5">
                <div class="col-sm-12 col-md-6">
                    <h2 class="Title--type-1 mb-3">
                        Evidence-based advocacy and<br/>
                        strong leadership can enable a<br/>
                        positive response to mounting<br/>
                        stakeholder scrutiny in the wake<br/>
                        of the pandemic.
                    </h2>
                    <p class="Paragraph--type-3">
                        Even before the pandemic, organisations faced intensifying scrutiny.
                        FTI’s 2020 Resilience Barometer<sup>TM</sup> shows that global executives already
                        believed that customers, governments, regulators and investors are the
                        stakeholders with the most impact on their businesses’ performance and
                        strategic direction.
                    </p>
                </div>
                <div class="col-sm-12 col-md-6">
                    <div class="mw-300px mx-auto text-center">
                        <div class="Progress-circle mx-auto">
                            <img src="./assets/img/government-percentage-v1.svg"/>
                        </div>
                        <p class="mt-3 Paragraph--type-2">
                            of business leaders surveyed were
                            not involved in the legal
                            processes at their organisation
                            <span>(compared to just 28% for operations

                            and 29% for strategy)</span>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="Section--decoration-left mb-5">
        <div class="container-large-md container">
            <div class="row mt-5">
                <div class="col-sm-12 col-md-6 pl-md-5">
                    <div class="mw-300px mx-auto">
                        <p class="Paragraph--type-2 my-0">
                            More than
                        </p>
                        <div class="Title--type-7 font-weight-bold color-blue1">
                            1 in 5
                        </div>
                        <p class="Paragraph--type-2 my-0 mb-5">
                            companies have experienced a leak
                            of internal sensitive communications
                            in the last 12 months
                        </p>
                        <div class="Title--type-6 color-blue1">
                            Only <div class="Title--type-7 color-blue1 d-inline">35%</div>
                        </div>
                        <div class="Number__bar">
                            <div class="Number__bar-full" style="width: 35%">

                            </div>
                        </div>

                        <p class="Paragraph--type-2">of companies are proactively mitigating
                            the risks of political disruptions or
                            abrupt policy changes</p>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6">
                    <p class="Paragraph--type-3">
                        The pandemic has accelerated this trend, with government scrutiny
                        particularly likely to increase. In a heavily politicised environment, any
                        form of state investment in the private sector, from relief funding to
                        equity holdings, comes with high expectations, strict conditions, and
                        risks. So do other forms of support, such as temporary relaxation of
                        competition and anti-trust rules, in a context of increased trade tensions.
                    </p>
                    <p class="Paragraph--type-3">
                        These businesses also face the “court of public opinion”. Active
                        engagement by employees and the broader public is generally on the
                        rise: <i>FTI's Anatomy of Crisis IV</i> found 62% of workers calling for the
                        media to scrutinize business more closely if a second COVID-19 spike
                        occurs.
                    </p>
                    <p class="Paragraph--type-3">
                        As stakeholders, governments and civil society will expect companies to
                        demonstrate strong governance and contribute meaningfully and
                        eectively to environmental or societal goals in return, illustrating the
                        growing importance of ESG performance.
                    </p>
                </div>
            </div>
        </div>

    </section>
    <section class="Section--background-gray pt-3 pb-5">
        <div class="container-large-md container">
            <div class="row my-3"></div>
            <div class="row">
                <div class="col-sm-12 mb-5">
                    <div class="Title--type-2">
                        DATA-DRIVEN COMMUNICATION ENABLES A CONSTRUCTIVE
                        RESPONSE TO SCRUTINY.
                    </div>
                </div>
                <div class="col-sm-12 col-md-6">
                    <p class="Paragraph--type-3">
                        Facing pressure from all sides, companies must reinforce their business

                        and advocacy strategies by basing all decisions on hard evidence. Advanced
                        data, intelligence gathering, and analysis should be used to

                        gauge positioning, assess stakeholder sentiment, and measure the

                        impact of engagement. Such assessments must be undertaken holistically
                        with respect to jurisdictions and geographies, ensuring strategies

                        remain effective across borders.
                    </p>
                </div>
                <div class="col-sm-12 col-md-6">
                    <p class="Paragraph--type-3">
                        Senior management communication is also pivotal – in particular, an
                        authentic, constructive dialogue between CEOs and stakeholders to

                        affirm the business’s role and purpose. These discussions promote financial
                        strength in addition to goodwill: vocal CEOs have generated

                        US$260bn additional shareholder value since the start of the pandemic.
                    </p>
                    <p class="Paragraph--type-3">
                        A resilient business must engage proactively with complex stakeholder networks,
                        transforming external scrutiny into a source of strength.
                    </p>
                </div>

            </div>
            <div class="row mt-5 py-5">
                <div class="col">
                    <div class="Number">
                        87%
                        <div class="Number__bar">
                            <div class="Number__bar-full" style="width: 87%">

                            </div>
                        </div>
                        <div class="Paragraph--type-2 mt-3">
                            of business leaders believe that
                            companies should be run in the interest
                            of all stakeholders, not
                            just shareholders
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="Number">
                        81%
                        <div class="Number__bar">
                            <div class="Number__bar-full" style="width: 81%">

                            </div>
                        </div>
                        <div class="Paragraph--type-2 mt-3">
                            of business leaders surveyed expect
                            to see an increase in regulations in
                            the next 12 months (compared to
                            76% in 2019)
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="Number">
                        71%
                        <div class="Number__bar">
                            <div class="Number__bar-full" style="width: 71%">

                            </div>
                        </div>
                        <div class="Paragraph--type-2 mt-3">
                            of large companies have been
                            investigated for market dominance
                            in the last 12 months
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="Section__triangle Section__triangle--type-3">
        <div class="container-large-md container py-5">
            <div class="row mt-5">
                <div class="col-sm-12 text-center">
                    <div class="Title--type-2 text-white">
                        COMPETE
                    </div>
                    <div class="Paragraph--type-1 text-white">
                        To grow their impact, businesses must build<br/>
                        evidence-based advocacy and communications<br/>
                        strategies supported by strong internal reporting<br/>
                        systems. Innovative, consistent, and substantive<br/>
                        engagement encompassing all stakeholder<br/>
                        groups across jurisdictions is essential.
                    </div>
                </div>
            </div>
            <div class="row py-5 my-5"></div>
            <div class="row mb-5 mt-150px">
                <div class="col-sm-12 col-md-4 text-center">
                    <div class="mw-360px mx-auto">
                        <div class="Title--type-2 text-white text-right">
                            ADAPT
                        </div>
                        <div class="Paragraph--type-1 text-right">
                            Increased scrutiny presents opportunities to<br/>
                            build resilience. Private sector engagement<br/>
                            can help steer society and the economy<br/>
                            towards recovery from COVID-19. Businesses<br/>
                            must demonstrate value and purpose while<br/>
                            navigating changing regulatory landscapes<br/>
                            across multiple geographies.
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-4"></div>
                <div class="col-sm-12 col-md-4 text-center">
                    <div class="mw-360px mx-auto">
                        <div class="Title--type-2 text-white text-left">
                            PROTECT
                        </div>
                        <div class="Paragraph--type-1 text-left">
                            Businesses face increasing scrutiny and<br/>
                            demands from government and civil society,<br/>
                            alongside other pressures such as shareholder<br/>
                            activism. Proactive, consistent engagement<br/>
                            and informed decision-making ensure effective<br/>
                            responses and protect freedom to operate.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <section class="Home__Perspectives">
        <div class="container-large-md container">
            <div class="Home__Perspectives__Title">
                <h2 class="Home__Perspectives__Title-Text"><?php echo $sections['perspectives']['title']; ?></h2>
            </div>
            <div class="Home__Perspectives__Slider-Wrapper">

                <div class="Home__Perspectives__Slider-Wrapper__Slider">
                    <div class="swipper-container">
                        <div class="Home__Perspectives-Slider-Button button-previous">
                            <img src="/content/img/left-arrow-navigation-v1.svg"/>
                        </div>
                        <div class="swiper-wrapper">

                            <div class="swiper-slide">
                                <div class="Home__Perspectives__Card">
                                    <div class="Home__Perspectives__Card__Header">
                                        <div class="Home__Perspectives__Card__Header-Image-Border border-line--type-3"></div>
                                        <div class="Home__Perspectives__Card__Header-Image-Placeholder" style="background-size:cover; background-image: url(/content/img/Crop-4-01.png)"></div>

                                    </div>
                                    <div class="Home__Perspectives__Card__Description">
                                        <p class="color-government">The CEO Brand and Its Impact On Business</p>
                                        <a class="color-government Home__Perspectives__Card__Description-Link"
                                           href="/government-and-stakeholder-relations/The-CEO-Brand-and-Its-Impact-On-Business">
                                            READ MORE
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="swiper-slide">
                                <div class="Home__Perspectives__Card">
                                    <div class="Home__Perspectives__Card__Header">
                                        <div class="Home__Perspectives__Card__Header-Image-Border border-line--type-3"></div>
                                        <div class="Home__Perspectives__Card__Header-Image-Placeholder" style="background-size:cover; background-image: url(/content/img/Crop-5-01.png)"></div>

                                    </div>
                                    <div class="Home__Perspectives__Card__Description">
                                        <p class="color-government">Activism Vulnerability Report</p>
                                        <a class="color-government Home__Perspectives__Card__Description-Link"
                                           href="/government-and-stakeholder-relations/Activism-Vulnerability-Report">
                                            READ MORE
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="swiper-slide">
                                <div class="Home__Perspectives__Card">
                                    <div class="Home__Perspectives__Card__Header">
                                        <div class="Home__Perspectives__Card__Header-Image-Border border-line--type-3"></div>
                                        <div class="Home__Perspectives__Card__Header-Image-Placeholder" style="background-size:cover; background-image: url(/content/img/Crop-2-01.png)"></div>

                                    </div>
                                    <div class="Home__Perspectives__Card__Description">
                                        <p class="color-government">The New T&Cs of Investor Relations An Open Letter to the FCO</p>
                                        <a class="color-government Home__Perspectives__Card__Description-Link"
                                           href="/government-and-stakeholder-relations/The-New-T&Cs-of-Investor-Relations">
                                            READ MORE
                                        </a>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="Home__Perspectives-Slider-Button button-next">
                            <img src="/content/img/right-arrow-navigation-v1.svg"/>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <?php include 'exploreanotherlever.php'; ?>
</main>
