<main class="economic-impact-and-sustainability">
    <div class="Background--Gradient-Blue-Black">
        <?php
        require __DIR__ . '/../components/leverCarousel.php'; ?>
    </div>

    <div class="container-large-md container mt-3 mt-lg-5">
    </div>
    <section class="Section--decoration-right">
        <div class="container-large-md container">
            <div class="row pt-0 pt-lg-5">
                <div class="col-sm-12 col-md-6">
                    <h2 class="Title--type-1 mb-3">With environmental, social and<br/>
                        governance (ESG) issues now<br/>
                        strongly influencing a business’s<br/>
                        financial performance, sustainability<br/>
                        must be at the heart of the<br/>
                        post-COVID-19 resilience agenda.
                    </h2>
                    <p class="Paragraph--type-3">
                        Environmental, social and governance (ESG) concerns often used to be
                        treated as marginal issues. However, companies now increasingly see them
                        as central business priorities. According to FTI Consulting’s Resilience
                        Barometer<sup>TM</sup>, published in January 2020, 96% of companies were under
                        pressure to improve their ESG offerings, with 34% under extreme pressure.
                    </p>
                </div>
                <div class="col-sm-12 col-md-6">
                    <div class="mw-300px mx-auto text-center">
                        <div class="Progress-circle mx-auto">
                            <img src="./assets/img/economic-impact-percentage-v1.svg"/>
                        </div>

                        <p class="mt-3 Paragraph--type-2">
                            of business leaders report that
                            their companies are under
                            pressure to improve their ESG
                            and sustainability offerings
                        </p>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="Section--decoration-left mb-5">
        <div class="container-large-md container">
            <div class="row mt-5">
                <div class="col-sm-12 col-md-6 pl-md-5 text-center x-y-center">
                    <div class="Title--type-6">
                        Employee diversity
                    </div>
                    <p class="Paragraph--type-2">
                        is the #1 CSR issue reported<br/>
                        by business leaders
                    </p>
                </div>
                <div class="col-sm-12 col-md-6">
                    <p class="Paragraph--type-3">
                        In particular, investors’ ESG agenda has become a priority for boards.
                        FTI’s Global Investor Insights 2020 reported that 82% of institutional
                        investors believed a company’s value increased 20% or more when it had
                        a positive ESG rating; 81% of institutional investors surveyed for FTI’s ESG
                        Resilience Compass 2020 faced increasing pressure to divest from
                        companies with poor ESG ratings.
                    </p>
                    <p class="Paragraph--type-3">
                        Stakeholder demands for higher ESG standards will only increase in the
                        wake of COVID-19 as organisations come under growing scrutiny from
                        governments, shareholders, customers and public – especially if they
                        have benefited from any form of state assistance.
                    </p>
                </div>
            </div>
        </div>

    </section>
    <section class="Section--background-gray pt-3 pb-5">
        <div class="container-large-md container">
            <div class="row my-3"></div>
            <div class="row">
                <div class="col-sm-12 mb-5">
                    <div class="Title--type-2">
                        BUILDING SUSTAINABILITY INTO THE RESILIENCE STRATEGY
                    </div>
                </div>
                <div class="col-sm-12 col-md-6">
                    <p class="Paragraph--type-3">
                        With ESG performance intrinsic to stakeholder confidence and financial
                        viability, business’s ability to weather future crises will depend heavily on
                        satisfying stakeholder expectations in this area. Sustainability must
                        therefore be viewed as an important element of resilience.
                    </p>
                    <p class="Paragraph--type-3">
                        Companies need to look well beyond mere regulatory compliance: they
                        need to anticipate and adjust to shis in government policy and in
                        shareholder and public attitudes, while remaining profitable. The
                        necessary insights into emerging attitudes can only be gained through
                        ongoing stakeholder engagement and communication.
                    </p>
                </div>
                <div class="col-sm-12 col-md-6">
                    <p class="Paragraph--type-3">
                        This communication must be two way, of course. Corporate reporting
                        needs to address sustainability issues fully: the ESG Resilience Compass
                        2020 found that 88% of institutional investors believed there should be
                        more reporting on the actual impact of businesses’ activities.
                    </p>
                    <p class="Paragraph--type-3">
                        Whatever the business’s size, industry or location, sustainability is
                        an important tool in building resilience and protecting value for
                        today and tomorrow.
                    </p>
                </div>

            </div>
            <div class="row mt-5 py-5">
                <div class="col">
                    <div class="Number">
                        87%
                        <div class="Number__bar color-white-bg">
                            <div class="Number__bar-full" style="width: 87%;"></div>
                        </div>
                        <div class="Paragraph--type-2 mt-3">
                            of business leaders believe that
                            companies should be run in the interest
                            of all stakeholders, not
                            just shareholders
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="Number">
                        36%
                        <div class="Number__bar color-white-bg">
                            <div class="Number__bar-full" style="width: 36%;"></div>

                        </div>
                        <div class="Paragraph--type-2 mt-3">
                            is the average company value
                            increase investors see as a result
                            of having a postitive ESG rating
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="Number">
                        93%
                        <div class="Number__bar color-white-bg">
                            <div class="Number__bar-full" style="width: 93%;"></div>
                        </div>
                        <div class="Paragraph--type-2 mt-3">
                            of institutional investors have been
                            requesting or intend to request
                            greater transparency from their
                            investee companies on their
                            sustainability strategies
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="Section__triangle Section__triangle--type-4">
        <div class="container-large-md container py-5">
            <div class="row mt-5">
                <div class="col-sm-12 text-center">
                    <div class="Title--type-2 text-white">
                        COMPETE
                    </div>
                    <div class="Paragraph--type-1 text-white">
                        Companies with robust ESG and sustainability<br/>
                        strategies can gain competitive advantage.<br/>
                        To be credible, however, the strategies must<br/>
                        be supported by relevant performance data<br/>
                        that convinces stakeholders, particularly<br/>
                        investors, that their expectations are<br/>

                        genuinely being met.
                    </div>
                </div>
            </div>
            <div class="row py-5 my-5"></div>
            <div class="row mb-5 mt-150px">
                <div class="col-sm-12 col-md-4 text-center">
                    <div class="mw-360px mx-auto">
                        <div class="Title--type-2 text-white text-right">
                            ADAPT
                        </div>
                        <div class="Paragraph--type-1 text-right">
                            Public policy and stakeholder expectations are<br/>
                            dynamic, so companies must try to anticipate<br/>
                            future changes through ongoing dialogue.<br/>
                            In particular, they must work to satisfy<br/>
                            investors’ evolving ESG agenda while<br/>
                            maintaining immediate profitability.
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-4"></div>
                <div class="col-sm-12 col-md-4 text-center">
                    <div class="mw-360px mx-auto">
                        <div class="Title--type-2 text-white text-left">
                            PROTECT
                        </div>
                        <div class="Paragraph--type-1 text-left">
                            To preserve profitability and freedom to operate,<br/>
                            businesses must proactively work towards<br/>
                            sustainability goals, internal and societal.<br/>
                            This brings risks as well as opportunities:<br/>
                            non-compliance or “greenwashing” could bring<br/>
                            reputational damage, while ineective initiatives<br/>
                            could waste money or incur penalties.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <section class="Home__Perspectives">
        <div class="container-large-md container">
            <div class="Home__Perspectives__Title">
                <h2 class="Home__Perspectives__Title-Text"><?php echo $sections['perspectives']['title']; ?></h2>
            </div>
            <div class="Home__Perspectives__Slider-Wrapper">

                <div class="Home__Perspectives__Slider-Wrapper__Slider">
                    <div class="swipper-container">
                        <div class="Home__Perspectives-Slider-Button button-previous">
                            <img src="/content/img/left-arrow-navigation-v1.svg"/>
                        </div>
                        <div class="swiper-wrapper">

                            <div class="swiper-slide">
                                <div class="Home__Perspectives__Card">
                                    <div class="Home__Perspectives__Card__Header">
                                        <div class="Home__Perspectives__Card__Header-Image-Border border-line--type-4"></div>
                                        <div class="Home__Perspectives__Card__Header-Image-Placeholder" style="background-size:cover; background-image: url(/content/img/Crop-4-01.png)"></div>

                                    </div>
                                    <div class="Home__Perspectives__Card__Description">
                                        <p class="color-economic-impact">
                                            Now is the time for companies to strengthen their ESG approach
                                        </p>
                                        <a class="color-economic-impact Home__Perspectives__Card__Description-Link"
                                           href="/economic-impact-sustainability/climate-image-a-new-growth-trajectory">
                                            READ MORE
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="swiper-slide">
                                <div class="Home__Perspectives__Card">
                                    <div class="Home__Perspectives__Card__Header">
                                        <div class="Home__Perspectives__Card__Header-Image-Border border-line--type-4"></div>
                                        <div class="Home__Perspectives__Card__Header-Image-Placeholder" style="background-size:cover; background-image: url(/content/img/Crop-5-01.png)"></div>

                                    </div>
                                    <div class="Home__Perspectives__Card__Description">
                                        <p class="color-economic-impact">                    A Test of Resilience: COVID-19 and the Business of Europe’s Green Deal

                                        </p>
                                        <a class="color-economic-impact Home__Perspectives__Card__Description-Link"
                                           href="/economic-impact-sustainability/A-Test-of-Resilience-COVID-19">
                                            READ MORE
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="swiper-slide">
                                <div class="Home__Perspectives__Card">
                                    <div class="Home__Perspectives__Card__Header">
                                        <div class="Home__Perspectives__Card__Header-Image-Border border-line--type-4"></div>
                                        <div class="Home__Perspectives__Card__Header-Image-Placeholder" style="background-size:cover; background-image: url(/content/img/Crop-2-01.png)"></div>

                                    </div>
                                    <div class="Home__Perspectives__Card__Description">
                                        <p class="color-economic-impact">
                                            Time to Rethink the ‘S’ in ESG
                                        </p>
                                        <a class="color-economic-impact Home__Perspectives__Card__Description-Link"
                                           href="/economic-impact-sustainability/Time-to-Rethink-the-S-in-ESG">
                                            READ MORE
                                        </a>
                                    </div>
                                </div>
                            </div>


                        </div>
                        <div class="Home__Perspectives-Slider-Button button-next">
                            <img src="/content/img/right-arrow-navigation-v1.svg"/>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <?php include 'exploreanotherlever.php'; ?>
</main>
