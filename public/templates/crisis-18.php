<main class="Home">
    <?php include 'perspectiveheader.php'; ?>

    <div class="Section--background-perspectives">
        <div class="container-large-md container">
            <div class="row">
                <div class="col-sm-12 col-md-8 Section--bg-white py-5">
                    <div class="Title--type-2 color-dark-blue mb-4">XX JULY 2020</div>
                    <hr class="HR HR--type-1">
                    <h1 class="Title--type-7 mt-3">
                        The New T&Cs of Investor Relations<br/>An Open Letter to the FCO
                    </h1>
                    <b>
                        The coronavirus pandemic will result in increased scrutiny on how your company treats employees,
                        clients, and the wider community. This scrutiny creates an imperative to embrace new levels of
                        transparency and compassion (T&C) when communicating with your investors.
                    </b>
                    <p>
                        In this short article, we speculate on the impact of the pandemic on investor relations and make
                        recommendations for how CFOs need to adapt to new socio-economic realities.
                    </p>
                    <b>The pandemic will revive the triple-bottom-line ideal</b>
                    <p>
                        A triple-bottom-line refers to the pursuit of profit in a way that also creates value for people
                        and the planet. Going forward, we expect this ideal will resonate with the public to a much
                        higher degree than before. Why? Because conversations on these two topics will be difficult to
                        separate from a discussion about profits in a post-pandemic world. New precedents will be set,
                        and we expect them to have enduring effects.
                    </p>
                    <b>Your investors will pursue increased ESG compliance</b>
                    <p>
                        If you are like most CFOs, you’ll be skeptical about our claim. And for good reasons: qualified
                        investors have traditionally relied on decision-making processes that made little or no
                        allowance for environmental, social and governance (ESG) matters. A simple glance at the latest
                        sell-side analyst reports will likely show us a continuation of that approach.
                    </p>
                    <p>
                        But things are already changing. Even before the pandemic, we’ve noticed a growing number of
                        institutional investors prioritizing ESG-compliant targets. We’ve seen retail investors
                        increasingly susceptible to storytelling pouring out of media outlets on these topics. And
                        COVID-19 is only likely to accelerate these trends.
                    </p>
                </div>
                <div class="col-sm-12 col-md-4 Section--sidebar">
                    <div class="Title--type-2 color-dark-blue mb-4">&nbsp;</div>
                    <div class="SocialBox">
                        SHARE THIS CONTENT
                        <ul class="SocialShare SocialShare--align-left mt-3">
                            <li class="SocialShare__Item">
                                <a class="SocialShare__Item__Link" href="#" target="_blank">
                                    <img class="SocialShare__Item__Link-Icon" src="../assets/img/facebook-icon-v2.svg">
                                </a>
                            </li>
                            <li class="SocialShare__Item">
                                <a class="SocialShare__Item__Link" href="#" target="_blank">
                                    <img class="SocialShare__Item__Link-Icon" src="../assets/img/twitter-icon-v2.svg">
                                </a>
                            </li>
                            <li class="SocialShare__Item">
                                <a class="SocialShare__Item__Link" href="#" target="_blank">
                                    <img class="SocialShare__Item__Link-Icon" src="../assets/img/linkedin-icon-v2.svg">
                                </a>
                            </li>
                            <li class="SocialShare__Item">
                                <a class="SocialShare__Item__Link" href="#" target="_blank">
                                    <img class="SocialShare__Item__Link-Icon" src="../assets/img/share-icon-v1.svg">
                                </a>
                            </li>
                        </ul>
                    </div>

                    <div class="Document mt-5">
                        <img src="../assets/img/documents/covid-19-new-tcs-investor-relations.png" class="img-fluid"/>
                    </div>

                    <a href="../assets/img/documents/covid-19-new-tcs-investor-relations.pdf" target="_blank"
                       class="Button Button--type-1 mt-3">
                        DOWNLOAD >
                    </a>
                </div>
            </div>
        </div>
    </div>

    <?php include 'exploreothertopics.php'; ?>
</main>
