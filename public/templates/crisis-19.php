<main class="Home">
    <?php include 'perspectiveheader.php'; ?>

    <div class="Section--background-perspectives">
        <div class="container-large-md container">
            <div class="row">
                <div class="col-sm-12 col-md-8 Section--bg-white py-5">
                    <div class="Title--type-2 color-dark-blue mb-4">XX JULY 2020</div>
                    <hr class="HR HR--type-1">
                    <h1 class="Title--type-7 mt-3">
                        Rethinking RIF Workforce Optimization in Healthcare
                    </h1>
                    <b>
                        Healthcare organizations have recently been stretched extremely thin to serve as front-line
                        response to accommodate care during the COVID-19 pandemic. This has caused massive changes in
                        workforce stability, ranging from increased turnover and mass furloughs to a large increase in
                        contract labor.
                    </b>
                    <p>
                        While volumes of COVID-19 patients may be difficult to plan for in the near and medium term,
                        provider organizations are faced with more healthcare workforce decisions to be made than ever
                        before. However, we must keep in mind that workforce optimization struggles precede the
                        pandemic, and our future decisions should avoid repeating or worsening the trends of the past.
                    </p>
                    <b>Labor Expense: A Constant Concern</b>
                    <p>
                        Expense growth is outpacing revenue growth in many healthcare organizations across the country.
                        Before COVID-19, salary and benefits combined were the largest expense line item and often
                        accounted for more than 50 percent of total operating costs for healthcare organizations. With
                        revenue loss and financial pressure caused by the pandemic, most of the nation’s provider
                        organizations are looking for a way to reduce labor expense.
                    </p>

                    <p>
                        Rather than immediately begin planning for a Reduction in Force (RIF), it is increasingly
                        important that organizations start by properly managing their workforces. Not only can workforce
                        optimization programs save money, they can also avoid the negative publicity related to staff
                        layoffs. Effective workforce optimization programs manage full-time employee (FTE) counts and
                        increase staff productivity and engagement. Improved engagement can decrease staff turnover, a
                        large and often unnecessary cost.
                    </p>
                    <b>
                        Reductions in Force

                    </b>
                    <p>
                        RIFs are a widespread issue, and especially so as a result of the ongoing COVID-19 pandemic,
                        which has affected nearly every healthcare organization in some way. Becker’s Healthcare reports
                        that 221 hospitals have defaulted to large-scale RIFs in response to COVID-19. However, even
                        when accounting for potential skews due to COVID-19, we see that some geographic areas are
                        significantly more prone to turnover and layoffs than others.
                    </p>
                </div>
                <div class="col-sm-12 col-md-4 Section--sidebar">
                    <div class="Title--type-2 color-dark-blue mb-4">&nbsp;</div>
                    <div class="SocialBox">
                        SHARE THIS CONTENT
                        <ul class="SocialShare SocialShare--align-left mt-3">
                            <li class="SocialShare__Item">
                                <a class="SocialShare__Item__Link" href="#" target="_blank">
                                    <img class="SocialShare__Item__Link-Icon" src="../assets/img/facebook-icon-v2.svg">
                                </a>
                            </li>
                            <li class="SocialShare__Item">
                                <a class="SocialShare__Item__Link" href="#" target="_blank">
                                    <img class="SocialShare__Item__Link-Icon" src="../assets/img/twitter-icon-v2.svg">
                                </a>
                            </li>
                            <li class="SocialShare__Item">
                                <a class="SocialShare__Item__Link" href="#" target="_blank">
                                    <img class="SocialShare__Item__Link-Icon" src="../assets/img/linkedin-icon-v2.svg">
                                </a>
                            </li>
                            <li class="SocialShare__Item">
                                <a class="SocialShare__Item__Link" href="#" target="_blank">
                                    <img class="SocialShare__Item__Link-Icon" src="../assets/img/share-icon-v1.svg">
                                </a>
                            </li>
                        </ul>
                    </div>

                    <div class="Document mt-5">
                        <img src="../assets/img/documents/covid-19-rethinking-rif-workforce-optimization-healthcare.png"
                             class="img-fluid"/>
                    </div>

                    <a href="../assets/img/documents/covid-19-rethinking-rif-workforce-optimization-healthcare.pdf"
                       target="_blank" class="Button Button--type-1 mt-3">
                        DOWNLOAD >
                    </a>
                </div>
            </div>
        </div>
    </div>

    <?php include 'exploreothertopics.php'; ?>
</main>
