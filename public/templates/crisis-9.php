<main class="Home">
    <?php include 'perspectiveheader.php'; ?>

    <div class="Section--background-perspectives">
        <div class="container-large-md container">
            <div class="row">
                <div class="col-sm-12 col-md-8 Section--bg-white py-5">
                    <div class="Title--type-2 color-dark-blue mb-4">XX JULY 2020</div>
                    <hr class="HR HR--type-1">
                    <h1 class="Title--type-7 mt-3">
                        Time to Rethink the ‘S’ in ESG
                    </h1>
                    <b>
                        COVID-19 prompts increased focus on a new ‘S’: The Stakeholder.
                    </b>
                    <p>
                        Almost two years ago, a teenage girl, Greta Thunberg, started her climate protest outside the
                        Swedish parliament, spawning a movement that captured the world’s attention. Over the same
                        period, what was less apparent was the shift in capital markets to place greater emphasis on
                        Environmental, Social & Governance or ‘ESG’ issues at companies around the world. In 2019, an
                        incremental US$70bn is estimated to have been invested in ESG funds while traditional equity
                        funds suffered almost US$200bn of outflows.
                    </p>
                    <p>
                        What was once a marginal consideration for investors is now front and centre. Since Enron’s
                        collapse in 2001, and subsequent failings that led to the financial crisis in 2008, governance
                        has been high on the corporate agenda. Recently, as regulators, investors and activists have
                        pressurised companies, environmental issues have also come to the fore. ESG factors are also
                        increasingly relevant in the cost of debt– both S&P Global and Moody’s acquired ESG ratings
                        agencies in the past year and are now including those ratings within their overall credit rating
                        criteria.
                    </p>
                    <p>
                        Despite all this, as a measure, the ‘S’ in ESG – the ’Social’ – has been somewhat left behind
                        and remains the hardest to define. ‘S’ factors impact businesses every day – customer or product
                        quality issues, data security, industrial relations or supply-chain difficulties – often causing
                        significant reputational damage. Think of the perceptions of how some retailers have treated
                        workers; the damage to FMCG brands of links with child labour; and the loss of confidence in
                        banks when IT systems fail and customer transactions can’t be honoured. These are all factors
                        which fall within the ‘S’.

                    </p>
                </div>
                <div class="col-sm-12 col-md-4 Section--sidebar">
                    <div class="Title--type-2 color-dark-blue mb-4">&nbsp;</div>
                    <div class="SocialBox">
                        SHARE THIS CONTENT
                        <ul class="SocialShare SocialShare--align-left mt-3">
                            <li class="SocialShare__Item">
                                <a class="SocialShare__Item__Link" href="#" target="_blank">
                                    <img class="SocialShare__Item__Link-Icon" src="../assets/img/facebook-icon-v2.svg">
                                </a>
                            </li>
                            <li class="SocialShare__Item">
                                <a class="SocialShare__Item__Link" href="#" target="_blank">
                                    <img class="SocialShare__Item__Link-Icon" src="../assets/img/twitter-icon-v2.svg">
                                </a>
                            </li>
                            <li class="SocialShare__Item">
                                <a class="SocialShare__Item__Link" href="#" target="_blank">
                                    <img class="SocialShare__Item__Link-Icon" src="../assets/img/linkedin-icon-v2.svg">
                                </a>
                            </li>
                            <li class="SocialShare__Item">
                                <a class="SocialShare__Item__Link" href="#" target="_blank">
                                    <img class="SocialShare__Item__Link-Icon" src="../assets/img/share-icon-v1.svg">
                                </a>
                            </li>
                        </ul>
                    </div>

                    <div class="Document mt-5">
                        <img src="../assets/img/documents/Time to Rethink the S in ESG May 2020.png" class="img-fluid"/>
                    </div>

                    <a href="../assets/img/documents/Time to Rethink the S in ESG May 2020.pdf" target="_blank"
                       class="Button Button--type-1 mt-3">
                        DOWNLOAD >
                    </a>
                </div>
            </div>
        </div>
    </div>

    <?php include 'exploreothertopics.php'; ?>
</main>
