<main class="Home">
    <div class="Background--Gradient-Blue-Black">
        <?php require __DIR__ . '/../components/leverCarousel.php'; ?>
    </div>

    <div class="container-large-md container mt-3 mt-lg-5">
    </div>
    <section class="Section--decoration-right">
        <div class="container-large-md container">
            <div class="row pt-0 pt-lg-5">
                <div class="col-sm-12 col-md-6">
                    <h2 class="Title--type-1 mb-3">The pandemic may have ushered in a new world, but the basic crisis
                        management principles are as relevant as ever.</h2>
                    <p class="Paragraph--type-3">
                        COVID-19 escalated into a crisis of unprecedented scale and complexity,
                        for which few organisations were prepared. According to FTI’s Resilience
                        Barometer™ 2020, only 26% of organisations had invested in crisis
                        management in the previous 12 months. Investors are only too aware of
                        this oversight: just 48% believe companies can manage crises effectively.
                    </p>
                </div>
                <div class="col-sm-12 col-md-6">
                    <div class="mw-300px mx-auto">
                        <div class="Number">
                            58%
                            <div class="Number__bar">
                                <div class="Number__bar-full" style="width: 58%">

                                </div>
                            </div>
                        </div>
                        <p class="mt-3 Paragraph--type-2">
                            of executives spent 5 hours or more a
                            day thinking about dealing with crises
                        </p>
                        <p class="mt-5 mb-0 Paragraph--type-2">
                            Companies which have experienced
                            a crisis are subject to
                        </p>
                        <div class="Number">
                            35x
                        </div>
                        <p class="Paragraph--type-2">
                            the media attention following that crisis
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="Section--decoration-left mb-5">
        <div class="container-large-md container">
            <div class="row mt-5">
                <div class="col-sm-12 col-md-6 pl-md-5">
                    <div class="Title--type-6">
                        Cyber attacks
                    </div>
                    <p class="Paragraph--type-2 my-0">
                    and
                    </p>
                    <div class="Title--type-6">
                        major product defects
                    </div>
                    <p class="Paragraph--type-2">were the two most common crises<br/>
                        experienced by respondents in 2019</p>
                </div>
                <div class="col-sm-12 col-md-6">
                    <p class="Paragraph--type-3">
                        Lack of preparedness costs dear. FTI’s Anatomy of a Crisis indicates that
                        the average share price impact from a crisis is 3.5 times greater than a
                        purely financial calculation would suggest. Reputational damage,
                        therefore, can undermine value just as much as financial damage. With
                        62% of workers wanting greater media scrutiny of company actions in
                        the event of a second COVID-19 spike, businesses must work even harder
                        than before to preserve their reputations.

                    </p>
                    <p class="Paragraph--type-3">
                        Such crisis plans as existed were at best jumping-off points for
                        companies’ COVID-19 response. The really critical factor was adaptability:
                        the ability to base decisions on new information.
                    </p>
                </div>
            </div>
        </div>

    </section>
    <section class="Section--background-gray pt-3 pb-5">
        <div class="container-large-md container">
            <div class="row my-3"></div>
            <div class="row">
                <div class="col-sm-12 mb-5">
                    <div class="Title--type-2">
                        ADOPT A HOLISTIC APPROACH ENSHRINING FUNDAMENTAL PRINCIPLES
                    </div>
                </div>
                <div class="col-sm-12 col-md-6">
                    <p class="Paragraph--type-3">
                        Adaptability depends on principles such as empathy, proactivity,
                        proportionality, decisiveness, cooperation, foresight and caution. These
                        often-neglected concepts should be at the heart of crisis management.
                    </p>
                    <p class="Paragraph--type-3">
                        Also essential is a multi-disciplinary team that collaborates and

                        example, before curtailing customer support programmes, finance teams
                        should consult customer-facing colleagues, who will tell them that, as
                        our data shows, 96% of customers expect programmes to continue
                        through any second wave.
                    </p>
                </div>
                <div class="col-sm-12 col-md-6">
                    <p class="Paragraph--type-3">
                        As well as putting the right leaders in place – crisis management
                        requires a specific skillset – it’s vital to embed the principles across
                        the organisation through training and awareness exercises.
                    </p>
                    <p class="Paragraph--type-3">
                        Enshrining crisis resilience principles in processes and culture will
                        create competitive advantage: superior ability to deal with the
                        challenges ahead.
                    </p>
                </div>

            </div>
            <div class="row mt-5 py-5">
                <div class="col">
                    <div class="Number">
                        36%
                        <div class="Number__bar">
                            <div class="Number__bar-full" style="width: 36%"></div>
                        </div>
                        <div class="Paragraph--type-2 mt-3">
                            of executives had experienced mental health problems as a result of dealing with crises
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="Number">
                        48%
                        <div class="Number__bar">
                            <div class="Number__bar-full" style="width: 48%"></div>
                        </div>
                        <div class="Paragraph--type-2 mt-3">
                            of investors think companies can manage crises effectively
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="Number">
                        26%
                        <div class="Number__bar">
                            <div class="Number__bar-full" style="width: 26%"></div>
                        </div>
                        <div class="Paragraph--type-2 mt-3">
                            of respondents had invested in crisis management in 2019
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="Section__triangle Section__triangle--type-1">
        <div class="container-large-md container py-5">
            <div class="row mt-5">
                <div class="col-sm-12 text-center">
                    <div class="Title--type-2 text-white">
                        COMPETE
                    </div>
                    <div class="Paragraph--type-1 text-white">
                        Crises drain morale, cost money, distract<br/>
                        leaders and divert attention from<br/>
                        organisational priorities. Resilience is essential<br/>
                        to staying ahead, and is achieved by building <br/>
                        the ability to predict, confront and overcome <br/>
                        rapidly escalating incidents. As the new normal <br/>
                        takes shape business leaders must examine the <br/>
                        broad crisis landscape which comes with it.
                    </div>
                </div>
            </div>
            <div class="row py-5 my-5"></div>
            <div class="row mb-5 mt-150px">
                <div class="col-sm-12 col-md-4 text-center">
                    <div class="mw-360px mx-auto">
                        <div class="Title--type-2 text-white text-right">
                            ADAPT
                        </div>
                        <div class="Paragraph--type-1 text-right">
                            The combination of new technology and an <br/>
                            increasingly unforgiving external environment <br/>
                            means crises escalate faster and inflict lasting <br/>
                            damage. Once distinct concepts, reputation  <br/>
                            and value are now totally intertwined. As the  <br/>
                            broader implications of crises become clearer,  <br/>
                            businesses must be willing to scrutinise their  <br/>
                            leadership, workforce and systems to identify<br/>
                            areas of improvement. A return to fundamental <br/>
                            crisis management principles will ensure that <br/>
                            business is prepared for emerging threats.<br/>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-4"></div>
                <div class="col-sm-12 col-md-4 text-center">
                    <div class="mw-360px mx-auto">
                        <div class="Title--type-2 text-white text-left">
                            PROTECT
                        </div>
                        <div class="Paragraph--type-1 text-left">
                            Cyber incidents, industrial accidents, media <br/>
                            scandals, global health crises… Events like <br/>
                            these can bring companies to their knees <br/>
                            overnight. The first priority for business leaders  <br/>
                            must be to optimally utilise pre-existing  <br/>
                            systems and capacities, ensuring their  <br/>
                            organisation can make effective choices under <br/>
                            pressure in response to unique scenarios.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <section class="Home__Perspectives">
        <div class="container-large-md container">
            <div class="Home__Perspectives__Title">
                <h2 class="Home__Perspectives__Title-Text"><?php echo $sections['perspectives']['title']; ?></h2>
            </div>
            <div class="Home__Perspectives__Slider-Wrapper">

                <div class="Home__Perspectives__Slider-Wrapper__Slider">
                    <div class="swipper-container">
                        <div class="Home__Perspectives-Slider-Button button-previous">
                            <img src="/content/img/left-arrow-navigation-v1.svg"/>
                        </div>
                        <div class="swiper-wrapper">

                            <div class="swiper-slide">
                                <div class="Home__Perspectives__Card">
                                    <div class="Home__Perspectives__Card__Header">
                                        <div class="Home__Perspectives__Card__Header-Image-Border border-line--type-1"></div>
                                        <div class="Home__Perspectives__Card__Header-Image-Placeholder" style="background-size:cover; background-image: url(/content/img/Crop-4-01.png)"></div>

                                    </div>
                                    <div class="Home__Perspectives__Card__Description">
                                        <p class="color-escalation">Anatomy of a Crisis I – Communicating through a crisis</p>
                                        <a class="color-escalation Home__Perspectives__Card__Description-Link"
                                           href="/escalation-planning-and-response/anatomy-of-a-crisis-I">
                                            READ MORE
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="swiper-slide">
                                <div class="Home__Perspectives__Card">
                                    <div class="Home__Perspectives__Card__Header">
                                        <div class="Home__Perspectives__Card__Header-Image-Border border-line--type-1"></div>
                                        <div class="Home__Perspectives__Card__Header-Image-Placeholder" style="background-size:cover; background-image: url(/content/img/Crop-5-01.png)"></div>

                                    </div>
                                    <div class="Home__Perspectives__Card__Description">
                                        <p class="color-escalation">The Anatomy Of A Crisis: Volume 2 – Why Do Markets Overreact To Profit Warnings?</p>
                                        <a class="color-escalation Home__Perspectives__Card__Description-Link"
                                           href="/escalation-planning-and-response/the-anatomy-of-a-crisis-volume-2">
                                            READ MORE
                                        </a>
                                    </div>
                                </div>
                            </div>


                            <div class="swiper-slide">
                                <div class="Home__Perspectives__Card">
                                    <div class="Home__Perspectives__Card__Header">
                                        <div class="Home__Perspectives__Card__Header-Image-Border border-line--type-1"></div>
                                        <div class="Home__Perspectives__Card__Header-Image-Placeholder" style="background-size:cover; background-image: url(/content/img/Crop-2-01.png)"></div>

                                    </div>
                                    <div class="Home__Perspectives__Card__Description">
                                        <p class="color-escalation">
                                            COVID-19: Is business ready for a second spike?
                                        </p>
                                        <a class="color-escalation Home__Perspectives__Card__Description-Link"
                                           href="/escalation-planning-and-response/covid-19">
                                            READ MORE
                                        </a>
                                    </div>
                                </div>
                            </div>



                        </div>
                        <div class="Home__Perspectives-Slider-Button button-next">
                            <img src="/content/img/right-arrow-navigation-v1.svg"/>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <?php include 'exploreanotherlever.php'; ?>
</main>
