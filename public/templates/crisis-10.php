<main class="Home">
    <?php include 'perspectiveheader.php'; ?>

    <div class="Section--background-perspectives">
        <div class="container-large-md container">
        <div class="row">
            <div class="col-sm-12 col-md-8 Section--bg-white py-5">
                <div class="Title--type-2 color-dark-blue mb-4">XX JULY 2020</div>
                <hr class="HR HR--type-1">
                <h1 class="Title--type-7 mt-3">
                    The Anatomy of a Crisis #3 – Cyber Breaches
                </h1>
                <b>
                    What can cyber breaches of the past 10 years teach us about how to prepare for them in the future?
                </b>
                <p>
                    Our research tells us that a cyber breach is the top concern for boards and management teams. They prioritise it over technological disruption concerns, product defects and even trade restrictions and they report lost revenue, customers and employees among the consequences. And yet we see that fewer than half of those business leaders are preparing to manage their cyber risk proactively in the year ahead. We believe that this paradox is worth investigating. In the third volume of The Anatomy of a Crisis, we aim to investigate the cyber landscape in more detail.
                </p>
            </div>
            <div class="col-sm-12 col-md-4 Section--sidebar">
                <div class="Title--type-2 color-dark-blue mb-4">&nbsp;</div>
                <div class="SocialBox">
                    SHARE THIS CONTENT
                    <ul class="SocialShare SocialShare--align-left mt-3">
                        <li class="SocialShare__Item">
                            <a class="SocialShare__Item__Link" href="#" target="_blank">
                                <img class="SocialShare__Item__Link-Icon" src="../assets/img/facebook-icon-v2.svg">
                            </a>
                        </li>
                        <li class="SocialShare__Item">
                            <a class="SocialShare__Item__Link" href="#" target="_blank">
                                <img class="SocialShare__Item__Link-Icon" src="../assets/img/twitter-icon-v2.svg">
                            </a>
                        </li>
                        <li class="SocialShare__Item">
                            <a class="SocialShare__Item__Link" href="#" target="_blank">
                                <img class="SocialShare__Item__Link-Icon" src="../assets/img/linkedin-icon-v2.svg">
                            </a>
                        </li>
                        <li class="SocialShare__Item">
                            <a class="SocialShare__Item__Link" href="#" target="_blank">
                                <img class="SocialShare__Item__Link-Icon" src="../assets/img/share-icon-v1.svg">
                            </a>
                        </li>
                    </ul>
                </div>

                <div class="Document mt-5">
                    <img src="../assets/img/documents/FTI-Consulting-Anatomy-of-a-crisis-3-Cyber.png" class="img-fluid"/>
                </div>

                <a href="../assets/img/documents/FTI-Consulting-Anatomy-of-a-crisis-3-Cyber.pdf" target="_blank" class="Button Button--type-1 mt-3">
                    DOWNLOAD >
                </a>
            </div>
        </div>
        </div>
    </div>

    <?php include 'exploreothertopics.php'; ?>
</main>
