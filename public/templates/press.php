<main class="economic-impact-and-sustainability">
    <div class="Background--Gradient-Blue-Black">
        <div class="LeverCarousel__bg LeverCarousel__bg--type-8">
            <div class="container-large-md container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="LeverCarousel__container">

                            <div class="LeverCarousel__navigation-container">

                            </div>
                            <div class="LeverCarousel__title">
                                <img class="LeverCarousel__decoration Animate Animate--slide-to-left" src="content/img/home-header-detail.svg">
                                Press
                            </div>
                            <div class="LeverCarousel__quote LeverCarousel__quote--type-1">
                                FTI Consulting experts are available to discuss how companies  <br/>
                                can remain resilient in the face of increasingly complex risks.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="color-blue900-bg">
        <div class="container-large-md container py-5">
        <div class="row">
            <div class="col-sm-12 mb-5">
                <div class="Title--type-2 color-white">
                    ADOPT A HOLISTIC APPROACH ENSHRINING FUNDAMENTAL PRINCIPLES
                </div>
            </div>
            <div class="col-sm-2">
                <div class="Person">
                    <div class="Person__image">
                        <img src="./assets/img/team-members/matthew_bashala.png">
                    </div>

                </div>
            </div>
            <div class="col-sm-10">
                <div class="">
                    <div class="Title--type-4 color-white mt-3 font-weight-bold">Matthew Bashalany</div>
                    <div class="Title--type-4 color-white my-3">
                        FTI Consulting Corporate Communications
                    </div>
                    <a href="mailto:matthew.bashalany@fticonsulting.com" class="Title--type-4 color-white mt-3 font-weight-bold">matthew.bashalany@fticonsulting.com</a>
                </div>
            </div>
        </div>
        </div>
    </div>

    <div class="Section--background-press">
        <div class="Section__press_bg__left"></div>
        <div class="Section__press_bg__right"></div>
        <div class="container-large-md container py-5">
            <div class="row">
                <div class="col-sm-5">

                    <div class="Title--type-1 color-white my-3">
                        FTI Consulting Survey<br/>
                        Finds Business Resilience<br/>
                        Remains Low Despite<br/>
                        Persistent Risks
                    </div>
                    <div class="Title--type-9 color-white font-weight-bold">
                        JANUARY 21, 2020
                    </div>
                </div>
                <div class="col-sm-7 d-flex flex-column justify-content-center pl-5">
                    <div class="Title--type-9 color-dark-blue font-weight-400 mb-2">
                        &bull; Average Score in the Annual FTI Resilience  Barometer Rises<br/>
                        Slightly, but 84% of G-20 Companies Expect a Crisis in 2020
                    </div>
                    <div class="Title--type-9 color-dark-blue font-weight-400 mb-2">
                        &bull; Business Leaders Focused on Cybersecurity, Financial Crime and<br/>
                        Regulation in 2020
                    </div>
                    <div class="Title--type-9 color-dark-blue font-weight-400 mb-2">
                        &bull; Companies Must Embrace Technology Transformation to Remain<br/>
                        Viable and Fight New Threats
                    </div>
                    <a href="#" class="read-more">
                        READ MORE
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="Section--background-gray">
        <div class="container-large-md container py-5">
            <div class="row">
                <div class="col-sm-12">
                    <div class="Title--type-2 font-weight-normal">
                        FTI RESILIENCE BAROMETER IN THE NEWS
                    </div>
                </div>
            </div>

            <div class="row py-4">
                <div class="col-sm-12 col-md-4 mb-4">
                    <div class="Post Post--type-2">
                        <div class="Post__wrapper">
                            <div class="Post__content"><div class="Post__date">28 July 2020</div>
                                
                                <div class="Post__title">FTI Consulting’s Resilience Barometer 2020</div>
                                <div class="Post__excerpt Post__excerpt--type-2">Corporate Compliance Insights</div>
                                <a href="#" class="Post__read_more Post__read_more--type-2">READ MORE&gt;</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12 col-md-4 mb-4">
                    <div class="Post Post--type-2">
                        <div class="Post__wrapper">
                            
                            <div class="Post__content"><div class="Post__date">28 July 2020</div>

                                <div class="Post__title">
                                    Companies unprepared
                                    for cyber, financial crime
                                    and regulatory threats
                                </div>
                                <div class="Post__excerpt Post__excerpt--type-2">Arab News</div>
                                <a href="#" class="Post__read_more Post__read_more--type-2">READ MORE&gt;</a>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="col-sm-12 col-md-4 mb-4">
                    <div class="Post Post--type-2">
                        <div class="Post__wrapper">
                            
                            <div class="Post__content"><div class="Post__date">28 July 2020</div>

                                <div class="Post__title">
                                    Fight fraud by staying
                                    alert
                                </div>
                                <div class="Post__excerpt Post__excerpt--type-2">FT Advertiser</div>
                                <a href="#" class="Post__read_more Post__read_more--type-2">READ MORE&gt;</a>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="col-sm-12 col-md-4 mb-4">
                    <div class="Post Post--type-2">
                        <div class="Post__wrapper">
                            
                            <div class="Post__content"><div class="Post__date">28 July 2020</div>

                                <div class="Post__title">FTI Consulting – Resilience Barometer</div>
                                <div class="Post__excerpt Post__excerpt--type-2">Invest Africa</div>
                                <a href="#" class="Post__read_more Post__read_more--type-2">READ MORE&gt;</a>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="col-sm-12 col-md-4 mb-4">
                    <div class="Post Post--type-2">
                        <div class="Post__wrapper">
                            
                            <div class="Post__content"><div class="Post__date">28 July 2020</div>

                                <div class="Post__title">FTI Consulting Adds Seven Cybersecurity Professionals in EMEA and Asia Pacific</div>
                                <div class="Post__excerpt Post__excerpt--type-2">IYahoo Finance </div>
                                <a href="#" class="Post__read_more Post__read_more--type-2">READ MORE&gt;</a>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="col-sm-12 col-md-4 mb-4">
                    <div class="Post Post--type-2">
                        <div class="Post__wrapper">
                            
                            <div class="Post__content"><div class="Post__date">28 July 2020</div>

                                <div class="Post__title">FTI Consulting Resilience Barometer Sheds Light on Lack of Business Preparedness</div>
                                <div class="Post__excerpt Post__excerpt--type-2">Wealth and Finance</div>
                                <a href="#" class="Post__read_more Post__read_more--type-2">READ MORE&gt;</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row text-center mb-5">
                <div class="Pagination mx-auto">
                    <a href="#" class="Pagination__item Pagination__arrow_left"></a>
                    <a href="#" class="Pagination__item Pagination--active">1</a>
                    <a href="#" class="Pagination__item">2</a>
                    <a href="#" class="Pagination__item">3</a>
                    <a href="#" class="Pagination__item">4</a>
                    <a href="#" class="Pagination__item Pagination__arrow_right"></a>
                </div>
            </div>
        </div>
    </div>
</main>
