<div class="Background--Gradient-Blue-Black">
    <div class="LeverCarousel__bg LeverCarousel__bg--type-8"
         style="background-image: url(./assets/img/header-bg/perspectives-header-bg-detail-v1.svg)">
        <div class="container-large-md container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="LeverCarousel__container">
                        <div class="LeverCarousel__navigation-container"></div>
                        <div class="LeverCarousel__title"><img
                                class="LeverCarousel__decoration Animate Animate--slide-to-left"
                                src="../assets/img/home-header-detail.svg"> Perspectives
                        </div>
                        <div class="LeverCarousel__quote LeverCarousel__quote--type-1"> Praesent commodo cursus
                            magna,<br/> vel scelerisque nisl consectetur et.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
