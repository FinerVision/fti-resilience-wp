<div class="Section--blue pb-5">
    <div class="container-large-md container pt-5">
        <div class="row py-5">
            <div class="Title--type-2 mx-auto text-white">
                EXPLORE OTHER TOPICS
            </div>
        </div>
        <div class="row mb-5">
            <div class="col-sm-12 col-md-3">
                <div class="OtherTopic">
                    <div class="OtherTopic__wrapper OtherTopic__wrapper--dark-blue">
                        <div class="OtherTopic__title">COVID-19</div>
                        <a class="OtherTopic__read_more OtherTopic__read_more--dark-blue" href="/covid">READ MORE></a>
                    </div>
                </div>
            </div>
<!--            <div class="col-sm-12 col-md-3">-->
<!--                <div class="OtherTopic">-->
<!--                    <div class="OtherTopic__wrapper OtherTopic__wrapper--turquoise">-->
<!--                        <div class="OtherTopic__title">Regions</div>-->
<!--                        <a class="OtherTopic__read_more OtherTopic__read_more--turquoise" href="#">READ MORE></a>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="col-sm-12 col-md-3">-->
<!--                <div class="OtherTopic">-->
<!--                    <div class="OtherTopic__wrapper OtherTopic__wrapper--blue">-->
<!--                        <div class="OtherTopic__title">Inudstries</div>-->
<!--                        <a class="OtherTopic__read_more OtherTopic__read_more--blue" href="#">READ MORE></a>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="col-sm-12 col-md-3">-->
<!--                <div class="OtherTopic">-->
<!--                    <div class="OtherTopic__wrapper OtherTopic__wrapper--gray">-->
<!--                        <div class="OtherTopic__title">General Counsel</div>-->
<!--                        <a class="OtherTopic__read_more OtherTopic__read_more--gray" href="#">READ MORE></a>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
        </div>
    </div>

</div>
