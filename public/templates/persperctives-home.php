<main class="Home">
    <div class="Background--Gradient-Blue-Black">
        <div class="LeverCarousel__bg LeverCarousel__bg--type-8" style="background-image: url(./assets/img/header-bg/perspectives-header-bg-detail-v1.svg)">
            <div class="container-large-md container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="LeverCarousel__container">

                            <div class="LeverCarousel__navigation-container">

                            </div>
                            <div class="LeverCarousel__title">
                                <img class="LeverCarousel__decoration Animate Animate--slide-to-left" src="content/img/home-header-detail.svg">
                                Perspectives
                            </div>
                            <div class="LeverCarousel__quote LeverCarousel__quote--type-1">
                                Resilience is the intersection of preparedness and risk. <br/>
                                Explore our latest insights into the challenges facing business leaders.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="Section--bg-perspectives-home">
        <div class="my-5 py-5">
            <div class="perspective-swipper-container">
                <!-- Additional required wrapper -->
                <div class="swiper-wrapper">
                    <!-- Slides -->

                    <div class="swiper-slide">
                        <div class="">

                            <div class="container-large-md container">
                                <div class="row">
                                    <div class="PostCarousel__button PostCarousel__button-prev"></div>

                                    <div class="col-sm-12">
                                        <div class="Title--type-2">LATEST</div>
                                        <hr class="HR--type-1 mb-5"/>
                                    </div>
                                    <div class="col-sm-12 col-md-6">
                                        <div class="PostCarousel__title">
                                            COVID:19: Effective Internal Comms in Unprecedented Times
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-6">
                                        <div class="PostCarousel__excerpt">
                                            <p>
                                                Amid public angst, capital market volatility and other
                                                uncertainties, companies are increasingly being scrutinized
                                                for their response to COVID-19.
                                            </p>
                                            <p>
                                                Within that context, companies must evolve their internal
                                                Within that context, companies must evolve their internal
                                                Within that context, companies must evolve their internal


                                                communications with employees and other stakeholders that is
                                                designed to address the disruption and uncertainty.
                                            </p>
                                        </div>

                                        <a href="" class="PostCarousel__read_more">READ MORE></a>
                                    </div>
                                    <div class="PostCarousel__button PostCarousel__button-next"></div>

                                </div>
                            </div>

                        </div>
                    </div>

                </div>

            </div>

        </div>
    </div>
    <div class="Section--decoration-right2">
        <div class="container-large-md container pt-5">
            <div class="row py-5">
                <div class="col-sm-12">
                    <div class="Title--type-2 text-white">
                        FEATURED TOPICS
                    </div>
                </div>
            </div>
            <div class="row mb-5">
                <div class="col-sm-12 col-md-3">
                    <div class="OtherTopic">
                        <div class="OtherTopic__wrapper OtherTopic__wrapper--dark-blue">
                            <div class="OtherTopic__title">COVID-19</div>
                            <a class="OtherTopic__read_more OtherTopic__read_more--dark-blue" href="/covid">READ MORE></a>
                        </div>
                    </div>
                </div>

            </div>

            <div class="row py-4">
                <div class="col-sm-12">
                    <div class="Title--type-2 text-white">ALL ARTICLES</div>
                </div>
            </div>
        </div>

    </div>


    <div class="Section--bg-dark-gray">
        <div class="container-large-md container pt-5">
            <div class="row py-4">
                <div class="col-sm-12 col-md-4 mb-4">
                    <div class="Input">
                        <input class="Input__element" type="text" placeholder="SEARCH"/>
                        <span class="Input__element Input__icon Input__icon--search"></span>
                    </div>
                </div>
                <div class="col-sm-12 col-md-4 mb-4">
                    <div class="Input">
                        <input class="Input__element" type="text" placeholder="FILTER"/>
                        <span class="Input__element Input__icon Input__icon--filter"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div>
        <div class="container-large-md container">
            <div class="row py-4">
                <div class="col-sm-12 col-md-4 mb-4">
                    <div class="Post">
                        <div class="Post__wrapper">
                            <div class="Post__head"></div>
                            <div class="Post__content">

                                <div class="Post__title">Rethinking RIF Workforce Optimization in Healthcare</div>
                                <div class="Post__excerpt">Healthcare organizations have recently been stretched extremely</div>
                                <a href="#" class="Post__read_more">READ MORE></a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12 col-md-4 mb-4">
                    <div class="Post">
                        <div class="Post__wrapper">
                            <div class="Post__head"></div>
                            <div class="Post__content">

                                <div class="Post__title">How Will Data Fare in the Global Fight Against the Pandemic?</div>
                                <div class="Post__excerpt">Data-harnessing companies on both
                                    sides of the Atlantic face immense</div>
                                <a href="#" class="Post__read_more">READ MORE></a>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="col-sm-12 col-md-4 mb-4">
                    <div class="Post">
                        <div class="Post__wrapper">
                            <div class="Post__head"></div>
                            <div class="Post__content">

                                <div class="Post__title">The Anatomy of a Crisis #3 – Cyber Breaches</div>
                                <div class="Post__excerpt">What can cyber breaches of the past 10 years teach us about how to prepare for them in the future?</div>
                                <a href="#" class="Post__read_more">READ MORE></a>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="col-sm-12 col-md-4 mb-4">
                    <div class="Post">
                        <div class="Post__wrapper">
                            <div class="Post__head"></div>
                            <div class="Post__content">

                                <div class="Post__title">Climate Image: A New
                                    Growth Trajectory?</div>
                                <div class="Post__excerpt">It can take decades to build the
                                    desired reputation and the press of
                                    a button to destroy one. From</div>
                                <a href="#" class="Post__read_more">READ MORE></a>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="col-sm-12 col-md-4 mb-4">
                    <div class="Post">
                        <div class="Post__wrapper">
                            <div class="Post__head"></div>
                            <div class="Post__content">

                                <div class="Post__title">Anatomy of a Crisis I –
                                    Communicating
                                    through a crisis</div>
                                <div class="Post__excerpt">In this age of round-the-clock
                                    company scrutiny, we see almost as</div>
                                <a href="#" class="Post__read_more">READ MORE></a>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="col-sm-12 col-md-4 mb-4">
                    <div class="Post">
                        <div class="Post__wrapper">
                            <div class="Post__head"></div>
                            <div class="Post__content">

                                <div class="Post__title">The CEO Brand and Its
                                    Impact On Business</div>
                                <div class="Post__excerpt">New research by FTI Consulting
                                    sheds light on the role of the CEO on
                                    a company’s valuation</div>
                                <a href="#" class="Post__read_more">READ MORE></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row text-center mb-5">
                <div class="Pagination mx-auto">
                    <a href="#" class="Pagination__item Pagination__arrow_left"></a>
                    <a href="#" class="Pagination__item Pagination--active">1</a>
                    <a href="#" class="Pagination__item">2</a>
                    <a href="#" class="Pagination__item">3</a>
                    <a href="#" class="Pagination__item">4</a>
                    <a href="#" class="Pagination__item Pagination__arrow_right"></a>
                </div>
            </div>
        </div>
    </div>
</main>
