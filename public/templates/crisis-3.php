<main class="Home">
    <?php include 'perspectiveheader.php'; ?>


    <div class="Section--background-perspectives">
        <div class="container-large-md container">
        <div class="row">
            <div class="col-sm-12 col-md-8 Section--bg-white py-5">
                <div class="Title--type-2 color-dark-blue mb-4">XX JULY 2020</div>
                <hr class="HR HR--type-1">
                <h1 class="Title--type-7 mt-3">
                    COVID-19: Is business ready for a second spike?
                </h1>

                <b>
                    What about the second spike?

                </b>
                <p>
                    As the corporate world emerges blinking into the sunlight following its extended hibernation, communicators will rightly observe that a lot has changed in our world as well. The regularity, content and tone of corporate statements have all changed beyond recognition in the past three months. One wonders when things will return to normal or if indeed, they ever will.
                </p>
                <p>
                    The pandemic has created a sense of ‘we’re all in this together’ and the wide range of supportive initiatives from companies has been as heart-warming as it has been surprising. When you think back to the confrontational mood at the start of the year, a lot has changed in a short space of time.
                </p>
                <p>
                    Now, as businesses turn their attention to the return to work, there is still the looming prospect of a second spike. Dominic Raab has warned that a second spike would "prolong the economic pain we are all going through”. Dr Hans Kluge, director for the World Health Organisation, has warned that European countries should brace themselves for a second wave of coronavirus infections, saying that now is the "time for preparation, not celebration".

                </p>
                <p>
                    And yet, it seems that the topic isn’t yet featuring in the business conversation as prominently it should be.
                </p>
            </div>
            <div class="col-sm-12 col-md-4 Section--sidebar">
                <div class="Title--type-2 color-dark-blue mb-4">&nbsp;</div>
                <div class="SocialBox">
                    SHARE THIS CONTENT
                    <ul class="SocialShare SocialShare--align-left mt-3">
                        <li class="SocialShare__Item">
                            <a class="SocialShare__Item__Link" href="#" target="_blank">
                                <img class="SocialShare__Item__Link-Icon" src="../assets/img/facebook-icon-v2.svg">
                            </a>
                        </li>
                        <li class="SocialShare__Item">
                            <a class="SocialShare__Item__Link" href="#" target="_blank">
                                <img class="SocialShare__Item__Link-Icon" src="../assets/img/twitter-icon-v2.svg">
                            </a>
                        </li>
                        <li class="SocialShare__Item">
                            <a class="SocialShare__Item__Link" href="#" target="_blank">
                                <img class="SocialShare__Item__Link-Icon" src="../assets/img/linkedin-icon-v2.svg">
                            </a>
                        </li>
                        <li class="SocialShare__Item">
                            <a class="SocialShare__Item__Link" href="#" target="_blank">
                                <img class="SocialShare__Item__Link-Icon" src="../assets/img/share-icon-v1.svg">
                            </a>
                        </li>
                    </ul>
                </div>

                <div class="Document mt-5">
                    <img src="../assets/img/documents/FTI Consulting Anatomy of a Crisis 4 - Is business ready for a second spike.png" class="img-fluid"/>
                </div>

                <a href="../assets/img/documents/FTI Consulting Anatomy of a Crisis 4 - Is business ready for a second spike.pdf" target="_blank" class="Button Button--type-1 mt-3">
                    DOWNLOAD >
                </a>
            </div>
        </div>
        </div>
    </div>

    <?php include 'exploreothertopics.php'; ?>
</main>
