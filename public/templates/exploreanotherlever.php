<div class="Section--dark-blue py-5">
    <div class="container-large-md container">
        <div class="row mb-4">
            <div class="col-sm-12 text-center mb-20">
                <div class="Title--type-3">
                    EXPLORE ANOTHER LEVER
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col col-lg-3 text-center">
                <a href="/business-model-workforce-transformation"
                   class="Title--type-4 Title--type-4--business-model"
                   id="explore-another-level"
                   data-path-name="business-model-workforce-transformation"
                >
                    BUSINESS MODEL &<br/>WORKFORCE TRANSFORMATION
                </a>
            </div>


            <div class="col col-lg-3 text-center mb-4">
                <a href="/digital-trust-and-ecosystems"
                   data-path-name="digital-trust-and-ecosystems"
                   class="Title--type-4 Title--type-4--digital" id="explore-another-level">
                    DIGITAL TRUST<br/>& ECOSYSTEMS
                </a>
            </div>
            <div class="col col-lg-3 text-center mb-4">
                <a href="/economic-impact-sustainability"
                   class="Title--type-4 Title--type-4--economic-impact"
                   id="explore-another-level"
                   data-path-name="economic-impact-sustainability"
                >
                    ECONOMIC IMPACT<br/>& SUSTAINABILITY
                </a>
            </div>

            <div class="col col-lg-3 text-center">
                <a href="/escalation-planning-and-response"
                   class="Title--type-4 Title--type-4--escalation"
                   id="explore-another-level"
                   data-path-name="escalation-planning-and-response"
                >
                    ESCALATION PLANNING<br/>
                    & RESPONSE
                </a>
            </div>
        </div>
        <div class="row mt-4">
            <div class="col col-lg-3 text-center mb-4">
                <a href="/government-and-stakeholder-relations"
                   data-path-name="government-and-stakeholder-relations"
                   class="Title--type-4 Title--type-4--government" id="explore-another-level">
                    GOVERNMENT &<br/>STAKEHOLDER RELATIONS

                </a>
            </div>
            <div class="col col-lg-3 text-center">
                <a href="/operational-financial-resilience"
                   class="Title--type-4 Title--type-4--operational" id="explore-another-level"
                   data-path-name="operational-financial-resilience"
                >
                    OPERATIONAL &<br/>FINANCIAL RESILIENCE
                </a>
            </div>
            <div class="col col-lg-3 text-center">
                <a href="/real-time-data-analytics"
                   data-path-name="real-time-data-analytics"
                   class="Title--type-4 Title--type-4--real-time"
                   id="explore-another-level">
                    REAL-TIME DATA<br/>
                    ANALYTICS
                </a>
            </div>
            <div class="col col-lg-3 text-center mb-4">
                <a href="/remediation-and-dispute-resolution"
                   data-path-name="remediation-and-dispute-resolution"
                   class="Title--type-4 Title--type-4--remediation" id="explore-another-level">
                    REMEDIATION &<br/>DISPUTE RESOLUTION
                </a>
            </div>

        </div>
    </div>
</div>
