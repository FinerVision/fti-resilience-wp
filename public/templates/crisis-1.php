<main class="Home">
    <?php include 'perspectiveheader.php'; ?>

    <div class="Background--Gradient-Blue-Black">
        <div class="LeverCarousel__bg LeverCarousel__bg--type-8"
             style="background-image: url(./assets/img/header-bg/perspectives-header-bg-detail-v1.svg)">
            <div class="container-large-md container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="LeverCarousel__container">
                            <div class="LeverCarousel__navigation-container"></div>
                            <div class="LeverCarousel__title"><img
                                    class="LeverCarousel__decoration Animate Animate--slide-to-left"
                                    src="../assets/img/home-header-detail.svg"> Perspectives
                            </div>
                            <div class="LeverCarousel__quote LeverCarousel__quote--type-1"> Praesent commodo cursus
                                magna,<br/> vel scelerisque nisl consectetur et.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="Section--background-perspectives">
        <div class="container-large-md container">
            <div class="row">
                <div class="col-sm-12 col-md-8 Section--bg-white py-5">
                    <div class="Title--type-2 color-dark-blue mb-4">XX JULY 2020</div>
                    <hr class="HR HR--type-1">
                    <h1 class="Title--type-7 mt-3">
                        Anatomy of a Crisis I – Communicating through a crisis
                    </h1>

                    <b>
                        In this age of round-the-clock company scrutiny, we see almost as much focus given to how a
                        company handles a crisis as the crisis itself.

                    </b>
                    <p>
                        With turbulence in our world growing and the always-on nature of the news, the potential for
                        crisis has become an almost daily consideration for business. globalisation, investor activism,
                        regulatory change, political and cyber risk are all contributing to increasing business
                        vulnerability and the need for Boards to carefully consider their ability to respond
                        effectively.
                    </p>
                    <p>
                        With this in mind, FTI Consulting has undertaken a piece of proprietary research of around 100
                        recent incidents that have made the headlines. Our objective was to shine a light on those
                        crises and assess how they played out with a view to helping businesses successfully navigate
                        future disruptive events of their own.
                    </p>
                    <p>
                        The crises we reviewed span the last 20 years, and include oil spills, cyber hacks, plane
                        crashes, cases of fraud, product recalls and many more. We were interested to see what patterns
                        emerge from these events – patterns which might be instructive for Boards and communicators when
                        facing their own crisis scenario.

                    </p>
                </div>
                <div class="col-sm-12 col-md-4 Section--sidebar">
                    <div class="Title--type-2 color-dark-blue mb-4">&nbsp;</div>
                    <div class="SocialBox">
                        SHARE THIS CONTENT
                        <ul class="SocialShare SocialShare--align-left mt-3">
                            <li class="SocialShare__Item">
                                <a class="SocialShare__Item__Link" href="#" target="_blank">
                                    <img class="SocialShare__Item__Link-Icon" src="../assets/img/facebook-icon-v2.svg">
                                </a>
                            </li>
                            <li class="SocialShare__Item">
                                <a class="SocialShare__Item__Link" href="#" target="_blank">
                                    <img class="SocialShare__Item__Link-Icon" src="../assets/img/twitter-icon-v2.svg">
                                </a>
                            </li>
                            <li class="SocialShare__Item">
                                <a class="SocialShare__Item__Link" href="#" target="_blank">
                                    <img class="SocialShare__Item__Link-Icon" src="../assets/img/linkedin-icon-v2.svg">
                                </a>
                            </li>
                            <li class="SocialShare__Item">
                                <a class="SocialShare__Item__Link" href="#" target="_blank">
                                    <img class="SocialShare__Item__Link-Icon" src="../assets/img/share-icon-v1.svg">
                                </a>
                            </li>
                        </ul>
                    </div>

                    <div class="Document mt-5">
                        <img
                            src="../assets/img/documents/FTI Consulting Anatomy of a Crisis 1 - Communicating-through-a-crisis.png"
                            class="img-fluid"/>
                    </div>

                    <a href="../assets/img/documents/FTI Consulting Anatomy of a Crisis 1 - Communicating-through-a-crisis.pdf"
                       target="_blank" class="Button Button--type-1 mt-3">
                        DOWNLOAD >
                    </a>
                </div>
            </div>
        </div>
    </div>
    <?php include 'exploreothertopics.php'; ?>
</main>
