<main class="Report">
<!--    <img class="Report--Detail--Horizontal" src="/content/img/shard-detail-horizontal-v1.svg"/>-->
    <div class="Section--report-bg">
        <div class="">
            <img class="Report--Detail--type-2" src="../assets/img/the-report-shards-detail-v1.svg"/>
        </div>

        <div class="container-large-md container">
                <div class="Report__Header">
                    <div class="row">
                        <div class="col-sm-8">
                            <h1 class="Report__Header-Title Animate Animate--fade-up">
                                Resilience Barometer<br><span class="font-weight-light">2020</span>
                            </h1>
                        </div>
                        <div class="col-sm-4">
                            <div class="Report__Header__Detail mt-3">
                                <img class="Report__Header__Detail-Image Animate Animate--slide-to-left"
                                     src="<?php echo $header['detailImage'] ?>"/>
                            </div>
                        </div>
                    </div>
                </div>

            <div class="row">
                <div class="col-sm-8">
                    <div class="Report__Header__Description">
                        <p class="Report__Header__Description-Text Animate Animate--fade-up"><?php echo nl2br($leftColumn['description']); ?></p>
                    </div>
                    <p class="Paragraph--type-3 Animate Animate--fade-up mt-4 mb-4 text-white"><?php echo nl2br($leftColumn['body']); ?></p>

                    <div class="Accordion">
                        <h3 class="Accordion__heading text-white text-uppercase font-weight-light">Executive
                            Summary</h3>
                        <div class="Accordion__item open">
                            <div class="Accordion__item__heading">
                                <h4 class="text-white text-uppercase">
                                    <strong>RESILIENCE SCORE:</strong>
                                    A MEASURE OF PREPAREDNESS
                                </h4>
                                <button class="Button Accordion__item__toggle active">
                                    <img src="/assets/img/accordion_toggle_caret.png">
                                </button>
                            </div>
                            <div class="Accordion__item__content Accordion--Content--Transparent--Blue">
                                <p class="Paragraph--type-3 mt-4 mb-4 text-white">
                                    <?php echo nl2br($leftColumn['accordion_items']['resilience_score']['copy'][0]); ?>
                                </p>
                                <div class="Paragraph--type-3 Paragraph--columns-2 mt-4 text-white">
                                    <?php echo $leftColumn['accordion_items']['resilience_score']['copy'][1]; ?>
                                </div>
                                <button class="Button Download text-white text-uppercase">
                                    Download Chapter&nbsp;&nbsp;&nbsp;&gt;
                                </button>
                            </div>
                        </div>
                        <div class="Accordion__item">
                            <div class="Accordion__item__heading">
                                <h4 class="text-white text-uppercase">
                                    <strong>CORPORATE RISKS:</strong>
                                    PREPARING FOR EMERGING THREATS
                                </h4>
                                <button class="Button Accordion__item__toggle active">
                                    <img src="/assets/img/accordion_toggle_caret.png">
                                </button>
                            </div>
                            <div class="Accordion__item__content">
                                <p class="Paragraph--type-3 mt-4 mb-4 text-white">
                                </p>
                                <div class="row mb-2 align-content-start">
                                    <div class="col-sm-6">
                                        <div class="Number">
                                            <?php echo $leftColumn['accordion_items']['corporate_risks']['stats']['number']; ?>
                                            <div class="Number__bar">
                                                <div class="Number__bar__foreground" style="width: <?php echo $leftColumn['accordion_items']['corporate_risks']['stats']['number']; ?>"></div>
                                            </div>
                                        </div>
                                        <p class="Paragraph--type-3 mt-4 mb-4 text-white">
                                            <?php echo nl2br($leftColumn['accordion_items']['corporate_risks']['stats']['copy']); ?>
                                        </p>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="box--background-blue p-4">
                                            <p class="Paragraph--type-3 text-white text-uppercase">
                                                <?php echo nl2br($leftColumn['accordion_items']['corporate_risks']['box']); ?>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <button class="Button Download text-white text-uppercase mt-4">
                                    Download Chapter&nbsp;&nbsp;&nbsp;&gt;
                                </button>
                            </div>
                        </div>
                        <div class="Accordion__item">
                            <div class="Accordion__item__heading">
                                <h4 class="text-white text-uppercase">
                                    <strong>CRISIS:</strong>
                                    THE HIDDEN COST
                                </h4>
                                <button class="Button Accordion__item__toggle active">
                                    <img src="/assets/img/accordion_toggle_caret.png">
                                </button>
                            </div>
                            <div class="Accordion__item__content">
                                <p class="Paragraph--type-3 mt-4 mb-4 text-white">
                                    <?php echo nl2br($leftColumn['accordion_items']['hidden_cost']['copy']); ?>
                                </p>
                                <div class="row">
                                    <?php foreach ($leftColumn['accordion_items']['hidden_cost']['stats'] as $stat): ?>
                                        <div class="col-sm-4">
                                            <div class="Number">
                                                <?php echo $stat['number']; ?>
                                                <div class="Number__bar"></div>
                                            </div>
                                            <p class="Paragraph--type-3 mt-4 mb-4 text-white">
                                                <?php echo nl2br($stat['copy']); ?>
                                            </p>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                                <button class="Button Download text-white text-uppercase mt-4">
                                    Download Chapter&nbsp;&nbsp;&nbsp;&gt;
                                </button>
                            </div>
                        </div>
                        <div class="Accordion__item">
                            <div class="Accordion__item__heading">
                                <h4 class="text-white text-uppercase">
                                    <strong>SUSTAINABILITY:</strong>
                                    ENGAGING FOR GROWTH
                                </h4>
                                <button class="Button Accordion__item__toggle active">
                                    <img src="/assets/img/accordion_toggle_caret.png">
                                </button>
                            </div>
                            <div class="Accordion__item__content">
                                <p class="Paragraph--type-3 mt-4 mb-4 text-white">
                                    <?php echo nl2br($leftColumn['accordion_items']['sustainability']['copy'][0]); ?>
                                </p>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <img src="/assets/img/sustainability-for-growth-detail-v1.svg"
                                             class="img-fluid">
                                        <p class="Paragraph--type-3 mt-4 mb-4 text-white">
                                            <?php echo nl2br($leftColumn['accordion_items']['sustainability']['copy'][1]); ?>
                                        </p>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="bg-dark text-center text-white p-4">
                                            <h4 class="text-uppercase">the top five<br/>reported issues are:</h4>
                                            <ol class="Report__NumberedList">
                                                <?php foreach ($leftColumn['accordion_items']['sustainability']['top_five_issues'] as $issue): ?>
                                                    <li class="border-white">
                                                        <?php echo $issue['copy']; ?> | <?php echo $issue['number']; ?>
                                                    </li>
                                                <?php endforeach; ?>
                                            </ol>
                                        </div>
                                    </div>
                                </div>
                                <button class="Button Download text-white text-uppercase mt-4">
                                    Download Chapter&nbsp;&nbsp;&nbsp;&gt;
                                </button>
                            </div>
                        </div>
                        <div class="Accordion__item">
                            <div class="Accordion__item__heading">
                                <h4 class="text-white text-uppercase">
                                    <strong>CYBERSECURITY:</strong>
                                    RESILIENCE REQURES PROACTIVITY
                                </h4>
                                <button class="Button Accordion__item__toggle active">
                                    <img src="/assets/img/accordion_toggle_caret.png">
                                </button>
                            </div>
                            <div class="Accordion__item__content">
                                <p class="Paragraph--type-3 mt-4 mb-4 text-white">
                                    <?php echo nl2br($leftColumn['accordion_items']['cybersecurity']['copy']); ?>
                                </p>
                                <div class="row">
                                    <?php foreach ($leftColumn['accordion_items']['cybersecurity']['stats'] as $i => $stat): ?>
                                        <div class="col-sm-6">
                                            <?php if ($i === 0): ?>
                                                <div class="row">
                                                    <div class="col-5">
                                                        <img src="/assets/img/cybersecurity_1.png" class="img-fluid">
                                                    </div>
                                                    <div class="col-7">
                                                        <p class="Paragraph Paragraph--type-3 text-white">
                                                            <?php echo $stat['copy']; ?>
                                                        </p>
                                                    </div>
                                                </div>
                                            <?php elseif ($i === 1): ?>
                                                <div class="row">
                                                    <div class="col-5">
                                                        <img src="/assets/img/cybersecurity-detail-v1.svg"
                                                             class="img-fluid">
                                                    </div>
                                                    <div class="col-7">
                                                        <p class="Paragraph Paragraph--type-3 text-white">
                                                            <?php echo $stat['copy']; ?>
                                                        </p>
                                                    </div>
                                                </div>
                                            <?php elseif ($i >= 2): ?>
                                                <div class="my-2 border-bottom border-dark"></div>
                                                <div class="Number">
                                                    <?php echo $stat['number']; ?>
                                                    <div class="Number__bar"></div>
                                                </div>
                                                <p class="Paragraph--type-3 mt-4 mb-4 text-white">
                                                    <?php echo nl2br($stat['copy']); ?>
                                                </p>
                                            <?php endif; ?>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                                <button class="Button Download text-white text-uppercase mt-4">
                                    Download Chapter&nbsp;&nbsp;&nbsp;&gt;
                                </button>
                            </div>
                        </div>
                        <div class="Accordion__item">
                            <div class="Accordion__item__heading">
                                <h4 class="text-white text-uppercase">
                                    <strong>TECHNOLOGY:</strong>
                                    TRANSFORMING BUSINESS MODELS
                                </h4>
                                <button class="Button Accordion__item__toggle active">
                                    <img src="/assets/img/accordion_toggle_caret.png">
                                </button>
                            </div>
                            <div class="Accordion__item__content">
                                <p class="Paragraph--type-3 mt-4 mb-4 text-white">
                                    <?php echo nl2br($leftColumn['accordion_items']['technology']['copy']); ?>
                                </p>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="Number">
                                            <div class="Number__bar">
                                                <span class="d-block px-4 text-white text-size-6rem">
                                                    <?php echo $leftColumn['accordion_items']['technology']['stats']['number']; ?>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <p class="Paragraph Paragraph--type-3 text-white text-uppercase">
                                            <?php echo $leftColumn['accordion_items']['technology']['stats']['copy']; ?>
                                        </p>
                                    </div>
                                </div>
                                <button class="Button Download text-white text-uppercase mt-4">
                                    Download Chapter&nbsp;&nbsp;&nbsp;&gt;
                                </button>
                            </div>
                        </div>
                        <div class="Accordion__item">
                            <div class="Accordion__item__heading">
                                <h4 class="text-white text-uppercase">
                                    <strong>RESEARCH METHODOLOGY</strong>
                                </h4>
                                <button class="Button Accordion__item__toggle active">
                                    <img src="/assets/img/accordion_toggle_caret.png">
                                </button>
                            </div>
                            <div class="Accordion__item__content">
                                <p class="Paragraph--type-3 mt-4 mb-4 text-white">
                                    <?php echo nl2br($leftColumn['accordion_items']['research_methodology']['copy'][0]); ?>
                                </p>
                                <div class="Paragraph--type-3 Paragraph--columns-2 mt-4 text-white">
                                    <?php echo $leftColumn['accordion_items']['research_methodology']['copy'][1]; ?>
                                </div>
                                <button class="Button Download text-white text-uppercase mt-5">
                                    Download Chapter&nbsp;&nbsp;&nbsp;&gt;
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 p-4 ">
                    <div class="Report__Sidebar">
                        <h4 class="text-white text-uppercase">Full 2020 Report</h4>
                        <img src="/assets/img/report_2020.jpg" class="mb-3 img-fluid"/>
                        <button class="Button Download text-white text-uppercase">
                            Download&nbsp;&nbsp;&nbsp;&gt;
                        </button>
                        <h4 class="text-white text-uppercase">Full 2019 Report</h4>
                        <img src="/assets/img/report_2019.png" class="mb-3 img-fluid" width="372"/>
                        <button class="Button Download text-white text-uppercase">
                            Download&nbsp;&nbsp;&nbsp;&gt;
                        </button>
                    </div>

                </div>
            </div>
        </div>
    </div>
</main>
