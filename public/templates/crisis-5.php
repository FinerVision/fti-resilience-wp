<main class="Home">
    <?php include 'perspectiveheader.php'; ?>

    <div class="Section--background-perspectives">
        <div class="container-large-md container">
            <div class="row">
                <div class="col-sm-12 col-md-8 Section--bg-white py-5">
                    <div class="Title--type-2 color-dark-blue mb-4">XX JULY 2020</div>
                    <hr class="HR HR--type-1">
                    <h1 class="Title--type-7 mt-3">
                        COVID-19 and Commercial Real Estate Considerations

                    </h1>
                    <b>
                        Commercial real estate (CRE) valuations are currently in a state of uncertainty for various
                        property sectors, as the depth and duration of an economic downturn is still unknown.
                    </b>
                    <p>
                        Landlords, tenants and lenders are hungry for data that reflect COVID-19’s immediate effects on
                        levels of rent deferrals and abatements and the longer-term effects on rents, expenses and
                        occupancies, but the script is still being written. All parties are in the middle of
                        multi-faceted negotiations between stakeholders with diverging interests.
                    </p>
                    <p>
                        Many landlords are initially taking “hardball” positions to preserve asset values but ultimately
                        recognize that they need their tenants to successfully navigate the crisis. Yet, landlords risk
                        tripping debt covenants by granting too much forbearance to help ailing tenants, which brings
                        lenders and special servicers into the mix.
                    </p>
                    <p>
                        The following is a collection of observations based on FTI Consulting’s review of current
                        economic indicators and interactions with landlords, tenants, and their creditors.

                    </p>
                    <p>
                        Other options may encompass a sale or licensing of assets for a needed cash infusion. Regardless
                        of which path is taken, a rigorous valuation of a company’s intellectual property will likely be
                        required.

                    </p>
                    <p>
                        For many U.S. businesses, one of the most valuable assets on the balance sheet is intellectual
                        property. In fact, in many cases, various forms of intellectual property and intellectual
                        capital provide the competitive advantage necessary for success.

                    </p>
                    <p>
                        As such, assigning a reasonable value to these differentiating assets is paramount in a
                        restructuring, sale, or licensing situation, in order to effectively capture the intellectual
                        property’s monetary worth. Understanding the potential impact of the COVID-19 pandemic may prove
                        beneficial, if not crucial, in determining an intellectual property asset’s value.

                    </p>
                </div>
                <div class="col-sm-12 col-md-4 Section--sidebar">
                    <div class="Title--type-2 color-dark-blue mb-4">&nbsp;</div>
                    <div class="SocialBox">
                        SHARE THIS CONTENT
                        <ul class="SocialShare SocialShare--align-left mt-3">
                            <li class="SocialShare__Item">
                                <a class="SocialShare__Item__Link" href="#" target="_blank">
                                    <img class="SocialShare__Item__Link-Icon" src="../assets/img/facebook-icon-v2.svg">
                                </a>
                            </li>
                            <li class="SocialShare__Item">
                                <a class="SocialShare__Item__Link" href="#" target="_blank">
                                    <img class="SocialShare__Item__Link-Icon" src="../assets/img/twitter-icon-v2.svg">
                                </a>
                            </li>
                            <li class="SocialShare__Item">
                                <a class="SocialShare__Item__Link" href="#" target="_blank">
                                    <img class="SocialShare__Item__Link-Icon" src="../assets/img/linkedin-icon-v2.svg">
                                </a>
                            </li>
                            <li class="SocialShare__Item">
                                <a class="SocialShare__Item__Link" href="#" target="_blank">
                                    <img class="SocialShare__Item__Link-Icon" src="../assets/img/share-icon-v1.svg">
                                </a>
                            </li>
                        </ul>
                    </div>

                    <div class="Document mt-5">
                        <img src="../assets/img/documents/covid-19.png" class="img-fluid"/>
                    </div>

                    <a href="../assets/img/documents/covid-19.pdf" target="_blank" class="Button Button--type-1 mt-3">
                        DOWNLOAD >
                    </a>
                </div>
            </div>
        </div>
    </div>

    <?php include 'exploreothertopics.php'; ?>
</main>
