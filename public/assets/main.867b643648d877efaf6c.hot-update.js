webpackHotUpdate("main",{

/***/ "./src/assets/scripts/Animation.js":
/*!*****************************************!*\
  !*** ./src/assets/scripts/Animation.js ***!
  \*****************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_0__);

/**
 * Check if elements with class 'Animate' are in the viewport. If so, add a class 'Animate--show' to them.
 * Animations are defined in Animations.scss
 */

var $animation_elements = jquery__WEBPACK_IMPORTED_MODULE_0___default()('.Animate');
var $window = jquery__WEBPACK_IMPORTED_MODULE_0___default()(window);

function check_if_in_view() {
  var window_height = $window.height();
  var window_top_position = $window.scrollTop();
  var window_bottom_position = window_top_position + window_height;
  jquery__WEBPACK_IMPORTED_MODULE_0___default.a.each($animation_elements, function (e) {
    var $element = jquery__WEBPACK_IMPORTED_MODULE_0___default()(this);
    var element_height = $element.outerHeight();
    var element_top_position = $element.offset().top;
    var element_bottom_position = element_top_position + element_height; //check to see if this current container is within viewport

    if (element_bottom_position >= window_top_position && element_top_position <= window_bottom_position) {
      $element.addClass('Animate--show'); // Count numbers

      if ($element.hasClass('Animate--count-numbers') && typeof $element.data('from') === 'number' && typeof $element.data('to') === 'number') {
        $element.removeClass('Animate--count-numbers');
        $element.css('opacity', 1);
        var suffix = $element.data('suffix');
        var precision = $element.data('precision');
        count_numbers($element, $element.data('from'), $element.data('to'), suffix, precision);
      }
    }
  });
}
/**
 * Number increment animation
 * @type {number}
 */


var NUMBER_INTERVAL = 1; // Time in ms between each number increment

function count_numbers($element, from, to) {
  var suffix = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : '';
  var precision = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : 1;
  var timer = setInterval(function () {
    var currentCount = Number.parseFloat($element.html());

    if (currentCount < from) {
      $element.html(from);
    } else if (currentCount >= to) {
      clearInterval(timer);
    } else {
      currentCount = currentCount + precision;
      var number;

      if (precision !== 1) {
        number = currentCount.toFixed(0);
      } else {
        number = currentCount;
      }

      $element.html(number);
    }
  }, NUMBER_INTERVAL);
}

jquery__WEBPACK_IMPORTED_MODULE_0___default()(document).ready(function () {
  $window.on('scroll resize', check_if_in_view);
  $window.trigger('scroll');
});

/***/ }),

/***/ "./src/assets/scripts/animateNumber.js":
/*!*********************************************!*\
  !*** ./src/assets/scripts/animateNumber.js ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Counter; });
function Counter(data) {
  var _default = {
    fps: 60,
    from: 0,
    time: 1000
  };

  for (var attr in _default) {
    if (typeof data[attr] === 'undefined') {
      data[attr] = _default[attr];
    }
  }

  if (typeof data.to === 'undefined') return;
  data.fps = typeof data.fps === 'undefined' ? 20 : parseInt(data.fps);
  data.from = typeof data.from === 'undefined' ? 0 : parseFloat(data.from);
  var frames = data.time / data.fps,
      inc = (data.to - data.from) / frames,
      val = data.from;

  if (typeof data.start === 'function') {
    data.start(data.from, data);
  }

  var interval = setInterval(function () {
    frames--;
    val += inc;

    if (val >= data.to) {
      if (typeof data.complete === 'function') {
        data.complete(data.to, data);
      }

      clearInterval(interval);
    } else if (typeof data.progress === 'function') {
      data.progress(val, data);
    }
  }, data.fps);
}

/***/ }),

/***/ "./src/assets/scripts/home.js":
/*!************************************!*\
  !*** ./src/assets/scripts/home.js ***!
  \************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var swiper__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! swiper */ "./node_modules/swiper/swiper.esm.js");
/* harmony import */ var _accordion__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./accordion */ "./src/assets/scripts/accordion.js");
/* harmony import */ var _animateNumber__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./animateNumber */ "./src/assets/scripts/animateNumber.js");
/* harmony import */ var _scrollAnimation__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./scrollAnimation */ "./src/assets/scripts/scrollAnimation.js");




swiper__WEBPACK_IMPORTED_MODULE_0__["default"].use([swiper__WEBPACK_IMPORTED_MODULE_0__["Navigation"]]);
new swiper__WEBPACK_IMPORTED_MODULE_0__["default"]('.swipper-container', {
  loop: true,
  direction: "horizontal",
  loopFillGroupWithBlank: true,
  navigation: {
    nextEl: '.button-next',
    prevEl: '.button-previous'
  },
  preventClicks: false,
  preventClicksPropagation: false,
  touchRatio: 0,
  spaceBetween: 20,
  autoplay: true,
  slidesPerView: 3,
  centeredSlides: false,
  breakpoints: {
    // when window width is >= 320px
    320: {
      slidesPerView: 1,
      spaceBetween: 20
    },
    // when window width is >= 480px
    576: {
      slidesPerView: 1,
      spaceBetween: 20
    },
    // when window width is >= 640px
    992: {
      slidesPerView: 2,
      spaceBetween: 20
    },
    // when window width is >= 1200px
    1200: {
      slidesPerView: 3,
      spaceBetween: 40
    }
  }
});
new _accordion__WEBPACK_IMPORTED_MODULE_1__["default"]({
  itemSelector: "#collapsible-body",
  buttonSelector: '#collapsible-button'
});
new _scrollAnimation__WEBPACK_IMPORTED_MODULE_3__["default"]({
  selector: '.Counter'
});

/***/ }),

/***/ "./src/assets/scripts/scrollAnimation.js":
/*!***********************************************!*\
  !*** ./src/assets/scripts/scrollAnimation.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return scrollAnimation; });
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classPrivateFieldGet(receiver, privateMap) { var descriptor = privateMap.get(receiver); if (!descriptor) { throw new TypeError("attempted to get private field on non-instance"); } if (descriptor.get) { return descriptor.get.call(receiver); } return descriptor.value; }

function _classPrivateFieldSet(receiver, privateMap, value) { var descriptor = privateMap.get(receiver); if (!descriptor) { throw new TypeError("attempted to set private field on non-instance"); } if (descriptor.set) { descriptor.set.call(receiver, value); } else { if (!descriptor.writable) { throw new TypeError("attempted to set read only private field"); } descriptor.value = value; } return value; }

var _timer = new WeakMap();

var scrollAnimation = /*#__PURE__*/function () {
  function scrollAnimation(_ref) {
    var selector = _ref.selector,
        onElementReached = _ref.onElementReached;

    _classCallCheck(this, scrollAnimation);

    _timer.set(this, {
      writable: true,
      value: null
    });

    _defineProperty(this, "windowOffsetBottom", null);

    _defineProperty(this, "collections", []);

    this.init(selector);
  }

  _createClass(scrollAnimation, [{
    key: "init",
    value: function init(selector) {
      this.collections = document.querySelectorAll(selector);
      this.windowOffsetBottom = window.innerHeight + window.scrollY;
      console.log(selector);
      this.animate();
      window.addEventListener('scroll', this.animate.bind(this));
    }
  }, {
    key: "getOffsetTop",
    value: function getOffsetTop(element) {
      var offsetTop = 0;

      while (element) {
        offsetTop += element.offsetTop;
        element = element.offsetParent;
      }

      return offsetTop;
    }
  }, {
    key: "animate",
    value: function animate() {
      var _this = this;

      this.windowOffsetBottom = window.innerHeight + window.scrollY;
      this.collections.forEach(function (elem, index) {
        var elemOffsetBottom = elem.clientHeight + _this.getOffsetTop(elem);

        if (_this.getOffsetTop(elem) < _this.windowOffsetBottom) {
          if (!elem.classList.contains('animate') && elemOffsetBottom - window.scrollY > 0 || _this.windowOffsetBottom > elemOffsetBottom) {
            _classPrivateFieldSet(_this, _timer, setTimeout(function () {
              elem.classList.add('animate');
            }, index === 0 ? 0 : 100 * index));

            elem.addEventListener('animationend', function (e) {
              e.target.removeAttribute('id');
              clearTimeout(_classPrivateFieldGet(_this, _timer));
            });
          }
        }
      });
    }
  }]);

  return scrollAnimation;
}();



/***/ })

})
//# sourceMappingURL=main.867b643648d877efaf6c.hot-update.js.map