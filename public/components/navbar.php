<?php
$items = require __DIR__ . '/../config/nav.php';
?>
<div class="Nav">
    <nav class="navbar navbar-dark navbar-expand-lg Navbar">
        <a class="navbar-brand" href="/">
            <img src="/content/img/logo.svg" alt="FTI" class="Animate Animate--slide-to-right"/>
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon">&nbsp;</span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto hidden Animate--delay-elements" id="navbar-nav">
                <?php foreach ($items as $item): ?>
                    <li class="nav-item Animate Animate--fade-up">
                        <?php if (!isset($item['dropdown'])): ?>
                            <a href="<?php echo $item['link']; ?>"
                               class="active<?php echo ($pathname !== $item['link']) ? 'active' : '' ?>">
                                <?php echo $item['title']; ?>
                            </a>
                        <?php endif;?>
                        <?php if (isset($item['dropdown'])) : ?>
                            <div class="nav-item__dropdown">
                                <a href="#" class="nav-item__dropdown-<?php echo ($pathname !== $item['link']) ? 'active' : '' ?>">
                                    <?php echo $item['title']; ?>
                                </a>
                            </div>
                        <?php endif; ?>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
    </nav>
</div>
<div class="custom-dropdown-menu nav-item__dropdown">
    <?php include "drop-down.php"?>
</div>
