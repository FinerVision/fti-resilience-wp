<div class="lever-swipper-container">
    <!-- Additional required wrapper -->
    <div class="swiper-wrapper">
        <!-- Slides -->
        <div class="swiper-slide">
            <div class="LeverCarousel__bg LeverCarousel__bg--type-1"
                 style="background-image: url(./assets/img/header-bg/lever-introduction-header-detail-v1.svg)">
                <div class="container-large-md container">
                    <div class="LeverCarousel__container py-5 py-lg-0">
                        <div class="LeverCarousel__navigation-container">
                            <div class="LeverCarousel__button LeverCarousel__button-prev"></div>

                            <div class="LeverCarousel__navigation-title">
                                7 RESILIENCE LEVERS
                            </div>
                            <div class="LeverCarousel__button LeverCarousel__button-next"></div>
                        </div>

                        <div class="LeverCarousel__title">
                            Escalation Planning<br/>
                            & Response
                        </div>

                        <div class="LeverCarousel__quote LeverCarousel__quote--type-1">
                            Resilience is the intersection of preparedness and risk.<br/>
                            Explore our latest insights into the challenges facing business leaders.
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="swiper-slide">
            <div class="LeverCarousel__bg LeverCarousel__bg--type-2" style="background-image: url(./assets/img/header-bg/lever-introduction-header-detail-v2.svg)">
                <div class="container-large-md container">
                    <div class="LeverCarousel__container py-5 py-lg-0">
                        <div class="LeverCarousel__navigation-container">
                            <div class="LeverCarousel__button LeverCarousel__button-prev"></div>

                            <div class="LeverCarousel__navigation-title">
                                7 RESILIENCE LEVERS
                            </div>
                            <div class="LeverCarousel__button LeverCarousel__button-next"></div>
                        </div>


                        <div class="LeverCarousel__title">
                            Remediation &<br/>
                            Dispute Resolution
                        </div>

                        <div class="LeverCarousel__quote LeverCarousel__quote--type-2">
                            Resilience is the intersection of preparedness and risk.<br/>
                            Explore our latest insights into the challenges facing business leaders.
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="swiper-slide">
            <div class="LeverCarousel__bg LeverCarousel__bg--type-3"
                 style="background-image: url(./assets/img/header-bg/lever-introduction-header-detail-v3.svg)">
                <div class="container-large-md container">
                    <div class="LeverCarousel__container py-5 py-lg-0">
                        <div class="LeverCarousel__navigation-container">
                            <div class="LeverCarousel__button LeverCarousel__button-prev"></div>

                            <div class="LeverCarousel__navigation-title">
                                7 RESILIENCE LEVERS
                            </div>
                            <div class="LeverCarousel__button LeverCarousel__button-next"></div>
                        </div>
                        <div class="LeverCarousel__title">
                            Government &<br/>
                            Stakeholder Relations
                        </div>

                        <div class="LeverCarousel__quote LeverCarousel__quote--type-3">
                            Resilience is the intersection of preparedness and risk.<br/>
                            Explore our latest insights into the challenges facing business leaders.
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="swiper-slide">
            <div class="LeverCarousel__bg LeverCarousel__bg--type-4" style="background-image: url(./assets/img/header-bg/lever-introduction-header-detail-v4.svg)">
                <div class="container-large-md container">
                    <div class="LeverCarousel__container py-5 py-lg-0">
                        <div class="LeverCarousel__navigation-container">
                            <div class="LeverCarousel__button LeverCarousel__button-prev"></div>

                            <div class="LeverCarousel__navigation-title">
                                7 RESILIENCE LEVERS
                            </div>
                            <div class="LeverCarousel__button LeverCarousel__button-next"></div>
                        </div>


                        <div class="LeverCarousel__title">
                            Economic Impact<br/>
                            & Sustainability
                        </div>

                        <div class="LeverCarousel__quote LeverCarousel__quote--type-4">
                            Resilience is the intersection of preparedness and risk.<br/>
                            Explore our latest insights into the challenges facing business leaders.
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="swiper-slide">
            <div class="LeverCarousel__bg LeverCarousel__bg--type-5" style="background-image: url(./assets/img/header-bg/lever-introduction-header-detail-v5.svg)">
                <div class="container-large-md container">
                    <div class="LeverCarousel__container py-5 py-lg-0">
                        <div class="LeverCarousel__navigation-container">
                            <div class="LeverCarousel__button LeverCarousel__button-prev"></div>

                            <div class="LeverCarousel__navigation-title">
                                7 RESILIENCE LEVERS
                            </div>
                            <div class="LeverCarousel__button LeverCarousel__button-next"></div>
                        </div>


                        <div class="LeverCarousel__title">
                            Business Model &<br/>
                            Workforce Transformation
                        </div>

                        <div class="LeverCarousel__quote LeverCarousel__quote--type-5">
                            Resilience is the intersection of preparedness and risk.<br/>
                            Explore our latest insights into the challenges facing business leaders.
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="swiper-slide">
            <div class="LeverCarousel__bg LeverCarousel__bg--type-6" style="background-image: url(./assets/img/header-bg/lever-introduction-header-detail-v6.svg)">
                <div class="container-large-md container">
                    <div class="LeverCarousel__container py-5 py-lg-0">
                        <div class="LeverCarousel__navigation-container">
                            <div class="LeverCarousel__button LeverCarousel__button-prev"></div>

                            <div class="LeverCarousel__navigation-title">
                                7 RESILIENCE LEVERS
                            </div>
                            <div class="LeverCarousel__button LeverCarousel__button-next"></div>
                        </div>


                        <div class="LeverCarousel__title">
                            Operational &<br/>
                            Financial Resilience
                        </div>

                        <div class="LeverCarousel__quote LeverCarousel__quote--type-6">
                            Resilience is the intersection of preparedness and risk.<br/>
                            Explore our latest insights into the challenges facing business leaders.
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="swiper-slide">
            <div class="LeverCarousel__bg LeverCarousel__bg--type-7" style="background-image: url(./assets/img/header-bg/lever-introduction-header-detail-v7.svg)">
                <div class="container-large-md container">
                    <div class="LeverCarousel__container py-5 py-lg-0">
                        <div class="LeverCarousel__navigation-container">
                            <div class="LeverCarousel__button LeverCarousel__button-prev"></div>

                            <div class="LeverCarousel__navigation-title">
                                7 RESILIENCE LEVERS
                            </div>
                            <div class="LeverCarousel__button LeverCarousel__button-next"></div>
                        </div>


                        <div class="LeverCarousel__title">
                            Digital Trust
                            <br/>& Ecosystems
                        </div>

                        <div class="LeverCarousel__quote LeverCarousel__quote--type-7">
                            Resilience is the intersection of preparedness and risk.<br/>
                            Explore our latest insights into the challenges facing business leaders.
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="swiper-slide">
            <div class="LeverCarousel__bg LeverCarousel__bg--type-8" style="background-image: url(assets/img/lever-introduction-header-detail-v8.svg)">
                <div class="container-large-md container">
                    <div class="LeverCarousel__container py-5 py-lg-0">
                        <div class="LeverCarousel__navigation-container">
                            <div class="LeverCarousel__button LeverCarousel__button-prev"></div>

                            <div class="LeverCarousel__navigation-title">
                                7 RESILIENCE LEVERS
                            </div>
                            <div class="LeverCarousel__button LeverCarousel__button-next"></div>
                        </div>


                        <div class="LeverCarousel__title">
                            Real-Time Data<br/>
                            Analytics
                        </div>

                        <div class="LeverCarousel__quote LeverCarousel__quote--type-7">
                            Resilience is the intersection of preparedness and risk.<br/>
                            Explore our latest insights into the challenges facing business leaders.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
