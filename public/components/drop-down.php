<div class="Section--dark-black py-3 py-lg-5">
    <div class="container-large-md container flex">
        <div class="row">
            <div class="col-lg-3 text-center my-1 my-lg-0">
                <a href="/business-model-workforce-transformation"
                   class="Title--type-4 Title--type-4--business-model"
                   id="explore-another-level"
                   data-path-name="business-model-workforce-transformation"
                >BUSINESS MODEL &<span class="slim-break"></span>WORKFORCE TRANSFORMATION</a>
            </div>
            <div class="col-lg-3 text-center my-1 my-lg-0">
                <a href="/digital-trust-and-ecosystems"
                   data-path-name="digital-trust-and-ecosystems"
                   class="Title--type-4 Title--type-4--digital" id="explore-another-level">
                    DIGITAL TRUST<span class="slim-break"></span>& ECOSYSTEMS
                </a>
            </div>
            <div class="col-lg-3 text-center my-1 my-lg-0">
                <a href="/economic-impact-sustainability"
                   class="Title--type-4 Title--type-4--economic-impact"
                   id="explore-another-level"
                   data-path-name="economic-impact-sustainability"
                >
                    ECONOMIC IMPACT<span class="slim-break"></span>& SUSTAINABILITY
                </a>
            </div>

            <div class="col-lg-3 text-center my-1 my-lg-0">
                <a href="/escalation-planning-and-response"
                   class="Title--type-4 Title--type-4--escalation"
                   id="explore-another-level"
                   data-path-name="escalation-planning-and-response"
                >
                    ESCALATION PLANNING<span class="slim-break"></span>
                    & RESPONSE
                </a>
            </div>
        </div>
        <div class="row mt-lg-4 flex-grow-1 flex-lg-grow-0">
            <div class="col-lg-3 text-center my-1 my-lg-0">
                <a href="/government-and-stakeholder-relations"
                   data-path-name="government-and-stakeholder-relations"
                   class="Title--type-4 Title--type-4--government" id="explore-another-level">
                    GOVERNMENT &<span class="slim-break"></span>STAKEHOLDER RELATIONS

                </a>
            </div>
            <div class="col-lg-3 text-center my-1 my-lg-0">
                <a href="/operational-financial-resilience"
                   class="Title--type-4 Title--type-4--operational" id="explore-another-level"
                   data-path-name="operational-financial-resilience"
                >
                    OPERATIONAL &<span class="slim-break"></span>FINANCIAL RESILIENCE
                </a>
            </div>
            <div class="col-lg-3 text-center my-1 my-lg-0">
                <a href="/real-time-data-analytics"
                   data-path-name="real-time-data-analytics" class="Title--type-4 Title--type-4--real-time"
                   id="explore-another-level">
                    REAL-TIME DATA<span class="slim-break"></span>
                    ANALYTICS
                </a>
            </div>
            <div class="col-lg-3 text-center my-1 my-lg-0">
                <a href="/remediation-and-dispute-resolution"
                   data-path-name="remediation-and-dispute-resolution"
                   class="Title--type-4 Title--type-4--remediation" id="explore-another-level">
                    REMEDIATION &<span class="slim-break"></span>DISPUTE RESOLUTION
                </a>
            </div>

        </div>
    </div>
</div>
