<?php
$footer = require __DIR__ . '/../config/footer.php';
?>
<footer class="Footer">
    <div class="container-large-md container">
        <div class="row">
            <div class="col-sm-8">
                <div class="Footer__Content">
                    <h2 class="Footer__Content-Title text-uppercase">
                        <?php
                        echo $footer['title']
                        ?>
                    </h2>
                    <p class="Footer__Content-Description">
                        <?php
                        echo $footer['description']
                        ?>
                    </p>
                    <p class="Footer__Content-Copyright">
                        <?php
                        echo $footer['copyright']
                        ?>
                    </p>
                </div>
            </div>
            <div class="col-sm-4">
                <ul class="SocialShare mt-3 mt-md-5">
                    <?php foreach ($footer['social'] as $key => $social) : ?>
                        <li class="SocialShare__Item">
                            <a class="SocialShare__Item__Link" href="<?php echo $social['url']; ?>" target="_blank">
                                <img class="SocialShare__Item__Link-Icon"
                                     src="/content/img/<?php echo $social['type'] ?>-icon-v1.svg"/>
                            </a>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
    </div>
</footer>
<?php if ($sections['contact']) : ?>
    <div class="Get-In-Touch-Placeholder"></div>
    <section class="Home__Get-In-Touch">
        <div class="container-large container">
            <div class="Home__Get-In-Touch__Collapsible">
                <div class="Home__Get-In-Touch__Collapsible-Body" id="collapsible-body">
                    <div class="row mb-3 pt-5 justify-content-center">
                        <?php foreach ($sections['contact'] as $key => $contact) : ?>
                            <div class="col-sm-12 col-md-3">
                                <div class="Person">
                                    <div class="Person__image">
                                        <img src="<?php echo $contact['imageSrc'] ?>"/>
                                    </div>
                                    <div class="Person__name"><?php echo $contact['name'] ?></div>
                                    <div class="Person__position"><?php echo $contact['role'] ?></div>
                                    <ul class="SocialShare mt-3">
                                        <li class="SocialShare__Item">
                                            <a class="SocialShare__Item__Link" href="<?php echo $contact['email'] ?>"
                                               target="_blank">
                                                <img class="SocialShare__Item__Link-Icon"
                                                     src="/content/img/mail-icon-v1.svg">
                                            </a>
                                        </li>

                                        <li class="SocialShare__Item">
                                            <a class="SocialShare__Item__Link" href="<?php echo $contact['linkedin'] ?>"
                                               target="_blank">
                                                <img class="SocialShare__Item__Link-Icon"
                                                     src="/content/img/linkedin-icon-v1.svg">
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <button class="Home__Get-In-Touch__Collapsible-Button">
                            <div id="collapsible-button" class="d-flex" data-accordion-key="0">
                                <h4 class="mb-0">GET IN TOUCH</h4>
                                <img src="/content/img/arrow.png" class="Home__Get-In-Touch__Collapsible-Button__icon"
                                     style="align-self: center;"/>
                            </div>
                        </button>
                    </div>
                    <div class="col-sm-6">
                        <div
                            class="Home__Resilience-Barometer__Content__Title Animate--delay-elements text-white h-100 justify-content-center justify-content-md-end">
                            <img class="Home__Resilience-Barometer__Content__Title-Icon Animate Animate--fade-up mb-0"
                                 src="/content/img/home-header-detail.svg">
                            <h4 class="Animate Animate--fade-up mb-0">EXPERTS WITH IMPACT</h4>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
<?php endif; ?>
