<?php

return [
    'title' => "About FTI Consulting",
    'description' => "FTI Consulting is an independent global business advisory firm dedicated to helping organisations manage change, mitigate risk and resolve disputes: financial, legal, operational, political & regulatory, reputational and transactional. FTI Consulting professionals, located in all major business centres throughout the world, work closely with clients to anticipate, illuminate and overcome complex business challenges and opportunities. The views expressed in any of the articles or other content hosted on this site are those of the author(s) and not necessarily the views of FTI Consulting, its management, its subsidiaries, its affiliates, or its other professionals.",
    'copyright' => '© 2020 FTI Consulting, Inc. All Rights Reserved',
    'social' => [
        // type is required, is used to get the filename e.g /content/img/[type].svg
        [
            'type' => 'facebook',
            'url' => '#'
        ],
        [
            'type' => 'twitter',
            'url' => '#'
        ],
        [
            'type' => 'linkedin',
            'url' => '#'
        ]
    ]
];
