<?php


return [
    '/' => [
        'layout' => __DIR__ . '/../layouts/default.php',
        'template' => __DIR__ . '/../templates/home.php',
        'data' => [
            'title' => 'FTI Resilience',
            'header' => [
                'noBackground' => true,
                'title' => 'Rebuild Resilience. Protect Value.',
                'detailImage' => 'content/img/home-header-detail.svg',
                'description' => "In preparing for a post-Covid world, organisations must balance short-term imperatives against longer-term focus to rebuild resilience and protect value. FTI Consulting’s Resilience Agenda provides fact-based and timely insights to enable decisions in a volatile and changing environment.",
            ],
            'sections' => [
                'infoGraphic' => [
                    'title' => 'The Resilience Agenda',
                    'linkText' => 'click to explore',
                    'linkUrl' => '#',
                    'iconSrc' => "/content/img/sub-title-line-detail-white-v1.svg"
                ],
                'resilienceBarometer' => [
                    'title' => 'The Resilience Barometer<sup>TM</sup>',
                    'description' => '<p>The Resilience Barometer™ 2020 data reveals how, for too many organisations, the pandemic has exposed a lack of resilience across critical areas: from business models to supply chains, and from crisis management to regulatory changes in an increasingly fragmented world. Whatever the business, industry or geography, a collective understanding of resilience is now critical.</p><p>To navigate the continuing disruption and uncertainty, and the inevitable economic fallout, businesses know they need to increase their resilience: that is, to ensure they are prepared for whatever comes next.</p><p>Drawing on insights from global C-suite professionals, FTI Consulting’s Resilience Agenda captures the experiences of business leaders facing major challenges – whether commercial, technological, reputational or legal. These learnings are encapsulated in seven resilience levers: key areas of focus that decision-makers must navigate in a fast-paced, disrupted environment.</p>',
                    'countsTitle' => "IN NUMBERS",
                    'largeCompaniesCountDescription' => '<p>We gathered perspectives<br/>of C-suite and senior<br/>managers from</p>',
                    'largeCompaniesCountTitle' => 'large companies',
                    'largeCompaniesCount' => 2276,
                    'peopleCountTitle' => 'people',
                    'peopleCountDescription' => "<p>The companies researched<br/>directly employ a total of</p>",
                    'peopleCount' => 58,
                    'globalTurnoverCountTitle' => "global turnover",
                    'globalTurnoverCountDescription' => '<p>Participating companies<br/>represent a sum aggregated</p>',
                    'globalTurnoverCount' => 44,
                ],
                'perspectives' => [
                    'title' => 'PERSPECTIVES',
                    'slides' => [
                        [
                            'description' => '<p>Investor Communications<br/>in Times of Uncertainty</p>',
                            'imageUrl' => '/content/img/Crop-2-01.png',
                            'linkText' => 'Read more>',
                            'linkUrl' => '#'
                        ],
                        [
                            'description' => '<p>COVID-19: Effective<br/>Internal Comms in<br/>Unprecedented Times</p>',
                            'imageUrl' => '/content/img/Crop-5-01.png',
                            'linkText' => 'Read more>',
                            'linkUrl' => '#'
                        ],
                        [
                            'description' => '<p>Coronavirus:<br/>Dealing with supply<br/>chain disruption</p>',
                            'imageUrl' => '/content/img/Crop-4-01.png',
                            'linkText' => 'Read more>',
                            'linkUrl' => '#'
                        ],
                        [
                            'description' => '<p>Investor Communications<br/>in Times of Uncertainty</p>',
                            'imageUrl' => '/content/img/Crop-2-01.png',
                            'linkText' => 'Read more>',
                            'linkUrl' => '#'
                        ]
                    ]
                ],
                'joinConversation' => [
                    'title' => 'Get the latest from FTI Consulting and join the conversation',
                    'linkText' => "#ResilienceBarometer",
                    'linkUrl' => "#"
                ],
                'getInTouch' => [
                    'buttonText' => 'GET IN TOUCH',
                    'cards' => []
                ]
            ],
        ],
    ],
    '/escalation-planning-and-response' => [
        'layout' => __DIR__ . '/../layouts/default.php',
        'template' => __DIR__ . '/../templates/lever.php',
        'data' => [
            'sections' => [
                'perspectives' => [
                    'title' => 'PERSPECTIVES',
                    'slides' => [
                        [
                            'description' => '<p>Investor Communications<br/>in Times of Uncertainty</p>',
                            'imageUrl' => '/content/img/Crop-2-01.png',
                            'linkText' => 'Read more>',
                            'linkUrl' => '#'
                        ],
                        [
                            'description' => '<p>COVID-19: Effective<br/>Internal Comms in<br/>Unprecedented Times</p>',
                            'imageUrl' => '/content/img/Crop-5-01.png',
                            'linkText' => 'Read more>',
                            'linkUrl' => '#'
                        ],
                        [
                            'description' => '<p>Coronavirus:<br/>Dealing with supply<br/>chain disruption</p>',
                            'imageUrl' => '/content/img/Crop-4-01.png',
                            'linkText' => 'Read more>',
                            'linkUrl' => '#'
                        ],
                        [
                            'description' => '<p>Investor Communications<br/>in Times of Uncertainty</p>',
                            'imageUrl' => '/content/img/Crop-2-01.png',
                            'linkText' => 'Read more>',
                            'linkUrl' => '#'
                        ]
                    ]
                ],
                'contact' => [
                    [
                        'name' => 'Tom Evrard',
                        'role' => 'Senior Managing Director, Strategic Communications',
                        'email' => 'tom.evrard@fticonsulting.com',
                        'linkedin' => 'https://www.linkedin.com/in/tom-evrard/',
                        'imageSrc' => './assets/img/team-members/getInTouch_placeholder-avatar.jpg',
                    ],
                    [
                        'name' => 'Meredith Griffanti',
                        'role' => 'Managing Director, Strategic Communications',
                        'email' => 'Meredith.Griffanti@fticonsulting.com',
                        'linkedin' => 'https://www.linkedin.com/in/mgriffanti/',
                        'imageSrc' => './assets/img/team-members/Meredith-Griffanti-Escalation-Planning-_-Reponse.jpg',
                    ],
                    [
                        'name' => 'James Melville-Ross',
                        'role' => 'UK Head, Crisis Communication',
                        'email' => 'James.Melville-Ross@FTIConsulting.com',
                        'linkedin' => 'https://www.linkedin.com/in/james-melville-ross-82843417/',
                        'imageSrc' => './assets/img/team-members/James-Melville-Ross-Escalation-planning-_-Response.jpg',
                    ]
                ]
            ]
        ]
    ],
    '/remediation-and-dispute-resolution' => [
        'layout' => __DIR__ . '/../layouts/default.php',
        'template' => __DIR__ . '/../templates/lever2.php',
        'data' => [
            'sections' => [
                'perspectives' => [
                    'title' => 'PERSPECTIVES',
                    'slides' => [
                        [
                            'description' => '<p>Investor Communications<br/>in Times of Uncertainty</p>',
                            'imageUrl' => '/content/img/Crop-2-01.png',
                            'linkText' => 'Read more>',
                            'linkUrl' => '#'
                        ],
                        [
                            'description' => '<p>COVID-19: Effective<br/>Internal Comms in<br/>Unprecedented Times</p>',
                            'imageUrl' => '/content/img/Crop-5-01.png',
                            'linkText' => 'Read more>',
                            'linkUrl' => '#'
                        ],
                        [
                            'description' => '<p>Coronavirus:<br/>Dealing with supply<br/>chain disruption</p>',
                            'imageUrl' => '/content/img/Crop-4-01.png',
                            'linkText' => 'Read more>',
                            'linkUrl' => '#'
                        ],
                        [
                            'description' => '<p>Investor Communications<br/>in Times of Uncertainty</p>',
                            'imageUrl' => '/content/img/Crop-2-01.png',
                            'linkText' => 'Read more>',
                            'linkUrl' => '#'
                        ]
                    ]
                ],

                'contact' => [
                    [
                        'name' => 'Almira Cemmell',
                        'role' => 'Head of GRIP, Europe &amp; Africa',
                        'email' => 'Almira.Cemmell@fticonsulting.com',
                        'linkedin' => 'https://www.linkedin.com/in/almira-zejnilagic-423a14/',
                        'imageSrc' => './assets/img/team-members/Almira-Cemmell-Remediation-and-Dispute-Resolution.jpg'
                    ],
                    [
                        'name' => 'John Ellison',
                        'role' => 'Senior Managing Director',
                        'email' => 'John.Ellison@fticonsulting.com',
                        'linkedin' => 'https://www.linkedin.com/in/john-ellison-62a1ab142/',
                        'imageSrc' => './assets/img/team-members/John-Ellison-Remediation-_-Dispute-Resolution.jpg'
                    ]
                ]
            ]
        ]
    ],
    '/government-and-stakeholder-relations' => [
        'layout' => __DIR__ . '/../layouts/default.php',
        'template' => __DIR__ . '/../templates/lever3.php',
        'data' => [
            'sections' => [
                'perspectives' => [
                    'title' => 'PERSPECTIVES',
                    'slides' => [
                        [
                            'description' => '<p>Investor Communications<br/>in Times of Uncertainty</p>',
                            'imageUrl' => '/content/img/Crop-2-01.png',
                            'linkText' => 'Read more>',
                            'linkUrl' => '#'
                        ],
                        [
                            'description' => '<p>COVID-19: Effective<br/>Internal Comms in<br/>Unprecedented Times</p>',
                            'imageUrl' => '/content/img/Crop-5-01.png',
                            'linkText' => 'Read more>',
                            'linkUrl' => '#'
                        ],
                        [
                            'description' => '<p>Coronavirus:<br/>Dealing with supply<br/>chain disruption</p>',
                            'imageUrl' => '/content/img/Crop-4-01.png',
                            'linkText' => 'Read more>',
                            'linkUrl' => '#'
                        ],
                        [
                            'description' => '<p>Investor Communications<br/>in Times of Uncertainty</p>',
                            'imageUrl' => '/content/img/Crop-2-01.png',
                            'linkText' => 'Read more>',
                            'linkUrl' => '#'
                        ]
                    ]
                ],

                'contact' => [
                    [
                        'name' => 'Alex Deane',
                        'role' => 'UK Head, Public Affairs',
                        'email' => 'Alex.Deane@fticonsulting.com',
                        'linkedin' => 'https://www.linkedin.com/in/ajcdeane/',
                        'imageSrc' => './assets/img/team-members/Alex-Deane-Government-_-Stakeholder.jpg'
                    ],
                    [
                        'name' => 'Julia Harrison',
                        'role' => 'Global Head, Public Affairs',
                        'email' => 'julia.harrison@fticonsulting.com',
                        'linkedin' => 'https://www.linkedin.com/in/julia-harrison-/',
                        'imageSrc' => './assets/img/team-members/Julia-Harrison-Govenment-_-Stakeholder-Communication.jpg'
                    ],
                    [
                        'name' => 'Simon Lewis',
                        'role' => 'Vice Chairman, EMEA',
                        'email' => 'Simon.Lewis@fticonsulting.com',
                        'linkedin' => 'https://www.linkedin.com/in/simon-lewis-b94139167/',
                        'imageSrc' => './assets/img/team-members/Simon-Lewis-Government-_-Stakeholder-Communications.jpg'
                    ]
                ]
            ]

        ]
    ],
    '/real-time-data-analytics' => [
        'layout' => __DIR__ . '/../layouts/default.php',
        'template' => __DIR__ . '/../templates/lever8.php',
        'data' => [
            'sections' => [
                'perspectives' => [
                    'title' => 'PERSPECTIVES',
                    'slides' => [
                        [
                            'description' => '<p>Investor Communications<br/>in Times of Uncertainty</p>',
                            'imageUrl' => '/content/img/Crop-2-01.png',
                            'linkText' => 'Read more>',
                            'linkUrl' => '#'
                        ],
                        [
                            'description' => '<p>COVID-19: Effective<br/>Internal Comms in<br/>Unprecedented Times</p>',
                            'imageUrl' => '/content/img/Crop-5-01.png',
                            'linkText' => 'Read more>',
                            'linkUrl' => '#'
                        ],
                        [
                            'description' => '<p>Coronavirus:<br/>Dealing with supply<br/>chain disruption</p>',
                            'imageUrl' => '/content/img/Crop-4-01.png',
                            'linkText' => 'Read more>',
                            'linkUrl' => '#'
                        ],
                        [
                            'description' => '<p>Investor Communications<br/>in Times of Uncertainty</p>',
                            'imageUrl' => '/content/img/Crop-2-01.png',
                            'linkText' => 'Read more>',
                            'linkUrl' => '#'
                        ]
                    ]
                ],
                'contact' => [
                    [
                        'name' => 'Alex Deane',
                        'role' => 'UK Head, Public Affairs',
                        'email' => 'Alex.Deane@fticonsulting.com',
                        'linkedin' => 'https://www.linkedin.com/in/ajcdeane/',
                        'imageSrc' => './assets/img/team-members/Alex-Deane-Government-_-Stakeholder.jpg',
                    ],
                    [
                        'name' => 'Julia Harrison',
                        'role' => 'Global Head, Public Affairs',
                        'email' => 'julia.harrison@fticonsulting.com',
                        'linkedin' => 'https://www.linkedin.com/in/julia-harrison-/',
                        'imageSrc' => './assets/img/team-members/Julia-Harrison-Govenment-_-Stakeholder-Communication.jpg',
                    ],
                    [
                        'name' => 'Simon Lewis',
                        'role' => 'Vice Chairman, EMEA',
                        'email' => 'Simon.Lewis@fticonsulting.com',
                        'linkedin' => 'https://www.linkedin.com/in/simon-lewis-b94139167/',
                        'imageSrc' => './assets/img/team-members/Simon-Lewis-Government-_-Stakeholder-Communications.jpg'
                    ]
                ]

            ]

        ]
    ],
    '/economic-impact-sustainability' => [
        'layout' => __DIR__ . '/../layouts/default.php',
        'template' => __DIR__ . '/../templates/lever4.php',
        'data' => [
            'sections' => [
                'perspectives' => [
                    'title' => 'PERSPECTIVES',
                    'slides' => [
                        [
                            'description' => '<p>Investor Communications<br/>in Times of Uncertainty</p>',
                            'imageUrl' => '/content/img/Crop-2-01.png',
                            'linkText' => 'Read more>',
                            'linkUrl' => '#'
                        ],
                        [
                            'description' => '<p>COVID-19: Effective<br/>Internal Comms in<br/>Unprecedented Times</p>',
                            'imageUrl' => '/content/img/Crop-5-01.png',
                            'linkText' => 'Read more>',
                            'linkUrl' => '#'
                        ],
                        [
                            'description' => '<p>Coronavirus:<br/>Dealing with supply<br/>chain disruption</p>',
                            'imageUrl' => '/content/img/Crop-4-01.png',
                            'linkText' => 'Read more>',
                            'linkUrl' => '#'
                        ],
                        [
                            'description' => '<p>Investor Communications<br/>in Times of Uncertainty</p>',
                            'imageUrl' => '/content/img/Crop-2-01.png',
                            'linkText' => 'Read more>',
                            'linkUrl' => '#'
                        ]
                    ]
                ],

                'contact' => [
                    [
                        'name' => 'Kerstin Duhme',
                        'role' => 'Head of Energy, Transport &amp; Environment, Brussels',
                        'email' => 'Kerstin.Duhme@fticonsulting.com',
                        'linkedin' => 'https://www.linkedin.com/in/kerstin-duhme-21b4015/',
                        'imageSrc' => './assets/img/team-members/Kerstin-Duhme-Economic-Impact-_-Sustainability.jpg',
                    ],
                    [
                        'name' => 'Martin Porter',
                        'role' => 'Senior Adviser',
                        'email' => 'Martin.Porter@fticonsulting.com',
                        'linkedin' => 'https://www.linkedin.com/in/martin-porter-74949716/',
                        'imageSrc' => './assets/img/team-members/Martin-Porter-Economic-Impact-_-Sustainability.jpg',
                    ],
                ]
            ]

        ]
    ],
    '/business-model-workforce-transformation' => [
        'layout' => __DIR__ . '/../layouts/default.php',
        'template' => __DIR__ . '/../templates/lever5.php',
        'data' => [
            'sections' => [
                'perspectives' => [
                    'title' => 'PERSPECTIVES',
                    'slides' => [
                        [
                            'description' => '<p>Investor Communications<br/>in Times of Uncertainty</p>',
                            'imageUrl' => '/content/img/Crop-2-01.png',
                            'linkText' => 'Read more>',
                            'linkUrl' => '#'
                        ],
                        [
                            'description' => '<p>COVID-19: Effective<br/>Internal Comms in<br/>Unprecedented Times</p>',
                            'imageUrl' => '/content/img/Crop-5-01.png',
                            'linkText' => 'Read more>',
                            'linkUrl' => '#'
                        ],
                        [
                            'description' => '<p>Coronavirus:<br/>Dealing with supply<br/>chain disruption</p>',
                            'imageUrl' => '/content/img/Crop-4-01.png',
                            'linkText' => 'Read more>',
                            'linkUrl' => '#'
                        ],
                        [
                            'description' => '<p>Investor Communications<br/>in Times of Uncertainty</p>',
                            'imageUrl' => '/content/img/Crop-2-01.png',
                            'linkText' => 'Read more>',
                            'linkUrl' => '#'
                        ]
                    ]
                ],

                'contact' => [
                    [
                        'name' => 'Leslie Benson',
                        'role' => 'Global Head, People & Change',
                        'email' => 'Leslie.Benson@fticonsulting.com',
                        'linkedin' => 'https://www.linkedin.com/in/leslie-benson/',
                        'imageSrc' => './assets/img/team-members/Leslie-Benson-Business-Model-_-Workforce-Transformation.jpg',
                    ],
                    [
                        'name' => 'John Malone',
                        'role' => 'Senior Managing Director',
                        'email' => 'John.Maloney@fticonsulting.com',
                        'linkedin' => 'https://www.linkedin.com/in/john-maloney-61ab156/',
                        'imageSrc' => './assets/img/team-members/John-Maloney-Business-Model-_-Workforce-Transformation.jpg',
                    ],
                    [
                        'name' => 'Victoria Strachwitz',
                        'role' => 'Global Head, People & Change',
                        'email' => 'victoria.strachwitz@fticonsulting.com',
                        'linkedin' => 'https://www.linkedin.com/in/victoria-strachwitz-786a557a/',
                        'imageSrc' => './assets/img/team-members/getInTouch_placeholder-avatar.jpg',
                    ],
                ]
            ]

        ]
    ],
    '/operational-financial-resilience' => [
        'layout' => __DIR__ . '/../layouts/default.php',
        'template' => __DIR__ . '/../templates/lever6.php',
        'data' => [
            'sections' => [
                'perspectives' => [
                    'title' => 'PERSPECTIVES',
                    'slides' => [
                        [
                            'description' => '<p>Investor Communications<br/>in Times of Uncertainty</p>',
                            'imageUrl' => '/content/img/Crop-2-01.png',
                            'linkText' => 'Read more>',
                            'linkUrl' => '#'
                        ],
                        [
                            'description' => '<p>COVID-19: Effective<br/>Internal Comms in<br/>Unprecedented Times</p>',
                            'imageUrl' => '/content/img/Crop-5-01.png',
                            'linkText' => 'Read more>',
                            'linkUrl' => '#'
                        ],
                        [
                            'description' => '<p>Coronavirus:<br/>Dealing with supply<br/>chain disruption</p>',
                            'imageUrl' => '/content/img/Crop-4-01.png',
                            'linkText' => 'Read more>',
                            'linkUrl' => '#'
                        ],
                        [
                            'description' => '<p>Investor Communications<br/>in Times of Uncertainty</p>',
                            'imageUrl' => '/content/img/Crop-2-01.png',
                            'linkText' => 'Read more>',
                            'linkUrl' => '#'
                        ]
                    ]
                ],
                'contact' => [
                    [
                        'name' => 'Karen Briggs',
                        'role' => 'Head of Forensic &amp; Litigation Consulting, EMEA',
                        'email' => 'Karen.Briggs@fticonsulting.com',
                        'linkedin' => 'https://www.linkedin.com/in/karen-briggs-70827a130/',
                        'imageSrc' => './assets/img/team-members/FTI-30720-Karen-14.jpg',
                    ],
                    [
                        'name' => 'Simon Granger',
                        'role' => 'Head of Corporate Finance &amp; Restructuring, EMEA',
                        'email' => 'Simon.Granger@FTIConsulting.com',
                        'linkedin' => 'https://www.linkedin.com/in/simon-granger-6abba75/',
                        'imageSrc' => './assets/img/team-members/Simon-Granger-Operational-_-Financial-Resilience.jpg',
                    ],
                ]
            ]

        ]
    ],
    '/digital-trust-and-ecosystems' => [
        'layout' => __DIR__ . '/../layouts/default.php',
        'template' => __DIR__ . '/../templates/lever7.php',
        'data' => [
            'sections' => [
                'perspectives' => [
                    'title' => 'PERSPECTIVES',
                    'slides' => [
                        [
                            'description' => '<p>Investor Communications<br/>in Times of Uncertainty</p>',
                            'imageUrl' => '/content/img/Crop-2-01.png',
                            'linkText' => 'Read more>',
                            'linkUrl' => '#'
                        ],
                        [
                            'description' => '<p>COVID-19: Effective<br/>Internal Comms in<br/>Unprecedented Times</p>',
                            'imageUrl' => '/content/img/Crop-5-01.png',
                            'linkText' => 'Read more>',
                            'linkUrl' => '#'
                        ],
                        [
                            'description' => '<p>Coronavirus:<br/>Dealing with supply<br/>chain disruption</p>',
                            'imageUrl' => '/content/img/Crop-4-01.png',
                            'linkText' => 'Read more>',
                            'linkUrl' => '#'
                        ],
                        [
                            'description' => '<p>Investor Communications<br/>in Times of Uncertainty</p>',
                            'imageUrl' => '/content/img/Crop-2-01.png',
                            'linkText' => 'Read more>',
                            'linkUrl' => '#'
                        ]
                    ]
                ],

                'contact' => [
                    [
                        'name' => 'Joshua Burch',
                        'role' => 'Head of Cybersecurity, EMEA',
                        'email' => 'Joshua.Burch@fticonsulting.com',
                        'linkedin' => 'https://www.linkedin.com/in/james-melville-ross-82843417/',
                        'imageSrc' => './assets/img/team-members/getInTouch_placeholder-avatar.jpg',
                    ],
                    [
                        'name' => 'Anthony J. Ferrante',
                        'role' => 'Global Head, Cybersecurity',
                        'email' => 'ajf@fticonsulting.com',
                        'linkedin' => 'https://www.linkedin.com/in/anthonyjferrante/',
                        'imageSrc' => './assets/img/team-members/Anthony-J-Ferrante.jpg'
                    ],
                ]
            ]

        ]
    ],
    '/the-report' => [
        'layout' => __DIR__ . '/../layouts/default.php',
        'template' => __DIR__ . '/../templates/report.php',
        'data' => [
            'title' => 'FTI Resilience',
            'header' => [
                'title' => "Resilience Barometer " . date('Y'),
                'detailImage' => 'content/img/home-header-detail.svg',
            ],
            'leftColumn' => [
                'description' => "The Resilience Barometer™ 2020 data reveals how, for too many organisations, the pandemic has exposed a lack of resilience across critical areas: from business models to supply chains, and from crisis management to regulatory changes in an increasingly fragmented world. Whatever the business, industry or geography, a collective understanding of resilience is now critical.",
                'body' => "The 2020 FTI Consulting Resilience Barometer incorporates the views of 2,276 respondents from large companies across all G20 countries. Read below to find out how business leaders across the G20 are preparing for the top emerging risks and crises that threaten revenue, value and reputation.",
                'accordion_items' => [
                    'resilience_score' => [
                        'heading' => "RESILIENCE SCORE:\nA MEASURE OF PREPAREDNESS",
                        'copy' => [
                            "The 2020 FTI Consulting Resilience Barometer shows that companies across the G20 achieved an average resilience score of 43 out of a total possible score of 100, improving slightly on last year’s average score of 40. But as companies face more complex risks from technology transformation, geopolitical tensions and the polarisation of the political landscape in a digitally connected world, they remain inadequately prepared.",
                            "<p>The survey of more than 2,200 executives from private and publicly traded companies across all G20 countries, measures the preparedness of companies against 18 scenarios most likely to negatively impact the bottom line, affect reputation and lead to increased regulatory pressure.</p>
<p>Encouragingly, in the face of such a dramatic increase in risk, this year’s survey did point to a small improvement in resilience scores, suggesting that some organisations are managing to contain and manage emerging threats through better preparedness.</p>
<p>Around the world, FTI Consulting professionals work closely with clients to anticipate and address the increasingly complex business challenges arising from the risks featured in this report. By testing the impact of 18 distinct scenarios most likely to negatively impact the bottom line, adversely affect reputation and lead to increased regulatory pressure, companies can develop strategic responses.</p>",
                        ],
                    ],
                    'corporate_risks' => [
                        'heading' => "CORPORATE RISKS:\nPREPARING FOR EMERGING THREATS",
                        'copy' => "Companies across the globe are facing unprecedented and increasingly complex challenges as digital disruption accelerates structural, geopolitical and societal changes. As these global risks intensify, companies need to rethink their preparedness strategies to build resilience.",
                        'box' => "‘Leaks of sensitive internal communications’ climbed into the top five corporate risk from sixth position last year",
                        'stats' => [
                            'number' => '50%',
                            'copy' => "of the companies we surveyed use Artificial Intelligence or Machine Learning to help avoid or prepare for emerging threats",
                        ],
                    ],
                    'hidden_cost' => [
                        'heading' => "CRISIS:\nTHE HIDDEN COST",
                        'copy' => "Our survey shows that the financial and reputational health of a company is not the only measure affected when a crisis hits. It also shows the damaging effects crises can wreak on the psychological and physical health of senior executives tasked with steering the company through challenging times.",
                        'stats' => [
                            [
                                'number' => '87%',
                                'copy' => "of respondents claim they have had a significant crisis situation negatively impacting their business in 2019",
                            ],
                            [
                                'number' => '84%',
                                'copy' => "of the companies surveyed expect a crisis in 2020",
                            ],
                            [
                                'number' => '36%',
                                'copy' => "reported mental health issues as a result of dealing with a crisis",
                            ],
                        ],
                    ],
                    'regulation' => [
                        'heading' => "REGULATION:\nDIALOGUE TO CREATE SUSTAINABLE FRAMEWORKS",
                        'copy' => "Technology transformation, geopolitical tensions and polarisation of the political landscape has made for more complex regulatory risks facing companies today. Resilient companies are engaging with a range of stakeholders in the policy- making process, re-affirming their licence to operate.",
                        'stats' => [
                            [
                                'number' => '81%',
                                'copy' => "of respondents expect to see an increase in regulations in the next 12 months, compared to 76% last year",
                            ],
                            [
                                'number' => '71%',
                                'copy' => "of large companies have been investigated for market dominance in the last 12 months",
                            ],
                            [
                                'number' => '42%',
                                'copy' => "of leaders believe they should personally comment on political and regulatory changes affecting their company",
                            ],
                        ],
                    ],
                    'sustainability' => [
                        'heading' => "SUSTAINABILITY:\nENGAGING FOR GROWTH",
                        'copy' => [
                            "Companies are increasingly expected to play a lead role in addressing environmental, social and governance (ESG) issues that have traditionally been the preserve of governments. The most resilient companies have embedded sustainability into their business models and decision-making structures and engage with a range of stakeholders to prepare for related risks.",
                            "<span class='d-block text-size-6rem'>39%</span>of respondents say the biggest pressure to be more transparent comes from regulators, compared to <strong class='text-primary'>37% for customers</strong>, <strong class='text-info'>19% for media</strong> and <strong class='text-success'>13% from activists or NGOs</strong>."
                        ],
                        'top_five_issues' => [
                            [
                                'copy' => "Energy Consumption",
                                'number' => '65%',
                            ],
                            [
                                'copy' => "Employee health and safety",
                                'number' => '60%',
                            ],
                            [
                                'copy' => "Labour practices",
                                'number' => '54%',
                            ],
                            [
                                'copy' => "Anticorruption practices",
                                'number' => '53%',
                            ],
                            [
                                'copy' => "Management of the legal and regulatory environment",
                                'number' => '52%',
                            ],
                        ],
                    ],
                    'cybersecurity' => [
                        'heading' => "CYBERSECURITY:\nRESILIENCE REQURES PROACTIVITY",
                        'copy' => "The complex and ever-changing nature of cyber risk requires a continued evolution in how organisations approach resilience. No longer is access constrained to the four walls of an organisation. Any connected entity can serve as a point of entry, including third-party vendors who act as a “back door” to larger enterprise networks. Responding effectively to cyber risk requires proactive and holistic management helping mitigate threats, reduce downtime, and protect an organisation’s reputation.",
                        'stats' => [
                            [
                                'number' => '90%',
                                'copy' => "of G20 leaders surveyed believe they have cybersecurity gaps",
                            ],
                            [
                                'copy' => "<span class='text-primary'>At least 1 in 4</span> organisations surveyed have experienced a cyber attack where assets were stolen or compromised in the last 12 months",
                            ],
                            [
                                'number' => '20%',
                                'copy' => "of companies were victims of a ransom or data hostage situation in 2019",
                            ],
                            [
                                'number' => '28%',
                                'copy' => "of respondents believe ‘employee awareness, security, culture and training’ are their biggest security gaps, and 35% have invested in this area over the past 12 months",
                            ],
                        ],
                    ],
                    'technology' => [
                        'heading' => "TECHNOLOGY:\nTRANSFORMING BUSINESS MODELS",
                        'copy' => "Digital disruption is an all-pervasive trend that presents companies with an opportunity to transform their business models to drive long term success. Resilient companies optimise their planning decisions by making technology transformation a strategic priority.",
                        'stats' => [
                            'number' => '59%',
                            'copy' => "of respondents believe ai & machine learning will impact significantly over the next 10 years",
                        ],
                    ],
                    'research_methodology' => [
                        'heading' => "RESEARCH METHODOLOGY",
                        'copy' => [
                            "The 2020 FTI Consulting Resilience Barometer incorporates the views of more than 2,000 respondents from large companies across all G20 countries. The quantitative survey was conducted in November 2019 and respondent profiles replicate those used in last year’s FTI Consulting Resilience Barometer.",
                            "<p>Each country’s results have been weighted so that each country represents a similar proportion in the total ‘G20’ results.  The majority (74%) of respondents were C-suite and senior managers executives from privately owned companies, while 26% were from publicly listed entities.</p>
<p>Respondents reported an average global turnover of USD 17,439 million over the past 12 months.  Companies reporting a global turnover of USD 1 billion over the past 12 months comprised 10% of the sample size, while those reporting more than USD 100 billion made up 7% and those reporting USD 100m made up 7%.  In total participating companies employ a global sum of 58 million people, employing an average of 23,336 people.</p>
<p>Respondents were classified according to industry, with Technology & Communications (26%); Consumer Goods (19%); Financials (14%) and Services (10%) making up the bulk of the sample.</p>",
                        ],
                    ],
                ],
            ],
            'rightColumn' => [

            ],
            'sections'=>[
                'contact' => [
                    [
                        'name' => 'Caroline Das-Monfrais',
                        'role' => 'Global Resilience Lead',
                        'linkedin' => 'https://www.linkedin.com/in/carolinedas-monfrais',
                        'imageSrc' => './assets/img/team-members/getInTouch_placeholder-avatar.jpg',
                    ],
                ]
            ]
        ]
    ],
    '/about' => [
        'layout' => __DIR__ . '/../layouts/default.php',
        'template' => __DIR__ . '/../templates/about.php',
        'data' => [


        ]
    ],
    '/perspectives' => [
        'layout' => __DIR__ . '/../layouts/default.php',
        'template' => __DIR__ . '/../templates/persperctives.php',
        'data' => [


        ]
    ],
    '/perspectives-home' => [
        'layout' => __DIR__ . '/../layouts/default.php',
        'template' => __DIR__ . '/../templates/persperctives-home.php',
        'data' => [
        ]
    ],
    '/escalation-planning-and-response/anatomy-of-a-crisis-I' => [
        'layout' => __DIR__ . '/../layouts/default.php',
        'template' => __DIR__ . '/../templates/crisis-1.php',
        'data' => [

        ]
    ],
    '/escalation-planning-and-response/the-anatomy-of-a-crisis-volume-2' => [
        'layout' => __DIR__ . '/../layouts/default.php',
        'template' => __DIR__ . '/../templates/crisis-2.php',
        'data' => [

        ]
    ],
    '/escalation-planning-and-response/covid-19' => [
        'layout' => __DIR__ . '/../layouts/default.php',
        'template' => __DIR__ . '/../templates/crisis-3.php',
        'data' => [

        ]
    ],
    '/operational-financial-resilience/Approaches-to-IP-Valuation-for-Pandemic-Recovery' => [
        'layout' => __DIR__ . '/../layouts/default.php',
        'template' => __DIR__ . '/../templates/crisis-4.php',
        'data' => [

        ]
    ],
    '/operational-financial-resilience/COVID-19-and-Commercial-Real-Estate-Considerations' => [
        'layout' => __DIR__ . '/../layouts/default.php',
        'template' => __DIR__ . '/../templates/crisis-5.php',
        'data' => [

        ]
    ],
    '/operational-financial-resilience/How-European-Retailers-Can-Manage-COVID-19' => [
        'layout' => __DIR__ . '/../layouts/default.php',
        'template' => __DIR__ . '/../templates/crisis-6.php',
        'data' => [

        ]
    ],
    '/economic-impact-sustainability/climate-image-a-new-growth-trajectory' => [
        'layout' => __DIR__ . '/../layouts/default.php',
        'template' => __DIR__ . '/../templates/crisis-7.php',
        'data' => [

        ]
    ],
    '/economic-impact-sustainability/A-Test-of-Resilience-COVID-19' => [
        'layout' => __DIR__ . '/../layouts/default.php',
        'template' => __DIR__ . '/../templates/crisis-8.php',
        'data' => [

        ]
    ],
    '/economic-impact-sustainability/Time-to-Rethink-the-S-in-ESG' => [
        'layout' => __DIR__ . '/../layouts/default.php',
        'template' => __DIR__ . '/../templates/crisis-9.php',
        'data' => [

        ]
    ],
    '/digital-trust-and-ecosystems/The-Anatomy-of-a-Crisis-3-Cyber-Breaches' => [
        'layout' => __DIR__ . '/../layouts/default.php',
        'template' => __DIR__ . '/../templates/crisis-10.php',
        'data' => [

        ]
    ],
    '/digital-trust-and-ecosystems/COVID-19-New-Cyber-Threats' => [
        'layout' => __DIR__ . '/../layouts/default.php',
        'template' => __DIR__ . '/../templates/crisis-11.php',
        'data' => [

        ]
    ],
    '/digital-trust-and-ecosystems/Building-Cybersecurity-Resilience' => [
        'layout' => __DIR__ . '/../layouts/default.php',
        'template' => __DIR__ . '/../templates/crisis-12.php',
        'data' => [

        ]
    ],

    '/remediation-and-dispute-resolution/COVID-19:CommercialRiskMitigation&DamagesClaimsinAfrica' => [
        'layout' => __DIR__ . '/../layouts/default.php',
        'template' => __DIR__ . '/../templates/crisis-13.php',
        'data' => [

        ]
    ],
    '/remediation-and-dispute-resolution/DecadeofDisputes:TheTrillionDollarInvestorView' => [
        'layout' => __DIR__ . '/../layouts/default.php',
        'template' => __DIR__ . '/../templates/crisis-14.php',
        'data' => [

        ]
    ],
    '/remediation-and-dispute-resolution/Tackling-financial-crime:-now-is-the-time-to-break-down-silos' => [
        'layout' => __DIR__ . '/../layouts/default.php',
        'template' => __DIR__ . '/../templates/crisis-15.php',
        'data' => [

        ]
    ],

    '/government-and-stakeholder-relations/The-CEO-Brand-and-Its-Impact-On-Business' => [
        'layout' => __DIR__ . '/../layouts/default.php',
        'template' => __DIR__ . '/../templates/crisis-16.php',
        'data' => [

        ]
    ],
    '/government-and-stakeholder-relations/Activism-Vulnerability-Report' => [
        'layout' => __DIR__ . '/../layouts/default.php',
        'template' => __DIR__ . '/../templates/crisis-17.php',
        'data' => [

        ]
    ],
    '/government-and-stakeholder-relations/The-New-T&Cs-of-Investor-Relations' => [
        'layout' => __DIR__ . '/../layouts/default.php',
        'template' => __DIR__ . '/../templates/crisis-18.php',
        'data' => [

        ]
    ],

    '/business-model-workforce-transformation/Rethinking-RIF-Workforce-Optimization-in-Healthcare' => [
        'layout' => __DIR__ . '/../layouts/default.php',
        'template' => __DIR__ . '/../templates/crisis-19.php',
        'data' => [

        ]
    ],
    '/business-model-workforce-transformation/Toward-a-Brave-New-World:-reboarding-your-workforce-in-the-new-normal' => [
        'layout' => __DIR__ . '/../layouts/default.php',
        'template' => __DIR__ . '/../templates/crisis-20.php',
        'data' => [

        ]
    ],
    '/business-model-workforce-transformation/The-Case-for-Culture' => [
        'layout' => __DIR__ . '/../layouts/default.php',
        'template' => __DIR__ . '/../templates/crisis-21.php',
        'data' => [

        ]
    ],
    '/real-time-data-analytics/How-Will-Data-Fare-in-the-Global-Fight-Against-the-Pandemic' => [
        'layout' => __DIR__ . '/../layouts/default.php',
        'template' => __DIR__ . '/../templates/crisis-23.php',
        'data' => [

        ]
    ],
    '/real-time-data-analytics/COVID-19-Impact-on-Our-Data-Footprint' => [
        'layout' => __DIR__ . '/../layouts/default.php',
        'template' => __DIR__ . '/../templates/crisis-24.php',
        'data' => [

        ]
    ],
    '/real-time-data-analytics/A-new-era-of-data-analytics-for-investigations-and-litigation' => [
        'layout' => __DIR__ . '/../layouts/default.php',
        'template' => __DIR__ . '/../templates/crisis-25.php',
        'data' => [

        ]
    ],
    '/press' => [
        'layout' => __DIR__ . '/../layouts/default.php',
        'template' => __DIR__ . '/../templates/press.php',
        'data' => [

        ]
    ],

];
