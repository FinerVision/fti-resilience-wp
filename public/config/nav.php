<?php

return [
    [
        'title' => 'HOME',
        'link' => '/',
    ],
    [
        'title' => 'RESILIENCE AGENDA',
        'link' => '/escalation-planning-and-response',
        'dropdown' => [
            [
                'link' => '/business-model-workforce-transformation',
                'name' => 'BUSINESS MODEL & WORKFORCE TRANSFORMATION'
            ],
            [
                'link' => '/digital-trust-and-ecosystems',
                'name' => 'DIGITAL TRUST & ECOSYSTEMS'
            ],
            [
                'link' => '/economic-impact-sustainability',
                'name' => 'ECONOMIC IMPACT & SUSTAINABILITY'
            ],
            [
                'link' => '/escalation-planning-and-response',
                'name' => 'ESCALATION PLANNING & RESPONSE',
            ],
            [
                'link' => '/government-and-stakeholder-relations',
                'name' => 'GOVERNMENT & STAKEHOLDER RELATIONS',
            ],
            [
                'link' => '/operational-financial-resilience',
                'name' => 'OPERATIONAL & FINANCIAL RESILIENCE',
            ],
            [
                'link' => '/real-time-data-analytics"',
                'name' => 'REAL-TIME DATA ANALYTICS',
            ],
            [
                'link' => '/remediation-and-dispute-resolution',
                'name' => 'REMEDIATION DISPUTE RESOLUTION',
            ],
        ]
    ],
    [
        'title' => 'PERSPECTIVES',
        'link' => '/perspectives-home',
    ],
    [
        'title' => 'THE REPORT',
        'link' => '/the-report',
    ],
    [
        'title' => 'PRESS',
        'link' => '/press',
    ],
    [
        'title' => 'ABOUT',
        'link' => '/about',
    ],
];
