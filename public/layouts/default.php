<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css"
          integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ=="
          crossorigin="">
    <link
        href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:ital,wght@0,200;0,300;0,400;0,700;0,900;1,200;1,300;1,400;1,600;1,700;1,900&display=swap"
        rel="stylesheet">
    <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css"/>
    <title><?php echo $title; ?></title>
    <link rel="stylesheet" href="/assets/index.css"/>

</head>
<body class="Section--blue-1000">
<?php

$pathname = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : '/';


require __DIR__ . '/../components/header.php'; ?>
<div class="template" >
    <?php include $template; ?>
</div>
<?php
require __DIR__ . '/../components/footer.php';
?>
<!--<script src="/runtime.4a114eaf5838f7a839fa.js"></script>-->
<!--<script src="/vendors.b713edcc99d32d2154ec.js"></script>-->
<!--<script src="/main.d0b33bc927e4a3c6fb3c.js"></script>-->
<script src="/assets/index.js"></script>


</body>
</html>
