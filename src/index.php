<?php

// @note basic router to serve up routes defined inside src/config/routes.php

$routes = require __DIR__ . '/config/routes.php';
$pathname = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : '/';

if (!isset($routes[$pathname])) {
    die('404 Page not Found');
}

$route = $routes[$pathname];
$template = isset($route['template']) ? $route['template'] : null;

if (isset($route['data'])) {
    foreach ($route['data'] as $key => $value) {
        ${"$key"} = $value;
    }
}

if (isset($route['layout'])) {
    include $route['layout'];
}
