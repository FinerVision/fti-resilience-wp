import "core-js/stable";
import 'bootstrap';
import "regenerator-runtime/runtime";
import React, {StrictMode} from "react";
import {hydrate, render} from "react-dom";
import "./scripts/home";
import "./scripts/report";
import "./scripts/components/LeverCarousel";
import "./scripts/components/PerspectiveCarousel";
import "./scripts/components/explore-another-level";
import "./scripts/Animation";
import App from "./scripts/components/triangle-graphic/app/app";
const triangleGraphic = document.getElementById('triangle-graphic');

(async () => {
  if (!("scrollBehavior" in document.documentElement.style)) {
    console.log("Loading scrollBehavior polyfill...")
    await import("scroll-behavior-polyfill");
  }

  if(triangleGraphic) {
    const renderer = {hydrate, render}[triangleGraphic.hasChildNodes() ? 'hydrate' : 'render'];
    renderer(<App/>, triangleGraphic);
  }
})();
