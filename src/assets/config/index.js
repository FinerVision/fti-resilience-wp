
const laverSlideFactory = (props) => {
  return {
    path: props.path,
    name: props.name,
    color: props.color || null
  }
};

export default {
  laverPages: [
    laverSlideFactory({
      path: '/escalation-planning-and-response',
      name: 'escalation-planning-and-response',
      color: '#5781A6',
    }),
    laverSlideFactory({
      path: '/remediation-and-dispute-resolution',
      name: 'remediation-and-dispute-resolution',
      color: '#2A6FA5',
    }),
    laverSlideFactory({
      path: '/government-and-stakeholder-relations',
      name: 'government-and-stakeholder-relations',
      color: '#0067B1',
    }),
    laverSlideFactory({
      path: '/economic-impact-sustainability',
      name: 'economic-impact-sustainability',
      color: '#00C9D4',
    }),
    laverSlideFactory({
      path: '/business-model-workforce-transformation',
      name: 'business-model-workforce-transformation',
      color: '#AFB2B3',
    }),
    laverSlideFactory({
      path: '/operational-financial-resilience',
      name: 'operational-financial-resilience',
      color: '#1BB680',
    }),
    laverSlideFactory({
      path: '/digital-trust-and-ecosystems',
      name: 'digital-trust-and-ecosystems',
      color: '#008FBE',
    }),
    laverSlideFactory({
      path: '/real-time-data-analytics',
      name: 'real-time-data-analytics',
      color: '#008FBE',
    })
  ]
}
