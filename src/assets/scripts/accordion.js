if (!Element.prototype.closest) {
  Element.prototype.closest = s => {
    let el = this;

    do {
      if (Element.prototype.matches.call(el, s)) return el;
      el = el.parentElement || el.parentNode;
    } while (el !== null && el.nodeType === 1);
    return null;
  };
}

export default class Accordion {
  #animationId = null;
  #items = [];
  #buttons = [];
  #currentKey = null;
  #parentContainer = null;
  #itemParentSelector = null;

  constructor(options = {}) {
    const {
      itemSelector,
      buttonSelector,
      itemParentSelector = null,
      parentContainer = null,
      shouldScrollIntoView = false,
    } = options;
    this.init(itemSelector, itemParentSelector, buttonSelector, parentContainer, shouldScrollIntoView);
  }

  init(itemSelector, itemParentSelector, buttonSelector, parentContainer, shouldScrollIntoView) {
    this.#items = document.querySelectorAll(itemSelector);
    this.#buttons = document.querySelectorAll(buttonSelector);
    this.#parentContainer = document.querySelector(parentContainer);
    this.#itemParentSelector = itemParentSelector;

    if(shouldScrollIntoView) {
      window.scrollTo(0, this.getOffsetTop(this.#buttons[0]));
    }
    this.#buttons.forEach((item, key) => {
      item.setAttribute('data-accordion-key', key);
      this.#items[key].setAttribute('data-accordion-key', key);
      item.addEventListener('click', this.#handleItemClick);
    })
  }

  getOffsetTop(element) {
    let offsetTop = 0;
    while(element) {
      offsetTop += element.offsetTop;
      element = element.offsetParent;
    }
    return offsetTop;
  }

  #handleItemClick = (e) => {
    const key = parseInt(e.currentTarget.getAttribute('data-accordion-key'));
    this.#currentKey = key;
    this.#collapseElement(this.#items[key], key);
  }

  hideAll = () => {
    if(this.#currentKey !== null) {
      this.#collapseElement(this.#items[this.#currentKey], this.#currentKey);
    }
  };

  #collapseElement = (element, key) => {
    let height = 0;
    if(element.clientHeight > 0) {
      if(this.#parentContainer) {
        requestAnimationFrame(() => {
          this.#parentContainer.style.maxHeight -= element.clientHeight;
        })
      }
      this.#animationId = requestAnimationFrame(() => {
        element.style.maxHeight = `0px`;
        this.#currentKey = null;
        this.#buttons[key].classList.remove('active');
        this.#itemParentSelector && this.#buttons[key].closest(this.#itemParentSelector).classList.remove('open');
      });
      return;
    }
    element.style.maxHeight = 'none';
    height = element.clientHeight;
    if(this.#parentContainer) {
      const parentHeight = this.#parentContainer.clientHeight;
      this.#parentContainer.style.maxHeight = `${parentHeight + element.clientHeight}px`;
    }
    element.style.maxHeight = '0px';
    this.#buttons[key].classList.add('active');
    this.#itemParentSelector && this.#buttons[key].closest(this.#itemParentSelector).classList.add('open');
    this.#animationId = requestAnimationFrame(() => {
      element.style.maxHeight = `${height}px`;
      this.#items.forEach((item, index) => {
        if (key !== index) {
          item.style.maxHeight = '0px';
          this.#buttons[index] && this.#buttons[index].classList.remove('active');
          this.#itemParentSelector && this.#buttons[index].closest(this.#itemParentSelector).classList.remove('open');
        }
      });
    })
  }
}
