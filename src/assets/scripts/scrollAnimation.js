export default class scrollAnimation {
  constructor({selector, onElementReached}) {
    this.init(selector);
  }
  #timer = null;
  windowOffsetBottom = null;
  collections = [];
  init(selector) {
    this.collections = document.querySelectorAll(selector);
    this.windowOffsetBottom = window.innerHeight + window.scrollY;
    console.log(selector);
    this.animate();
    window.addEventListener('scroll', this.animate.bind(this));
  }
  getOffsetTop(element) {
    let offsetTop = 0;
    while(element) {
      offsetTop += element.offsetTop;
      element = element.offsetParent;
    }
    return offsetTop;
  }

  animate(){
    this.windowOffsetBottom = window.innerHeight + window.scrollY;
    this.collections.forEach((elem, index) => {
      let elemOffsetBottom = elem.clientHeight + this.getOffsetTop(elem);
      if(this.getOffsetTop(elem) < this.windowOffsetBottom) {
        if(!elem.classList.contains('animate') && (elemOffsetBottom - window.scrollY > 0) || (this.windowOffsetBottom > elemOffsetBottom)) {
          this.#timer = setTimeout(() => {
            elem.classList.add('animate');

          }, index === 0 ? 0 : 100 * index);
          elem.addEventListener('animationend', (e) => {
            e.target.removeAttribute('id');

            clearTimeout(this.#timer);
          })
        }
      }
    })
  }
}
