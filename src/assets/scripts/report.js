import Accordion from "./accordion";

new Accordion({
  itemSelector: ".Accordion__item__content",
  itemParentSelector: ".Accordion__item",
  buttonSelector: '.Accordion__item__toggle',
});
