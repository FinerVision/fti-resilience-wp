const dropdownWrapper = document.querySelectorAll('.nav-item__dropdown');
const menu = document.querySelector('.custom-dropdown-menu');

if(dropdownWrapper && menu) {
  dropdownWrapper.forEach((item) => {
    item.addEventListener('mouseenter', () => {
      menu.style.opacity = 1;
      menu.style.zIndex = 1000;
    });
    item.addEventListener('mouseleave', () => {
      menu.style.opacity = 0;
      menu.style.zIndex = -1;
    });

  })
}
