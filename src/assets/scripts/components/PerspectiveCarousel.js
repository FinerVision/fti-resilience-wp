import SwiperCore, {Navigation} from 'swiper';


SwiperCore.use([Navigation]);
new SwiperCore('.perspective-swipper-container', {
  loop: true,
  direction: "horizontal",
  loopFillGroupWithBlank: true,
  navigation: {
    nextEl: '.PostCarousel__button-next',
    prevEl: '.PostCarousel__button-prev',
  },
  autoplay: {
    delay : 5000,
    disableOnInteraction : false,
  },
  spaceBetween: 0,

  slidesPerView: 1,
  centeredSlides: true,
  breakpoints: {
    // when window width is >= 320px
    320: {
      slidesPerView: 1,
      spaceBetween: 0
    },
    // when window width is >= 480px
    576: {
      slidesPerView: 1,
      spaceBetween: 0
    },
    // when window width is >= 640px
    992: {
      slidesPerView: 1,
      spaceBetween: 0
    },
    // when window width is >= 1200px
    1200: {
      slidesPerView: 1,
      spaceBetween: 0
    },

  }
});
