import { useCallback, useEffect, useState } from "react";

export default function useSize(element) {
  const [size, setSize] = useState({ width: 0, height: 0 });

  const updateSize = useCallback(() => {
    setSize({ width: element.innerWidth, height: element.innerHeight });
  }, [setSize, element]);

  useEffect(() => {
    updateSize();
    window.addEventListener("resize", updateSize);
    return () => {
      window.removeEventListener("resize", updateSize);
    };
  }, [element]);

  return size;
}
