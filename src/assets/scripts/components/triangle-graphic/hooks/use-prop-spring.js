import { useSpring } from "react-spring";

export default function usePropSpring(props) {
  return useSpring({
    ref: props.ref,
    delay: props.delay || 0,
    onStart: props.onStart,
    onRest: props.onRest,
    onFrame: props.onFrame,
    iconScale: props.scale || 1,
    textFill: props.textFill || "#fff",
  });
}
