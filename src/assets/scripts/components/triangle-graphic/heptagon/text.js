import React, { useCallback, useContext, useEffect } from "react";
import { motion, useAnimation } from "framer-motion";
import { HeptagonContext } from "../config/context";

const DURATION = 0.5;

export default function Text({ children, id }) {
  const { activeId, mobile } = useContext(HeptagonContext);
  const controls = useAnimation();

  const sequence = useCallback(async () => {
    await controls.start({
      opacity: mobile ? 1 : 0,
      transition: { duration: DURATION, delay: 0.3 },
    });
    await controls.start({
      opacity: mobile ? 0 : 1,
      transition: { duration: DURATION, delay: 0.5 },
    });
  }, [controls, mobile]);

  useEffect(() => {
    if (activeId === id && !mobile) {
      sequence();
    } else {
      controls.start({
        opacity: mobile ? 0 : 1,
        transition: { duration: DURATION },
      });
    }
  }, [activeId]);

  return (
    <motion.g
      className="text"
      animate={controls}
      initial={{ opacity: mobile ? 0 : 1 }}
    >
      {children}
    </motion.g>
  );
}
