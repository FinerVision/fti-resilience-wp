import React, { useContext } from "react";
import { animated, useSpring } from "react-spring/web.cjs";
import { HeptagonContext } from "../config/context";

export default function ReadMore({ x, y, id }) {
  const { activeId, mobile } = useContext(HeptagonContext);
  const active = activeId === id;

  const props = useSpring({
    delay: active ? (mobile ? 1500 : 250) : 0,
    opacity: activeId === id && !mobile ? 1 : 0,
  });

  return (
    <animated.g
      className="read-more"
      fill="#fff"
      style={{
        ...props,
        transform: `translate(${x}px, ${y}px)`,
      }}
    >
      <path
        d="m.5 23c0 12.4 10.1 22.5 22.5 22.5h79c12.4 0 22.5-10.1 22.5-22.5s-10.1-22.5-22.5-22.5h-79c-12.4 0-22.5 10.1-22.5 22.5z"
        fill="transparent"
        stroke="#fff"
        strokeMiterlimit="10"
        className="read-more-bg"
      />
      <path
        d="m79.5 18.6c-1.6 0-2.7 1.4-2.7 3.7s1.1 3.8 2.7 3.8 2.7-1.5 2.7-3.8-1.1-3.7-2.7-3.7z"
        fill="none"
      />
      <path
        d="m55.7 18.7h-1v7.3h1c2.1 0 3.2-1.3 3.2-3.7 0-2.3-1.1-3.6-3.2-3.6z"
        fill="none"
      />
      <path
        d="m48 18.7c-.3 1.1-.6 2.1-.9 3.1l-.4 1.4h2.8l-.5-1.4c-.3-1-.6-2-1-3.1z"
        fill="none"
      />
      <path
        d="m34.4 20.3c0-1.2-.8-1.6-2.2-1.6h-1.5v3.4h1.5c1.4 0 2.2-.6 2.2-1.8z"
        fill="none"
      />
      <path
        d="m90.7 20.3c0-1.2-.8-1.6-2.2-1.6h-1.5v3.4h1.5c1.4 0 2.2-.6 2.2-1.8z"
        fill="none"
      />
      <g fill="#fff" className="read-more-text">
        <path d="m35.6 20.4c0-1.9-1.3-2.6-3.2-2.6h-2.9v9.2h1.2v-3.9h1.7l2.2 3.8h1.3l-2.3-3.9c1.2-.4 2-1.2 2-2.6zm-3.4 1.7h-1.5v-3.4h1.5c1.4 0 2.2.4 2.2 1.6s-.8 1.8-2.2 1.8z" />
        <path d="m39 22.7h3.5v-1h-3.5v-2.9h4.1v-1h-5.2v9.2h5.4v-1h-4.3z" />
        <path d="m47.4 17.8-3.1 9.1h1.2l.9-2.8h3.3l.9 2.9h1.2l-3.1-9.2zm2 5.4h-2.8l.4-1.4c.3-1 .6-2 .9-3.1h.1c.3 1.1.6 2.1.9 3.1z" />
        <path d="m55.8 17.8h-2.3-.1v9.2h2.4c2.8 0 4.3-1.7 4.3-4.6 0-3-1.5-4.6-4.3-4.6zm-.1 8.2h-1v-7.3h1c2.1 0 3.2 1.3 3.2 3.6 0 2.4-1.1 3.7-3.2 3.7z" />
        <path d="m70.2 22.7c-.2.6-.4 1.3-.6 1.9h-.1c-.3-.6-.5-1.3-.7-1.9l-1.8-4.9h-1.4-.1v9.1h1.1v-5.1c0-.8 0-1.9-.1-2.7h.1l.7 2.1 1.7 4.8h.8l1.7-4.7.7-2.1h.1c0 .8-.1 1.9-.1 2.7v5.1h1.1v-9.2h-1.4z" />
        <path d="m79.5 17.6c-2.3 0-3.9 1.8-3.9 4.7 0 3 1.6 4.8 3.9 4.8s3.9-1.9 3.9-4.8-1.6-4.7-3.9-4.7zm0 8.5c-1.6 0-2.7-1.5-2.7-3.8s1.1-3.7 2.7-3.7 2.7 1.4 2.7 3.7-1.1 3.8-2.7 3.8z" />
        <path d="m91.8 20.4c0-1.9-1.3-2.6-3.2-2.6h-2.9v9.2h1.2v-3.9h1.7l2.2 3.8h1.3l-2.3-3.9c1.2-.4 2-1.2 2-2.6zm-3.3 1.7h-1.5v-3.4h1.5c1.4 0 2.2.4 2.2 1.6s-.8 1.8-2.2 1.8z" />
        <path d="m95.3 26v-3.3h3.5v-1h-3.5v-2.9h4.1v-1h-5.2v9.2h5.4v-1z" />
      </g>
    </animated.g>
  );
}
