import React, { useContext } from "react";
import { animated, useSpring } from "react-spring/web.cjs";
import { HeptagonContext } from "../config/context";

export default function Icon({ children, id }) {
  const { activeId, mobile } = useContext(HeptagonContext);

  const props = useSpring({
    config: {
      duration: 1000,
    },
    scale: !mobile || (activeId === id && !mobile) ? 1 : 3,
  });

  return (
    <animated.g className="icon" style={props}>
      {children}
    </animated.g>
  );
}
