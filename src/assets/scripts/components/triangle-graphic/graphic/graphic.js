import React from "react";
import "./graphic.scss";
import Heptagon from "../heptagon/heptagon";

export default function Graphic() {
  return (
    <div className="Graphic">
      <Heptagon />
    </div>
  );
}
