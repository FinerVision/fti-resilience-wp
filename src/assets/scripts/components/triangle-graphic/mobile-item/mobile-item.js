import React, {
  useCallback,
  useContext,
  useEffect,
  useRef,
  useState,
} from "react";
import $ from "jquery";
import "./mobile-item.scss";
import { HeptagonContext } from "../config/context";
import assets from "../config/assets";
import { map } from "../utils";

export default function MobileItem({ id, title, description, link }) {
  const { activeId, setActiveIdState } = useContext(HeptagonContext);
  const container = useRef(null);
  const content = useRef(null);
  const [scrollHeight, setScrollHeight] = useState(0);
  const [maxHeight, setMaxHeight] = useState(0);
  const [top, setTop] = useState(0);

  const updateScrollHeight = useCallback(() => {
    setScrollHeight(content.current.scrollHeight);
  }, [setScrollHeight, content]);

  const updateTop = useCallback(() => {
    setTop(container.current.offsetTop || 0);
  }, [setTop, container]);

  const toggleActiveId = useCallback(() => {
    const active = activeId === id;
    setActiveIdState(active ? null : id);
  }, [setActiveIdState, activeId, id]);

  useEffect(() => {
    updateTop();
    window.addEventListener("resize", updateTop);
    return () => {
      window.removeEventListener("resize", updateTop);
    };
  }, [container]);

  useEffect(() => {
    updateScrollHeight();
    window.addEventListener("resize", updateScrollHeight);
    return () => {
      window.removeEventListener("resize", updateScrollHeight);
    };
  }, [content]);

  useEffect(updateTop, [maxHeight]);

  useEffect(() => {
    const active = activeId === id;
    setMaxHeight(active ? scrollHeight : 0);
    const scrollingComponent = document.querySelector(".App");
    if (active && scrollingComponent) {
      setTimeout(() => {
        $(scrollingComponent).animate(
          { scrollTop: top },
          map(
            Math.abs(scrollingComponent.scrollHeight - top),
            0,
            scrollingComponent.scrollHeight,
            150,
            2500
          )
        );
        // scrollingComponent.scroll({ top, behavior: "smooth" });
      }, 175);
    }
  }, [activeId]);

  return (
    <div
      className={`MobileItem MobileItem--${id} MobileItem--${
        activeId === id ? "open" : "closed"
      }`}
      ref={container}
    >
      <div className="MobileItem__title" onClick={toggleActiveId}>
        <div className="text">
          <img src={assets.fractals[id]} alt="" className="fractal" />
          <span dangerouslySetInnerHTML={{ __html: title }} />
        </div>
        <img src={assets.plus} alt="Expand" className="cta" />
      </div>
      <div className="MobileItem__content" ref={content} style={{ maxHeight }}>
        <div
          className="description"
          dangerouslySetInnerHTML={{ __html: description }}
        />
        <a href={link} target="_blank" className="read-more">
          <img src={assets.readMore} alt="Read More" />
        </a>
      </div>
    </div>
  );
}
