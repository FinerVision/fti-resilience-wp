import plus from "../../../../img/triangle-graphic/plus.svg";
import escalation from "../../../../img/triangle-graphic/escalation.svg";
import remediation from "../../../../img/triangle-graphic/remediation.svg";
import government from "../../../../img/triangle-graphic/government.svg";
import economic from "../../../../img/triangle-graphic/economic.svg";
import business from "../../../../img/triangle-graphic/business.svg";
import operational from "../../../../img/triangle-graphic/operational.svg";
import digital from "../../../../img/triangle-graphic/digital.svg";
import realTime from "../../../../img/triangle-graphic/real-time.svg";
import readMore from "../../../../img/triangle-graphic/read-more.svg";

export default {
  plus,
  readMore,
  fractals: {
    escalation,
    remediation,
    government,
    economic,
    business,
    operational,
    digital,
    "real-time": realTime,
  },
};
