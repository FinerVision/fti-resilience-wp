import React, { Fragment } from "react";
import get from "lodash/get";

export default function Elements({ children }) {
  return [...children]
    .sort((a, b) => {
      const aZ = get(a, "props.zIndex", 0);
      const bZ = get(b, "props.zIndex", 0);
      return aZ - bZ;
    })
    .map((child) => {
      const key = get(child, "props.id");
      return <Fragment key={key}>{child}</Fragment>;
    });
}
