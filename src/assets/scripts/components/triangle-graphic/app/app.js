
import React, {Component, lazy, Suspense, useState, useContext} from "react";
import "./app.scss";
import {AppContext} from "../config/context";
import Graphic from '../graphic/graphic.js';

export default function App() {
  const loading = useState(true);
  const config = useState({});
  const context = useContext(AppContext);
  const state = {
    loading,
    config,
    context,
  };

  return (
    <AppContext.Provider value={state}>
      <Graphic/>
    </AppContext.Provider>
  );
}
