import config from '../../config';

const links = document.querySelectorAll('#explore-another-level');

const findElement = (path) => {

};

if(links) {
  const currentLocation = window.location.pathname;
  config.laverPages.forEach(({path, color, name}, index) => {
    if(currentLocation.includes(path)) {
      let element = null;
      links.forEach((elem) => {
        if(elem.getAttribute('data-path-name') === name) {
         element = elem;
        }
      });
      if(element) {
        element.style['text-decoration'] = 'underline';
        element.style['text-decoration-color'] = color
        element.style['font-weight'] = "600";
      }
    }
  });
}
