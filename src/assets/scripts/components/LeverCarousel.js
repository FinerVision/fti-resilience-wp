import SwiperCore, {Navigation} from 'swiper';
import config from '../../config/index';


SwiperCore.use([Navigation]);
const carousel = new SwiperCore('.lever-swipper-container', {
  loop: true,
  direction: "horizontal",
  loopFillGroupWithBlank: true,
  navigation: {
    nextEl: '.LeverCarousel__button-next',
    prevEl: '.LeverCarousel__button-prev',
  },
  autoplay: {
    delay : 5000,
    disableOnInteraction : false,
  },
  spaceBetween: 0,
  initialSlide: config.laverPages.findIndex((slide) => window.location.pathname === slide.path),
  slidesPerView: 1,
  centeredSlides: true,
  breakpoints: {}
});

carousel.on('slideChangeTransitionEnd', (index) => {
  window.location.pathname = config.laverPages[index.realIndex].path;
});
