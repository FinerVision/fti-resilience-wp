import SwiperCore, {Navigation} from 'swiper';
import Accordion from "./accordion";
import scrollAnimation from "./scrollAnimation";
import './components/dropdown';

SwiperCore.use([Navigation]);
new SwiperCore('.swipper-container', {
  loop: true,
  direction: "horizontal",
  loopFillGroupWithBlank: true,
  navigation: {
    nextEl: '.button-next',
    prevEl: '.button-previous',
  },
  preventClicks: false,
  preventClicksPropagation: false,
  touchRatio: 0,
  spaceBetween: 20,
  autoplay: true,
  slidesPerView: 3,
  centeredSlides: false,
  breakpoints: {
    // when window width is >= 320px
    320: {
      slidesPerView: 1,
      spaceBetween: 20
    },
    // when window width is >= 480px
    576: {
      slidesPerView: 1,
      spaceBetween: 20
    },
    // when window width is >= 640px
    992: {
      slidesPerView: 2,
      spaceBetween: 20
    },
      // when window width is >= 1200px
    1200: {
      slidesPerView: 3,
      spaceBetween: 40
    },

  }
});
new Accordion({
  itemSelector: "#collapsible-body",
  buttonSelector: '#collapsible-button',
});
new scrollAnimation({
  selector: '.Counter',
});

