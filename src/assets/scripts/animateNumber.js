export default function Counter(data) {
  let _default = {
    fps: 60,
    from: 0,
    time: 1000,
  };
  for (let attr in _default) {
    if (typeof data[attr] === 'undefined') {
      data[attr] = _default[attr];
    }
  }

  if (typeof data.to === 'undefined')
    return;

  data.fps = typeof data.fps === 'undefined' ? 20 : parseInt(data.fps);
  data.from = typeof data.from === 'undefined' ? 0 : parseFloat(data.from);

  let frames = data.time / data.fps,
    inc = (data.to - data.from) / frames,
    val = data.from;

  if (typeof data.start === 'function') {
    data.start(data.from, data)
  }
  let interval = setInterval(function() {
    frames--;
    val += inc;
    if (val >= data.to) {
      if (typeof data.complete === 'function') {
        data.complete(data.to, data)
      }
      clearInterval(interval);
    } else if (typeof data.progress === 'function') {
      data.progress(val, data)
    }
  }, data.fps);
}
