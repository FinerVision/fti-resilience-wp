# Policy Exchange

FTI Resilience website templates.

### Getting Started

```bash
cp .env.example .env
docker-compose up --build # this takes ~5 minutes on first run
```

Open the site, running on [localhost:8000](http://localhost:8000)

### Contributing

**Adding variables to templates:**

Open `src/config/routes.php`, find the route you want to edit, and add the variable as a `key => value` pair to the `data` array. Now, from within your template file (e.g. `src/templates/home.php`), you can use the variable here.

**Adding new routes:**

Open `src/config/routes.php` and add a new route block to the array.
